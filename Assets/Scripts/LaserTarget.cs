﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// component for objects that can take damage from a laser

public class LaserTarget : MonoBehaviour
{
    [SerializeField] private AudioSource targetAudio;

    public void TakeLaserDamage(float healthDamage, Vector2 contactPoint)
    {
        var character = GetComponent<CharacterController>();
        if (character != null && !character.HasFainted && !character.IsTeleporting)
        {
            if (targetAudio != null)
                GameEvents.OnPlayAudio?.Invoke(targetAudio, AudioManager.ClipTag.LaserFry, false, 0f, false);

            GameEvents.OnCharacterFaint?.Invoke(character);
        }
    }
}
