using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// connects the switch to the door!

public class DoorSwitch : MonoBehaviour
{
    public Switch doorSwitch;
    public Door door;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawLine(doorSwitch.transform.position, door.transform.position);
    }
}
