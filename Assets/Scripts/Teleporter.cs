using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 1-way or 2-way teleportation device!

public class Teleporter : MonoBehaviour
{
    public TeleportEntry entry;
    public TeleportEntry exit;

    public bool TwoWay = true;          // if 2-way, can enter via entry or exit, else only entry

    private float teleportTime = 0.25f;


    public void Teleport(CharacterController character, TeleportEntry from)
    {
        if (!TwoWay && from != entry)
            return;

        if (from == entry)
        {
            character.Teleport(from, exit, teleportTime);
        }
        else if (from == exit)
        {
            character.Teleport(from, entry, teleportTime);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(entry.transform.position, exit.transform.position);

        if (TwoWay)
        {
            Gizmos.DrawWireSphere(entry.transform.position, 0.4f);
            Gizmos.DrawWireSphere(exit.transform.position, 0.4f);
        }
        else        // one way - entry only active
        {
            Gizmos.DrawWireSphere(entry.transform.position, 0.4f);
        }
    }
}
