using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// aims a laser at AimTarget

public class Aim : MonoBehaviour
{
    public Transform AimTarget;
    public SpriteRenderer Crosshair;
    public LaserSource Laser;
    public bool FireWhenInRange = false;                    // laser is activated only while laser target is in range (else on timer)
    public LineRenderer AimBeam;

    public float BeamWidth = 0.02f;
    public Color BeamColor = Color.red;                     // aim beam

    [Space]
    public Color ClearShotLaserColour = Color.red;          // colour of laser sprite when crosshair activated on target
    public Color ClearShotBeamColour = Color.yellow;        // colour of laser beam when crosshair activated on target

    private Vector2 directionFromTarget;
    private Vector2 directionToTarget;

    private bool TargetInRange => Vector2.Distance(transform.position, AimTarget.position) <= Laser.FireRange;
    private bool HasClearShot => Crosshair != null && Crosshair.enabled;


    private void Start()
    {
        AimBeam.startWidth = BeamWidth;
        AimBeam.endWidth = BeamWidth;

        AimBeam.startColor = BeamColor;
        AimBeam.endColor = BeamColor;
    }

    // look at target and activate crosshair if within range and has a clear shot
    private void Update()
    {
        if (AimTarget == null || Laser == null || Crosshair == null || AimBeam == null)
            return;

        // aim if target in range and enable crosshair and aim beam if has clear shot
        if (TargetInRange)
        {
            LookAt2D(AimTarget.position);

            Crosshair.enabled = AimBeam.enabled = ClearShotToTarget(out Vector2 hitPosition);
            Crosshair.transform.position = hitPosition;

            if (HasClearShot)        // set clear shot colour
            {
                AimBeam.SetPosition(0, Laser.FirePoint.position);
                AimBeam.SetPosition(1, hitPosition);

                Laser.SetLaserColour(ClearShotBeamColour);
                Laser.SetSpriteColour(ClearShotLaserColour);
            }

            if (FireWhenInRange)
                Laser.ActivateLaser();
        }
        else if (FireWhenInRange)
            Laser.DeactivateLaser();

        if (!TargetInRange || !HasClearShot)        // back to default laser settings
        {
            Crosshair.enabled = AimBeam.enabled = false;
            Laser.DefaultLaserColour();
            Laser.DefaultSpriteColour();
        }
    }

    private void LookAt2D(Vector2 target)
    {
        directionFromTarget = (Vector2)transform.position - target;
        directionToTarget = target - (Vector2)transform.position;

        transform.rotation = Quaternion.FromToRotation(Vector2.up, directionFromTarget);
    }

    private bool ClearShotToTarget(out Vector2 hitPosition)
    {
        var hit = Laser.LaserCast(directionToTarget);

        if (hit.collider != null)
        {
            hitPosition = hit.point;
            return hit.collider.transform == AimTarget;
        }

        // hit nothing
        hitPosition = transform.position;
        return false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(Laser.FirePoint.position, Laser.FireRange);
    }
}

