using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CapsuleCollider2D))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(WallJump))]

// controls movement of character via left/right swipe and tap or input buttons/keys

public class CharacterController : MonoBehaviour
{
    public SpriteRenderer sprite;

    //public bool DirectionControl = true;        // push in direction of swipe
    //public bool PushOnDrag = false;             // push in direction of swipe, only on swipe end if false

    public bool JumpFromGroundOnly = true;      // can only jump when on ground
    public bool OnGround { get; private set; }

    //public int JumpLimit = 2;                   // if JumpFromGroundOnly. 0 == infinite
    //[SerializeField] private int jumpCount = 0;
    //private bool JumpLimitReached => JumpLimit > 0 && jumpCount >= JumpLimit;

    //public bool StopOnDoubleTap = true;         // tap 2 fingers to stop

    [Space]
    public bool LimitVelocity = true;           // except gravity
    public float maxHorizSpeed = 6f;            // speed limit
    public float maxUpSpeed = 6f;               // upwards only

    private float minMovingSpeed = 0.25f;       // else considered stationary (x and/or y axis)

    //public bool MovingHoriz => Mathf.Abs(SpeedHoriz) > 0f;
    public bool MovingHoriz => MovingLeft || MovingRight;
    public bool MovingLeft => SpeedHoriz < -minMovingSpeed;
    public bool MovingRight => SpeedHoriz > minMovingSpeed;

    //public bool MovingVertically => Mathf.Abs(SpeedVert) > 0f;
    public bool MovingVert => MovingUp || MovingDown;
    public bool MovingUp => SpeedVert > minMovingSpeed;
    public bool MovingDown => SpeedVert < -minMovingSpeed;

    public float SpeedHoriz => rBody.velocity.x;
    public float SpeedVert => rBody.velocity.y;

    public bool HasFainted { get; private set; } = false;
    public bool ReachedExit { get; private set; } = false;
    public bool IsTeleporting { get; private set; } = false;
    public Vector2 LastFrameVelocity { get; private set; } = Vector2.zero;

    private float teleportDelayTime = 1f;           // pause before can teleport again
    private bool teleportDelay = false;

    // pushed via swipe
    //public float PushForce = 0.2f;
    //private float maxSwipeForce = 2000f;      // clamped

    //public float DragForce = 0.05f;

    //// jump via tap
    //public float StandJumpForce = 80f;     // if not moving
    //public float MoveJumpForce = 4.5f;       // x movement speed

    public ParticleSystem AppearParticles;      // on fade in
    public ParticleSystem TeleportParticles;    

    // direction
    public enum Facing
    {
        Left,
        Right
    }
    [SerializeField] private Facing currentFacing;

    private float flipTime = 0.2f;                 // face left / right
    private bool flipping = false;                  // leantween to face opposite direction

    private Vector2 startScale;
    private float startGravity;

    private bool gameStarted = false;
    private bool menuShowing = false;
    //private bool swiping = false;    

    private bool gamePaused = false;

    private Rigidbody2D rBody;
    private CapsuleCollider2D characterCollider;
    private AudioSource characterAudio;
    private WallJump wallJump;


    private void OnEnable()
    {
        GameEvents.OnGameStart += OnGameStart;
        GameEvents.OnGamePause += OnGamePause;

        //GameEvents.OnSwipeStart += OnSwipeStart;
        //GameEvents.OnSwipeMove += OnSwipeMove;
        //GameEvents.OnSwipeEnd += OnSwipeEnd;

        GameEvents.OnCharacterGrounded += OnCharacterGrounded;
        GameEvents.OnCharacterMove += OnCharacterMove;

        GameEvents.OnAllGemsCollected += OnAllGemsCollected;
        GameEvents.OnReachedFinish += OnReachedFinish;
        GameEvents.OnCharacterFaint += OnCharacterFaint;
        GameEvents.OnMenuShowing += OnMenuShowing;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStart -= OnGameStart;
        GameEvents.OnGamePause -= OnGamePause;

        //GameEvents.OnSwipeStart -= OnSwipeStart;
        //GameEvents.OnSwipeMove -= OnSwipeMove;
        //GameEvents.OnSwipeEnd -= OnSwipeEnd;

        GameEvents.OnCharacterGrounded -= OnCharacterGrounded;
        GameEvents.OnCharacterMove -= OnCharacterMove;

        GameEvents.OnAllGemsCollected -= OnAllGemsCollected;
        GameEvents.OnReachedFinish -= OnReachedFinish;
        GameEvents.OnCharacterFaint -= OnCharacterFaint;
        GameEvents.OnMenuShowing -= OnMenuShowing;
    }

    private void OnMenuShowing(bool showing)
    {
        menuShowing = showing;
    }

    private void Awake()
    {
        rBody = GetComponent<Rigidbody2D>();
        characterAudio = GetComponent<AudioSource>();
        characterCollider = GetComponent<CapsuleCollider2D>();
        wallJump = GetComponent<WallJump>();

        startScale = sprite.transform.localScale;
        startGravity = rBody.gravityScale;
    }


    private void FixedUpdate()
    {
        if (LimitVelocity)
        {
            if (MovingLeft || MovingRight || MovingUp)      // let gravity do its thing!
            {
                var horizVelocity = Mathf.Clamp(rBody.velocity.x, -maxHorizSpeed, maxHorizSpeed);
                // let gravity do its thing!
                var vertVelocity = MovingDown ? rBody.velocity.y : Mathf.Clamp(rBody.velocity.y, -maxUpSpeed, maxUpSpeed);

                //Debug.Log($"LimitVelocity {rBody.velocity.x} {rBody.velocity.y} limited = {horizVelocity} {vertVelocity}");
                rBody.velocity = new Vector2(horizVelocity, vertVelocity);
            }
        }

        LastFrameVelocity = rBody.velocity;
    }

    private void OnGameStart(GameData gameData, int playCount, bool promptPassword)
    {
        gameStarted = true;

        AppearParticles.Play();
        GameEvents.OnPlayAudio?.Invoke(characterAudio, AudioManager.ClipTag.Spawn, true, 0f, false);
    }

    private void OnGamePause(bool paused)
    {
        gamePaused = paused;
    }

    //private void OnSwipeStart(Vector2 startPosition, int touchCount)
    //{
    //    if (!gameStarted || gamePaused || swiping || menuShowing)
    //        return;

    //    if (HasFainted || IsTeleporting || ReachedExit)
    //        return;

    //    //if (StopOnDoubleTap && touchCount == 2)
    //    //{
    //    //    StopHoriz();
    //    //    return;
    //    //}

    //    if (touchCount == 1)
    //        swiping = true;
    //}

    //// direction is already normalized
    //private void OnSwipeMove(Vector2 movePosition, Vector2 direction, float speed, float distance, int touchCount)
    //{
    //    if (!gameStarted || gamePaused || !swiping || menuShowing)
    //        return;

    //    if (HasFainted || IsTeleporting || ReachedExit)
    //        return;

    //    if (JumpLimitReached)
    //        return;

    //    //if (DirectionControl && PushOnDrag && !JumpFromGroundOnly)
    //    //{
    //    //    SwipePush(DragForce, direction, speed, distance);
    //    //}
    //}

    // direction is already normalized
    //private void OnSwipeEnd(Vector2 endPosition, Vector2 direction, float speed, float distance, int touchCount, int tapCount)
    //{
    //    if (!gameStarted || gamePaused || !swiping || menuShowing)
    //        return;

    //    swiping = false;

    //    if (HasFainted || IsTeleporting || ReachedExit)
    //        return;

    //    if (JumpFromGroundOnly)
    //    {
    //        if (!OnGround && JumpLimitReached)
    //            return;
    //    }

    //    //if (DirectionControl && !PushOnDrag)
    //    //{
    //    //    SwipePush(PushForce, direction, speed, distance);
    //    //}
    //}

    // direction is normalised
    private void OnCharacterMove(Vector2 moveDirection, float force)
    {
        if (!gameStarted || gamePaused || menuShowing)
            return;

        if (IsTeleporting || HasFainted || force <= 0f)
            return;

        // hijack move direction and force for wall jump, if available
        if (wallJump.AgainstWall != null)
        {
            //Debug.Log($"OnCharacterMove: AgainstWall = {wallJump.AgainstWall.name} JumpWallSide = {wallJump.AgainstWallSide} direction = {direction}");

            switch (wallJump.AgainstWallDirection)
            {
                case WallJump.WallDirection.OnLeft:
                    if (moveDirection == Vector2.right)             // wall jump diagonally to the right!
                    {
                        moveDirection = new Vector2(1, 1);     
                        force = wallJump.AgainstWall.WallJumpForce;
                        wallJump.AgainstWall.WallJumpPulse();

                        GameEvents.OnCharacterWallJump?.Invoke(wallJump.AgainstWall, WallJump.WallDirection.OnLeft);
                    }
                    break;

                case WallJump.WallDirection.OnRight:
                    if (moveDirection == Vector2.left)              // wall jump diagonally to the left!
                    {
                        moveDirection = new Vector2(-1, 1);     
                        force = wallJump.AgainstWall.WallJumpForce;
                        wallJump.AgainstWall.WallJumpPulse();

                        GameEvents.OnCharacterWallJump?.Invoke(wallJump.AgainstWall, WallJump.WallDirection.OnRight);
                    }
                    break;
            }
        }

        Push(moveDirection * force);
    }

    //private void SwipePush(float force, Vector2 direction, float speed, float distance)
    //{
    //    float swipeForce = distance * speed;  // longer and faster swipe == more force
    //    //Debug.Log($"SwipePush: swipeForce = {swipeForce} maxSwipeForce = {maxSwipeForce}");
    //    swipeForce = Mathf.Clamp(swipeForce, 0f, maxSwipeForce);
    //    Push(force * swipeForce * direction);

    //    jumpCount++;
    //}

    public void Push(Vector2 force)
    {
        //Debug.Log($"Push: force = {force}");
        rBody.AddForce(force, ForceMode2D.Impulse);
    }

    public void StopHoriz()
    {
        rBody.velocity = new Vector2(0f, rBody.velocity.y);
    }

    public void StopVert()
    {
        rBody.velocity = new Vector2(rBody.velocity.x, 0);
    }

    public void StopDead()
    {
        rBody.velocity = Vector2.zero;
    }

    public void SetFacing(Facing facing)
    {
        if (currentFacing == facing)
            return;

        if (flipping)
            return;

        currentFacing = facing;
        flipping = true;

        LeanTween.rotateAround(gameObject, transform.up, 180f, flipTime)
                    .setEaseOutBack()
                    .setOnComplete(() => { flipping = false; });
    }

    private void OnCharacterGrounded(CharacterController character, bool isGrounded, bool wasGrounded, Collider2D groundedOn)
    {
        OnGround = isGrounded;

        //if (OnGround)
        //{
        //    jumpCount = 0;
        //    //Debug.Log($"SetGrounded: jumpCount = {jumpCount}");
        //}
    }

    public void Teleport(TeleportEntry teleportFrom, TeleportEntry teleportTo, float teleportTime)
    {
        if (IsTeleporting || teleportDelay)
            return;

        IsTeleporting = true;
        rBody.velocity = Vector2.zero;      // stop
        characterCollider.enabled = false;

        if (TeleportParticles != null)
            TeleportParticles.Play();

        ActivateGravity(false);

        GameEvents.OnPlayAudio?.Invoke(teleportFrom.GetComponent<AudioSource>(), AudioManager.ClipTag.Teleport, true, 0f, false);

        // shrink and move to teleportFrom centre
        LeanTween.scale(sprite.gameObject, Vector2.zero, teleportTime * 0.5f)
                    .setEaseInQuart();

        LeanTween.move(gameObject, teleportFrom.transform.position, teleportTime)
                    .setEaseInQuart()
                    .setOnComplete(() =>
                    {
                        // hide and teleport to teleportTo
                        sprite.enabled = false;

                        LeanTween.move(gameObject, teleportTo.transform.position, teleportTime)
                                    .setOnComplete(() =>
                                    {
                                        // reveal and scale back up
                                        sprite.enabled = true;
                                        //characterCollider.enabled = true;

                                        if (TeleportParticles != null)
                                            TeleportParticles.Stop();

                                        AppearParticles.Play();
                                        ActivateGravity(true);

                                        GameEvents.OnPlayAudio?.Invoke(teleportTo.GetComponent<AudioSource>(), AudioManager.ClipTag.Teleport, true, 0f, false);

                                        LeanTween.scale(sprite.gameObject, startScale, teleportTime * 0.5f)
                                                    .setEaseOutQuart()
                                                    .setOnComplete(() =>
                                                    {
                                                        characterCollider.enabled = true;
                                                        IsTeleporting = false;
                                                        StartCoroutine(DelayTeleportOff());
                                                        //ActivateGravity(true);
                                                    });
                                    });
                    });
    }

    private IEnumerator DelayTeleportOff()
    {
        teleportDelay = true;
        yield return new WaitForSeconds(teleportDelayTime);
        teleportDelay = false;
    }


    private void OnAllGemsCollected(float collectedGemValue)
    {
        GameEvents.OnPlayAudio?.Invoke(characterAudio, AudioManager.ClipTag.Success, false, 0f, false);
    }

    private void OnReachedFinish()
    {
        ReachedExit = true;
        GameEvents.OnPlayAudio?.Invoke(characterAudio, AudioManager.ClipTag.Success, true, 0f, false);
    }

    private void OnCharacterFaint(CharacterController character)
    {
        if (HasFainted)
            return;

        StopDead();
        GameEvents.OnPlayAudio?.Invoke(characterAudio, AudioManager.ClipTag.Faint, true, 0f, false);

        HasFainted = true;
    }

    private void ActivateGravity(bool activate)
    {
        rBody.gravityScale = activate ? startGravity : 0f;
    }

    public void HitAudio()
    {
        if (! HasFainted && ! ReachedExit)
            GameEvents.OnPlayAudio?.Invoke(characterAudio, AudioManager.ClipTag.CharacterHit, false, 0f, false);
    }

    public void FootfallAudio()
    {
        if (!HasFainted && !ReachedExit)
            GameEvents.OnPlayAudio?.Invoke(characterAudio, AudioManager.ClipTag.Footstep, false, 0f, false);
    }

    public void JumpAudio()
    {
        if (!HasFainted && !ReachedExit)
            GameEvents.OnPlayAudio?.Invoke(characterAudio, AudioManager.ClipTag.Jump, false, 0f, false);
    }
}
