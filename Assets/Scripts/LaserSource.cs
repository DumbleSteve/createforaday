﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// shoots a laser beam!
// does damage to a LaserTarget

public class LaserSource : MonoBehaviour
{
    [Header("Laser Beam")]
    public LineRenderer LaserBeam;
    public float LaserWidth = 0.04f;
    public Transform FirePoint;                    // laser start
    public Transform FireEndPoint;
    public SpriteRenderer LaserSprite;

    public Vector2 FireDirection => (FireEndPoint.position - FirePoint.position).normalized;
    public float FireRange => Vector2.Distance(FirePoint.position, FireEndPoint.position);

    [Header("Laser Time")]
    public float LaserDelay = 0f;
    public float LaserInterval = 3f;
    public float LaserTime = 1.5f;

    [Header("Particles")]
    public GameObject StartVFX;
    public GameObject EndVFX;

    [Header("Audio")]
    public AudioSource LaserAudio;          // looping
    //public AudioManager.ClipTag LaserAudioTag = AudioManager.ClipTag.Laser;
 
    private float pitchMin = 0.75f;
    private float pitchMax = 1.5f;
    private float randomPitch => Random.Range(pitchMin, pitchMax);

    private float laserPitch;                       // random, but stays fixed for this laser

    public Color LaserColour = Color.yellow;        // line renderer colour

    private float LaserIntensity = 2f;

    [Header("Damage Inflicted")]
    public float HealthDamage = 0.5f;           // damage inflicted on LaserTarget - each frame!

    private List<ParticleSystem> particleList = new List<ParticleSystem>();

    [Header("Laser Hit")]
    public LayerMask LaserLayers;           // for ray cast hits

    private bool firing = false;            // laser on

    //public Color defaultColour = Color.white;
    public Color NormalColour = Color.white;

    private Aim laserAim;                   // optional component - only fire laser when target in range


    private void Awake()
    {
        //defaultColour = LaserSprite.color;
        laserAim = GetComponent<Aim>();         // optional
    }

    // Start is called before the first frame update
    private void Start()
    {
        PopulateParticleList();
        DefaultLaserColour();
        SetLaserWidth();
        SetLaserAudio();

        DeactivateLaser();

        CeaseFire();

        if (laserAim == null || ! laserAim.FireWhenInRange)
            StartCoroutine(PulseLaser());
    }

    // indefinate on/off cycle
    private IEnumerator PulseLaser()
    {
        yield return new WaitForSeconds(LaserDelay);

        while (true)
        {
            yield return new WaitForSeconds(LaserInterval);
            ActivateLaser();
            yield return new WaitForSeconds(LaserTime);
            DeactivateLaser();
        }
    }

    private IEnumerator FireLaser()
    {
        if (firing)
            yield break;

        firing = true;

        //Vector2 fireDirection;
        Vector2 laserEndPoint;
        RaycastHit2D hit;

        LaserBeam.enabled = true;

        while (firing)
        {
            //fireDirection = (FireEndPoint.position - FirePoint.position).normalized;
            hit = LaserCast(FireDirection);

            if (hit.collider != null)
            {
                laserEndPoint = hit.point;

                var laserTarget = hit.collider.GetComponent<LaserTarget>();
                if (laserTarget != null)
                {
                    laserTarget.TakeLaserDamage(HealthDamage, laserEndPoint);
                }
            }
            else            // no hit - full range
            {
                //laserEndPoint = (Vector2)FirePoint.position + (fireDirection * FireRange);
                laserEndPoint = FireEndPoint.position;
            }

            LaserBeam.SetPosition(0, FirePoint.position);
            LaserBeam.SetPosition(1, laserEndPoint);

            StartVFX.transform.position = FirePoint.position;
            EndVFX.transform.position = laserEndPoint;

            yield return null;
        }

        LaserBeam.enabled = false;
    }

    public RaycastHit2D LaserCast(Vector2 direction)
    {
        //return Physics2D.CircleCast(FirePoint.position, LaserWidth / 2f, direction, FireRange, LaserLayers);
        return Physics2D.CircleCast(FirePoint.position, LaserWidth / 2f, FireDirection, FireRange, LaserLayers);
    }


    public void ActivateLaser()
    {
        CeaseFire();

        StartCoroutine(FireLaser());
        ActivateParticles();

        if (LaserAudio != null)
            GameEvents.OnPlayAudio?.Invoke(LaserAudio, AudioManager.ClipTag.Laser, false, laserPitch, false);
    }


    public void DeactivateLaser()
    {
        LaserBeam.enabled = false;
        CeaseFire();

        DeactivateParticles();

        if (LaserAudio != null)
            LaserAudio.Stop();
    }


    private void ActivateParticles()
    {
        foreach (var particles in particleList)
        {
            particles.Play();
        }
    }

    private void DeactivateParticles()
    {
        foreach (var particles in particleList)
        {
            particles.Stop();
        }
    }

    private void SetLaserWidth()
    {
        LaserBeam.startWidth = LaserWidth;
        LaserBeam.endWidth = LaserWidth;
    }
     
    // set random pitch for this laser
    private void SetLaserAudio()
    {
        laserPitch = randomPitch;
    }

    public void DefaultLaserColour()
    {
        SetLaserColour(LaserColour, LaserIntensity);
    }

    public void SetLaserColour(Color colour, float intensity = 1f)
    {
        if (LaserBeam.startColor == colour)
            return;

        //foreach (var particles in particleList)
        //{
        //    var main = particles.main;
        //    main.startColor = colour;
        //}

        LaserBeam.startColor = colour;
        LaserBeam.endColor = colour;

        // set shader property value
        //LaserBeam.material.SetColor("LaserColour", colour * intensity);
    }

    public void DefaultSpriteColour()
    {
        LaserSprite.color = NormalColour;
    }

    public void SetSpriteColour(Color colour)
    {
        LaserSprite.color = colour;
    }

    private void PopulateParticleList()
    {
        foreach (Transform child in StartVFX.transform)
        {
            var particles = child.GetComponent<ParticleSystem>();

            if (particles != null)
                particleList.Add(particles);
        }

        foreach (Transform child in EndVFX.transform)
        {
            var particles = child.GetComponent<ParticleSystem>();

            if (particles != null)
                particleList.Add(particles);
        }
    }

    private void CeaseFire()
    {
        firing = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        //var fireDirection = (FireDirection.position - FirePoint.position).normalized;
        //Gizmos.DrawLine(FirePoint.position, FirePoint.position + (fireDirection * FireRange));
        Gizmos.DrawLine(FirePoint.position, FireEndPoint.position);
    }
}
