using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(SpriteRenderer))]

public class SpriteFader : MonoBehaviour
{
    public float FadeTime = 1f;
    public float FadeOutDelay = 1f;

    public float AscendTime = 2f;
    public float AscendDelay = 0.25f;
    public float AscendDistance = -1f;

    public SpriteRenderer sprite;

    private Color spriteColour;

    private void OnEnable()
    {
        GameEvents.OnGameStart += OnGameStart;
        GameEvents.OnGameWon += OnGameWon;          // pause before next scene loaded
    }

    private void OnDisable()
    {
        GameEvents.OnGameStart -= OnGameStart;
        GameEvents.OnGameWon -= OnGameWon;
    }


    private void Awake()
    {
        //sprite = GetComponent<SpriteRenderer>();
        spriteColour = sprite.color;

        sprite.color = Color.clear;
    }

    private void OnGameStart(GameData gameData, int playCount, bool promptPassword)
    {
        FadeIn();
    }

    private void OnGameWon(int gameHundredths, int bestHundredths, int bonusHundredths, string nextLevelName, GameData game)
    {
        FadeOut(false);
    }


    public void FadeIn()
    {
        LeanTween.value(gameObject, Color.clear, spriteColour, FadeTime)
                        .setOnUpdate((Color c) => sprite.color = c)
                        .setEaseOutQuad();
    }

    public void FadeOut(bool ascend)
    {
        LeanTween.value(gameObject, spriteColour, Color.clear, FadeTime)
                        .setDelay(FadeOutDelay)
                        .setOnUpdate((Color c) => sprite.color = c)
                        .setEaseOutQuad();

        //if (ascend)
        //{
        //    LeanTween.moveY(gameObject, AscendDistance, AscendTime)
        //                .setDelay(AscendDelay)
        //                .setEaseOutQuad();
        //}
    }
}
