using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Animator))]

public class Door : MonoBehaviour
{
    public bool IsExit = false;
    public bool AnimateOpenClose = false;
    public bool OpenVertically = false;
    public bool ShrinkLockedOnUnlock = true;      // locked sprite scales down when unlocked
    public bool ScaleUnlockedOnUnlock = true;     // unlocked sprite scales up when unlocked

    [Space]
    public DoorSwitch ParentDoorSwitch;
    private Collider2D doorCollider;

    public SpriteRenderer LockedSprite;
    public SpriteRenderer UnlockedSprite;

    //public AudioManager.ClipTag openAudioTag = AudioManager.ClipTag.DoorOpen; 
    //public AudioManager.ClipTag closeAudioTag = AudioManager.ClipTag.DoorClose; 

    private AudioSource doorAudio;
    private Animator doorAnimator;

    public float openCloseTime = 0.75f;
    private Vector2 lockedStartScale;
    private Vector2 unlockedStartScale;


    private void Awake()
    {
        doorCollider = GetComponent<Collider2D>();
        doorAudio = GetComponent<AudioSource>();
        doorAnimator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        GameEvents.OnGameStart += OnGameStart;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStart -= OnGameStart;
    }

    private void Start()
    {
        lockedStartScale = LockedSprite.transform.localScale;       // scales to zero to 'open'
        unlockedStartScale = UnlockedSprite.transform.localScale;   // scales from zero to 'open'

        //UnlockedSprite.transform.localScale = new Vector2(unlockedStartScale.x, 0f);

        //if (AnimateOpenClose)
        //{
        //    UnlockedSprite.gameObject.SetActive(false);
        //    LockedSprite.gameObject.SetActive(true);
        //}

        //Lock(true);
    }

    private void OnGameStart(GameData gameData, int playCount, bool promptPassword)
    {
        if (AnimateOpenClose)
        {
            UnlockedSprite.gameObject.SetActive(false);
            LockedSprite.gameObject.SetActive(true);
        }

        Lock(true);
    }

    // show (scale up) LockedSprite to hide UnlockedSprite
    public void Lock(bool silent)
    {
        doorCollider.isTrigger = false;

        if (!silent)
        {
            GameEvents.OnPlayAudio?.Invoke(doorAudio, AudioManager.ClipTag.DoorClose, false, 0f, false);
        }

        if (AnimateOpenClose)
        {
            doorAnimator.SetTrigger("DoorClose");
        }
        else if (OpenVertically)
        {
            // door sprite's pivot point is top centre
            LeanTween.scaleY(LockedSprite.gameObject, lockedStartScale.y, openCloseTime)
                            .setEaseOutQuart();

            if (ScaleUnlockedOnUnlock)
            {
                //UnlockedSprite.transform.localScale = new Vector2(unlockedStartScale.x, 0f);
                LeanTween.scaleY(UnlockedSprite.gameObject, 0f, openCloseTime)
                        .setEaseOutQuart();
            }
        }
        else
        {
            // door sprite's pivot point is left centre
            LeanTween.scaleX(LockedSprite.gameObject, lockedStartScale.x, openCloseTime)
                            .setEaseOutQuart();

            if (ScaleUnlockedOnUnlock)
            {
                //UnlockedSprite.transform.localScale = new Vector2(unlockedStartScale.x, 0f);
                LeanTween.scaleX(UnlockedSprite.gameObject, 0f, openCloseTime)
                        .setEaseOutQuart();
            }
        }
    }

    // hide (scale down) LockedSprite to reveal UnlockedSprite
    public void UnLock(bool silent)
    {
        doorCollider.isTrigger = true;

        if (!silent)
        {
            GameEvents.OnPlayAudio?.Invoke(doorAudio, AudioManager.ClipTag.DoorOpen, false, 0f, false);
        }

        if (AnimateOpenClose)
        {
            doorAnimator.SetTrigger("DoorOpen");
        }
        else if (OpenVertically)
        {
            // door sprite's pivot point is top centre
            if (ShrinkLockedOnUnlock)
            {
                LeanTween.scaleY(LockedSprite.gameObject, 0f, openCloseTime)
                    .setEaseOutQuart();
            }

            if (ScaleUnlockedOnUnlock)
            {
                UnlockedSprite.transform.localScale = new Vector2(unlockedStartScale.x, 0f);
                LeanTween.scaleY(UnlockedSprite.gameObject, unlockedStartScale.y, openCloseTime)
                        .setEaseOutQuart();
            }
        }
        else
        {
            // door sprite's pivot point is left centre
            if (ShrinkLockedOnUnlock)
            {
                LeanTween.scaleX(LockedSprite.gameObject, 0f, openCloseTime)
                    .setEaseOutQuart();
            }

            if (ScaleUnlockedOnUnlock)
            {
                UnlockedSprite.transform.localScale = new Vector2(0f, unlockedStartScale.y);

                LeanTween.scaleX(UnlockedSprite.gameObject, unlockedStartScale.x, openCloseTime)
                        .setEaseOutQuart();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (IsExit && collision.gameObject.CompareTag("Player"))
        {
            var character = collision.gameObject.GetComponent<CharacterController>();
            if (character.HasFainted || character.IsTeleporting)
                return;

            doorCollider.enabled = false;
            GameEvents.OnReachedFinish?.Invoke();
            //Debug.Log("Reached EXIT!!");
        }
    }
}
