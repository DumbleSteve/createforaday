using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuUI : MonoBehaviour
{
    public Button ResetButton;
    public Button ConfirmButton;
    public Button QuitButton;

    private string gameName;


    private void OnEnable()
    {
        GameEvents.OnGameStart += OnGameStart;

        InitButtons();

        ResetButton.onClick.AddListener(ConfirmReset);
        ConfirmButton.onClick.AddListener(ResetData);
        QuitButton.onClick.AddListener(QuitGame);
    }

    private void OnDisable()
    {
        GameEvents.OnGameStart -= OnGameStart;
 
        ResetButton.onClick.RemoveListener(ConfirmReset);
        ConfirmButton.onClick.RemoveListener(ResetData);
        QuitButton.onClick.RemoveListener(QuitGame);
    }

    private void OnGameStart(GameData gameData, int playCount, bool promptPassword)
    {
        gameName = gameData.GameName;
    }

    private void InitButtons()
    {
        ResetButton.gameObject.SetActive(true);
        ConfirmButton.gameObject.SetActive(false);
    }

    private void ConfirmReset()
    {
        ResetButton.gameObject.SetActive(false);
        ConfirmButton.gameObject.SetActive(true);
    }

    private void ResetData()
    {
        GameEvents.OnResetGameData?.Invoke(); // gameName);       // resets all SO data and reloads current scene
        ConfirmButton.gameObject.SetActive(false);
    }

    private void QuitGame()
    {
        Application.Quit();
    }

}
