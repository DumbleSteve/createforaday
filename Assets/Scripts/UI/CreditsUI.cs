using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class CreditsUI : MonoBehaviour
{
    public TextMeshProUGUI gameNameText;
    public TextMeshProUGUI teamText;
    public TextMeshProUGUI schoolText;
    public TextMeshProUGUI creditsText;


    public void SetCredits(string gameName, string teamName, string schoolName, List<string> credits)
    {
        gameNameText.text = gameName;
        teamText.text = teamName;
        schoolText.text = schoolName;

        if (credits == null || credits.Count == 0)
            return;

        string creditsString = "";
        foreach (string credit in credits)
        {
            creditsString += credit;
            creditsString += "\n";
        }
        creditsText.text = creditsString;
    }
}
