using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WinUI : MonoBehaviour
{
    public TextMeshProUGUI congratsText;

    public TextMeshProUGUI gameTimeText;
    public TextMeshProUGUI bestTimeText;
    public TextMeshProUGUI bonusTimeText;
    public TextMeshProUGUI bonusTimePrompt;

    public TextMeshProUGUI newBestText;           // PB - flash on/off

    public TextMeshProUGUI totalTimeText;

    //public Color newPBColour = Color.magenta;
    public TextMeshProUGUI nextLevelText;           // on button

    public Button continueButton;
    public Button replayButton;

    public List<string> Congrats;
    public string RandomCongrats => Congrats.Count > 0 ? Congrats[Random.Range(0, Congrats.Count)] : "Congratulations!";

    private float textFadeTime = 0.2f;
    private float textFlashPauseTime = 0.5f;

    private void OnEnable()
    {
        continueButton.onClick.AddListener(ContinueClicked);
        replayButton.onClick.AddListener(ReplayClicked);
    }

    private void OnDisable()
    {
        continueButton.onClick.RemoveListener(ContinueClicked);
        replayButton.onClick.RemoveListener(ReplayClicked);
    }

    private void Start()
    {
        newBestText.enabled = false;
    }

    private void ContinueClicked()
    {
        GameEvents.OnLoadNextLevel?.Invoke();
    }

    private void ReplayClicked()
    {
        GameEvents.OnReloadLevel?.Invoke();
    }

    public void SetWinText(int gameHundredths, int bestHundredths, int totalPlayHundredths, int bonusHundredths, string nextLevelName)
    {
        congratsText.text = RandomCongrats;
        gameTimeText.text = Utility.FormatTime(gameHundredths);
        bestTimeText.text = Utility.FormatTime(bestHundredths);

        bonusTimePrompt.enabled = bonusHundredths > 0;
        if (bonusHundredths > 0)
        {
            bonusTimeText.text = Utility.FormatTime(bonusHundredths);
            StartCoroutine(RepeatFlashText(bonusTimeText, bonusTimeText.color));
        }

        //totalTimeText.text = "Total Play Time: " + Utility.FormatTime(totalPlayHundredths);

        if (string.IsNullOrEmpty(nextLevelName))
            nextLevelText.text = "Next...";
        else
            nextLevelText.text = "Next: " + nextLevelName + "...";
    }

    public void NewPB(int newBestTime, bool flash)
    {
        bestTimeText.text = "Best: " + Utility.FormatTime(newBestTime);

        if (flash)
            StartCoroutine(RepeatFlashText(newBestText, newBestText.color));
    }

    private IEnumerator RepeatFlashText(TextMeshProUGUI text, Color textColour)
    {
        while (true)
        {
            FlashText(text, textColour);
            yield return new WaitForSeconds((textFlashPauseTime * 2f) + (textFadeTime * 2f));
        }
    }

    private void FlashText(TextMeshProUGUI text, Color textColour)
    {
        text.color = Color.clear;
        text.enabled = true;

        LeanTween.value(text.gameObject, Color.clear, textColour, textFadeTime)
                    .setDelay(textFlashPauseTime)
                    .setOnUpdate((Color c) => text.color = c)
                    .setEaseOutSine()
                    .setOnComplete(() =>
                    {
                        LeanTween.value(text.gameObject, textColour, Color.clear, textFadeTime)
                                    .setDelay(textFlashPauseTime)
                                    .setOnUpdate((Color c) => text.color = c)
                                    .setEaseInSine();
                    });
    }
}
