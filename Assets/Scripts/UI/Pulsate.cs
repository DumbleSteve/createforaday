using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulsate : MonoBehaviour
{
    public Color fromColour = Color.black;
    public Color toColour = Color.white;

    public SpriteRenderer sprite;       // child object

    public bool delayPulse;             // wait pulseTime before pulsating

    public float pulseTime = 3f;
    private float pulseFactor = 1.5f;
    private Vector2 pulseScale;         // based on scale at start

    private float fromAlpha = 0.75f;
    private Color fromAlphaColour;

    private float toAlpha = 0.125f;
    private Color toAlphaColour;


    private void Start()
    {
        pulseScale = sprite.transform.localScale * pulseFactor;

        fromAlphaColour = new Color(fromColour.r, fromColour.g, fromColour.b, fromAlpha);
        toAlphaColour = new Color(toColour.r, toColour.g, toColour.b, toAlpha);

        Pulse();
    }

    private void Pulse()
    {
        LeanTween.scale(sprite.gameObject, pulseScale, pulseTime)
                    .setDelay(delayPulse ? pulseTime : 0f)
                    .setEaseInSine()
                    .setLoopPingPong();

        LeanTween.value(sprite.gameObject, fromAlphaColour, toAlphaColour, pulseTime)
                    .setDelay(delayPulse ? pulseTime : 0f)
                    .setOnUpdate((Color c) => sprite.color = c)
                    .setEaseInSine()
                    .setLoopPingPong();
    }

}
