using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using SimpleJSON;


public class PlayerUI : MonoBehaviour
{
    public TMP_InputField PlayerInput;
    public TMP_Dropdown SchoolDropdown;
    public TMP_InputField PasswordInput;

    public TextMeshProUGUI FeedbackText;

    public Button SaveButton;               // prompts for confirmation
    public Button ConfirmButton;            // enabled when input validated ok, saves to db
    public Button SkipButton;            // enabled when input validated ok, saves to db

    private List<string> playerNames;       // to validate new name
    private List<string> schoolNames;       // for dropdown
    private List<string> passwords;          // to validate

    private string playerName;              // as entered
    private string schoolName;              // as picked from dropdown
    private string password;              // as entered

    private bool playersLoaded = false;
    private bool schoolsLoaded = false;
    private bool passwordsLoaded = false;

//    private void Awake()
//    {
//#if !UNITY_EDITOR && UNITY_WEBGL
//        WebGLInput.captureAllKeyboardInput = false;
//#endif
//    }

    private void OnEnable()
    {
#if !UNITY_EDITOR && UNITY_WEBGL
        WebGLInput.captureAllKeyboardInput = false;
#endif

        GameEvents.OnFireStorePlayersRetrieved += OnPlayersChanged;    
        GameEvents.OnFireStorePlayersChanged += OnPlayersChanged;      // changed in db...
        GameEvents.OnFireStoreSchoolsRetrieved += OnSchoolsChanged;
        GameEvents.OnFireStoreSchoolsChanged += OnSchoolsChanged;      // changed in db...
        GameEvents.OnFireStorePasswordsRetrieved += OnPasswordsChanged;
        GameEvents.OnFireStorePasswordsChanged += OnPasswordsChanged;      // changed in db...

        GameEvents.OnFireStoreGetSchools?.Invoke();
        GameEvents.OnFireStoreGetPlayers?.Invoke();
        GameEvents.OnFireStoreGetPasswords?.Invoke();

        // start listening for db changes to repopulate validation lists
        GameEvents.OnFireStoreStartListenSchoolsChanged?.Invoke();       // OnSchoolsChanged callback
        GameEvents.OnFireStoreStartListenPlayersChanged?.Invoke();       // OnPlayersChanged callback
        GameEvents.OnFireStoreStartListenPasswordsChanged?.Invoke();       // OnPasswordsChanged callback

        SaveButton.onClick.AddListener(ConfirmNewPlayer);       
        ConfirmButton.onClick.AddListener(SaveNewPlayer);
        SkipButton.onClick.AddListener(SkipRegisterPlayer);

        SaveButton.gameObject.SetActive(true);                  // prompts for confirmation
        ConfirmButton.gameObject.SetActive(false);              // enabled when input validated ok - saves to db
    }

    private void OnDisable()
    {
        GameEvents.OnFireStorePlayersRetrieved -= OnPlayersChanged;
        GameEvents.OnFireStorePlayersChanged -= OnPlayersChanged;      // changed in db...
        GameEvents.OnFireStoreSchoolsRetrieved -= OnSchoolsChanged;
        GameEvents.OnFireStoreSchoolsChanged -= OnSchoolsChanged;      // changed in db...
        GameEvents.OnFireStorePasswordsRetrieved -= OnPasswordsChanged;
        GameEvents.OnFireStorePasswordsChanged -= OnPasswordsChanged;      // changed in db...

        // stop listening for db changes
        GameEvents.OnFireStoreStopListenSchoolsChanged?.Invoke();
        GameEvents.OnFireStoreStopListenPlayersChanged?.Invoke();
        GameEvents.OnFireStoreStopListenPasswordsChanged?.Invoke();

        SaveButton.onClick.RemoveListener(ConfirmNewPlayer);
        ConfirmButton.onClick.RemoveListener(SaveNewPlayer);
        SkipButton.onClick.RemoveListener(SkipRegisterPlayer);

#if !UNITY_EDITOR && UNITY_WEBGL
        WebGLInput.captureAllKeyboardInput = true;
#endif
    }


    private bool ValidateNewPlayer()
    {
        if (!playersLoaded || !schoolsLoaded || !passwordsLoaded)
        {
            FeedbackText.text = "Data loading...";
            return false;
        }

        playerName = PlayerInput.text = Utility.FirebaseClean(PlayerInput.text);       // replace / and . with _
        schoolName = SchoolDropdown.captionText.text;
        password = PasswordInput.text = Utility.FirebaseClean(PasswordInput.text);       // replace / and . with _

        if (string.IsNullOrEmpty(playerName))
        {
            FeedbackText.text = "Enter a name for your Leaderboard scores!";
            return false;
        }

        if (PlayerExists(playerName))
        {
            FeedbackText.text = "Player name '" + playerName + "' is already in use!";
            PlayerInput.text = "";
            return false;
        }

        if (string.IsNullOrEmpty(password))
        {
            FeedbackText.text = "Please enter a new Leaderboard Access Key";
            return false;
        }

        if (PasswordExists(password))
        {
            FeedbackText.text = "Access Key '" + password + "' is already in use!";
            PasswordInput.text = "";
            return false;
        }

        var fullName = Utility.FullPlayerName(playerName, schoolName, true);
        FeedbackText.text = "Please check your details before saving:\n" + fullName + "\nRemember your Leaderboard Access Key:   " + password;
        return true;
    }

    // on 'save' button click...
    private void ConfirmNewPlayer()
    {
        if (ValidateNewPlayer())
        {
            SaveButton.gameObject.SetActive(false);
            ConfirmButton.gameObject.SetActive(true);
        }
    }

    // on 'confirm' button click...
    private void SaveNewPlayer()
    {
        if (ValidateNewPlayer())     // to make sure wasn't added by somebody else since hitting 'save' button...!!
        {
            ConfirmButton.interactable = false;

            // save player to firestore db
            var newPlayerData = new PlayerData(playerName, schoolName, password);
            GameEvents.OnFireStoreSetPlayer?.Invoke(playerName, schoolName, password, JsonUtility.ToJson(newPlayerData));

            //GameEvents.OnPlayerDataSet?.Invoke(newPlayerData);       // eg. set UI
        }
    }

    private void SkipRegisterPlayer()
    {
        GameEvents.OnSkipRegisterPlayer?.Invoke();      // hide PlayerPanel
    }

    private bool PlayerExists(string playerName)
    {
        return playerNames.Contains(playerName);      // ie. false if already exists
    }

    private void OnPlayersChanged(string playersJson)
    {
        //GameEvents.OnDebugText?.Invoke($"OnPlayersChanged: {playersJson}");
        ParsePlayers(playersJson);      // rebuild validation list
    }

    // deserialize json and populate playerNames validation list
    private void ParsePlayers(string json)
    {
        playerNames = new();
        JSONNode node = JSON.Parse(json);

        foreach (KeyValuePair<string, JSONNode> value in node)
        {
            string playerName = value.Key;
            //JSONNode playerNode = value.Value;

            playerNames.Add(playerName);
        }

        playerNames = playerNames.OrderBy(i => i).ToList();
        playersLoaded = true;
    }

    // firebase callbacks

    private void OnSchoolsChanged(string schools)
    {
        ParseSchools(schools);      // rebuild validation list
    }

    // deserialize json and populate dropdown
    private void ParseSchools(string json)
    {
        schoolNames = new();
        JSONNode node = JSON.Parse(json);

        foreach (KeyValuePair<string, JSONNode> value in node)
        {
            string schoolName = value.Key;
            //JSONNode schoolNode = value.Value;

            schoolNames.Add(schoolName);
        }

        schoolNames = schoolNames.OrderBy(i => i).ToList();
        schoolNames.Insert(0, "");      // blank option at top (default no school)

        SchoolDropdown.ClearOptions();
        SchoolDropdown.AddOptions(schoolNames);
        schoolsLoaded = true;
    }

    private bool PasswordExists(string password)
    {
        return passwords.Contains(password);      // ie. false if already exists
    }

    private void OnPasswordsChanged(string passwordsJson)
    {
        ParsePasswords(passwordsJson);      // rebuild validation list
    }

    //private bool ValidatePassword()
    //{
    //    if (!passwordsLoaded)
    //    {
    //        FeedbackText.text = "Password data loading...";
    //        return false;
    //    }

    //    password = PasswordInput.text = Utility.FirebaseClean(PasswordInput.text);       // replace / and . with _

    //    if (string.IsNullOrEmpty(password))
    //    {
    //        FeedbackText.text = "Please enter a Name for the Leaderboard!";
    //        return false;
    //    }

    //    if (PasswordExists(password))
    //    {
    //        FeedbackText.text = "Password '" + password + "' is already in use!";
    //        PasswordInput.text = "";
    //        return false;
    //    }

    //    if (string.IsNullOrEmpty(password))
    //    {
    //        FeedbackText.text = "Please enter a Leaderboard Password";
    //        return false;
    //    }

    //    FeedbackText.text = "Please confirm your Password before saving...\n" + "[ " + password + " ]";
    //    return true;
    //}

    // deserialize json and populate passwords validation list
    private void ParsePasswords(string json)
    {
        passwords = new();
        JSONNode node = JSON.Parse(json);

        foreach (KeyValuePair<string, JSONNode> value in node)
        {
            string password = value.Key;
            //JSONNode passwordNode = value.Value;

            passwords.Add(password);
        }

        passwordsLoaded = true;
    }
}

