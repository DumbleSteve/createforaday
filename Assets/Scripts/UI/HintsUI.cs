using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HintsUI : MonoBehaviour
{
    public Button UpButton;
    public Button LeftButton;
    public Button RightButton;


    private void OnEnable()
    {
        UpButton.onClick.AddListener(UpButtonClicked);
        LeftButton.onClick.AddListener(LeftButtonClicked);
        RightButton.onClick.AddListener(RightButtonClicked);
    }

    private void OnDisable()
    {
        UpButton.onClick.RemoveListener(UpButtonClicked);
        LeftButton.onClick.RemoveListener(LeftButtonClicked);
        RightButton.onClick.RemoveListener(RightButtonClicked);
    }

    private void UpButtonClicked()
    {
        
    }

    private void LeftButtonClicked()
    {
        
    }

    private void RightButtonClicked()
    {
        
    }
}
