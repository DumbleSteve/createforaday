using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// button and text for testing firestore async calls

public class DebugUI : MonoBehaviour
{
    public TextMeshProUGUI DebugText;
    public Button DebugButton1;
    public Button DebugButton2;
    public string DebugInput1 = "Jam";
    public string DebugInput2 = "Obergon";

    public LeaderboardUI leaderboard;       // TEST

    public void OnEnable()
    {
        GameEvents.OnDebugText += OnDebugText;

        GameEvents.OnFireStoreError += OnFirestoreError;
        GameEvents.OnFireStoreInfo += OnFirestoreInfo;

        DebugButton1.onClick.AddListener(DebugClick1);
        DebugButton2.onClick.AddListener(DebugClick2);
    }

    public void OnDisable()
    {
        GameEvents.OnDebugText -= OnDebugText;

        GameEvents.OnFireStoreError -= OnFirestoreError;
        GameEvents.OnFireStoreInfo -= OnFirestoreInfo;

        DebugButton1.onClick.RemoveListener(DebugClick1);
        DebugButton2.onClick.RemoveListener(DebugClick2);
    }

    private void DebugClick1()
    {
        GameEvents.OnFireStoreGetPassword?.Invoke(DebugInput1);
    }


    private void DebugClick2()
    {
        GameEvents.OnFireStoreGetPassword?.Invoke(DebugInput2);
    }

    private void OnFirestoreError(string error)
    {
        DebugText.color = Color.red;
        DebugText.text = error;
    }

    private void OnFirestoreInfo(string info)
    {
        DebugText.color = Color.yellow;
        DebugText.text = info;
    }


    private void OnDebugText(string text)
    {
        DebugText.color = Color.white;
        DebugText.text += "\n" + text;
    }
}
