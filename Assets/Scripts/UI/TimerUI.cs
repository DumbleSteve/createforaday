using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class TimerUI : MonoBehaviour
{
    public TextMeshProUGUI timerText;
    public TextMeshProUGUI bestTimeText;       // PB
    public TextMeshProUGUI topTimeText;        // leaderboard #1

    private bool pulsing = false;
    private float pulseFactor = 1.5f;
    private float pulseTime = 0.2f;

    private void OnEnable()
    {
        GameEvents.OnGameTimer += OnGameTimer;
        GameEvents.OnNewBestTime += OnNewBestTime; 
    }

    private void OnDisable()
    {
        GameEvents.OnGameTimer -= OnGameTimer;
        GameEvents.OnNewBestTime -= OnNewBestTime;
    }


    private void OnGameTimer(int gameHundredths)
    {
        timerText.text = Utility.FormatTime(gameHundredths);
    }

    private void OnNewBestTime(GameData gameData, int newBestTime, bool onWin, bool newPlayer)
    {
        bestTimeText.text = Utility.FormatTime(newBestTime);
    }

    public void SetTopTime(int bestTime)
    {
        topTimeText.text = Utility.FormatTime(bestTime);
        PulseTopTime();
    }

    private void PulseTopTime()
    {
        if (pulsing)
            return;

        pulsing = true;
        var startScale = topTimeText.transform.localScale;

        LeanTween.scale(topTimeText.gameObject, startScale * pulseFactor, pulseTime)
                    .setEaseOutQuint()
                    .setLoopPingPong(1)
                    .setOnComplete(() => pulsing = false);
    }
}
