using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


[RequireComponent(typeof(Button))]

public class LevelButton : MonoBehaviour
{
    public TextMeshProUGUI levelNameText;
    public TextMeshProUGUI creditsText;
    public TextMeshProUGUI bestTimeText;

    public TextMeshProUGUI playTimeText;        // total - from db
    public TextMeshProUGUI playCountText;       // total - from db

    private GameData gameData;
    private Button levelButton;

    private int levelIndex;     // in gameLibrary

    public string LevelName => gameData.GameName;


    private void Awake()
    {
        levelButton = GetComponent<Button>();
    }

    private void OnEnable()
    {
        GameEvents.OnFireStoreLevelStatsChanged += OnLevelStatsChanged;      // any level - only update this button if its level changed

        // each level button starts a listener for db changes to its level stats
        GameEvents.OnFireStoreStartListenLevelStatsChanged?.Invoke(LevelName);

        levelButton.onClick.AddListener(LevelSelected);
    }

    private void OnDisable()
    {
        GameEvents.OnFireStoreLevelStatsChanged -= OnLevelStatsChanged;

        GameEvents.OnFireStoreStopListenLevelStatsChanged?.Invoke(LevelName);

        levelButton.onClick.RemoveListener(LevelSelected);
    }

    public void Init(GameData data, int gameIndex)
    {
        gameData = data;
        levelIndex = gameIndex;
        levelNameText.text = LevelName;
        creditsText.text = data.SchoolName;
    }

    public void SetColour(Color colour)
    {
        GetComponent<Image>().color = colour;
    }


    private void SetStats(LevelStats stats)
    {
        playCountText.text = "Played: " + stats.PlayCount + " (" + Utility.FormatHoursMins(stats.PlayTime) + ")";
        playTimeText.text = "Time: " + Utility.FormatHoursMins(stats.PlayTime);
        bestTimeText.text = Utility.FormatTime(stats.BestTime);
    }

    private void LevelSelected()
    {
        GameEvents.OnLevelSelected?.Invoke(gameData, levelIndex);       // load scene
    }

    // change event fired on any level changed, so only update this button if it's level was changed
    private void OnLevelStatsChanged(string json)
    {
        LevelStats stats = JsonUtility.FromJson<LevelStats>(json);

        if (stats.LevelName == LevelName)
            SetStats(stats);
    }
}
