using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StartUI : MonoBehaviour
{
    public Image Logo;
    public TextMeshProUGUI Byron;

    public float FadeInTime = 1.0f;
    public float ByronTime = 0.75f;      // delay before shown


    private void Start()
    {
        // fade in logo
        LeanTween.value(gameObject, Color.clear, Color.white, FadeInTime)
            .setEaseOutQuad()
            .setOnUpdate((Color c) => Logo.color = c);

        // show 'byron bay' after a delay
        Byron.gameObject.SetActive(false);

        LeanTween.value(gameObject, 0f, 1f, ByronTime)
                    //.setDelay(ByronTime)
                    .setOnComplete(() => Byron.gameObject.SetActive(true));
    }
}
