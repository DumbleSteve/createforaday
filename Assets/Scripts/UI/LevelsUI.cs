using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.Linq;


public class LevelsUI : MonoBehaviour
{
    public LevelButton LevelButtonPrefab;
    public Transform ScrollContent;

    public Color CurrentLevelColour;

    private List<GameData> gameLevels;
    private List<LevelButton> levelButtons;

    //private float fillInterval = 0.1f;

    //private void OnEnable()
    //{
        //GameEvents.OnNewBestTime += OnNewBestTime;
    //}

    //private void OnDisable()
    //{
        //GameEvents.OnNewBestTime -= OnNewBestTime;
    //}

    // PopulateLevels after loading saved data into all gameLibrary games
    // fill ScrollContent with buttons
    public void PopulateLevels(List<GameData> levels, bool clear)
    {
        gameLevels = levels;

        if (clear)
            ClearLevels();

        //StartCoroutine(FillLevels(levels));

        levelButtons = new();

        for (int i = 0; i < levels.Count; i++)
        {
            GameData level = levels[i];
            //GameEvents.OnDebugText?.Invoke($"PopulateLevels: {level.GameName} {level.saveData.BestTime}");

            LevelButton newButton = Instantiate(LevelButtonPrefab, ScrollContent);
            newButton.Init(level, i);

            levelButtons.Add(newButton);
        }
    }

    //private IEnumerator FillLevels(List<GameData> levels)
    //{
    //    levelButtons = new();

    //    for (int i = 0; i < levels.Count; i++)
    //    {
    //        GameData level = levels[i];
    //        //GameEvents.OnDebugText?.Invoke($"FillLevels: {level.GameName} {level.saveData.BestTime}");

    //        LevelButton newButton = Instantiate(LevelButtonPrefab, ScrollContent);
    //        newButton.Init(level, i);

    //        levelButtons.Add(newButton);

    //        yield return new WaitForSeconds(fillInterval);
    //    }

    //    yield return null;
    //}

    private void ClearLevels()
    {
        foreach (Transform level in ScrollContent)
        {
            Destroy(level.gameObject);
        }
    }

    //private void OnNewBestTime(GameData gameData, int newBestTime, bool onWin, bool newPlayer)
    //{
    //    var levelButton = levelButtons.FirstOrDefault(button => button.LevelName == gameData.GameName);

    //    if (levelButton != null)
    //        levelButton.SetBestTime(newBestTime);
    //}

    public void SetCurrentGame(GameData currentGame)
    {
        for (int i = 0; i < gameLevels.Count; i++)
        {
            if (gameLevels[i].GameName == currentGame.GameName)
                levelButtons[i].SetColour(CurrentLevelColour);
            else
                levelButtons[i].SetColour(gameLevels[i].LevelColour);
        }
    }
}
