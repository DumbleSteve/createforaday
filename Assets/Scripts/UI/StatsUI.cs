using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StatsUI : MonoBehaviour
{
    public TextMeshProUGUI statsText;

    public void SetStatsText(string stats)
    {
        statsText.text = stats;
    }
}
