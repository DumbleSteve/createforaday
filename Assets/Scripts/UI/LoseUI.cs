using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LoseUI : MonoBehaviour
{
    public TextMeshProUGUI tryAgainText;
    public TextMeshProUGUI gameTimeText;
    public TextMeshProUGUI totalTimeText;

    public Button continueButton;

    public List<string> TryAgain;
    public string RandomTryAgain => TryAgain.Count > 0 ? TryAgain[Random.Range(0, TryAgain.Count)] : "Try Again!";

    private string lastTryAgain;

    private void OnEnable()
    {
        GameEvents.OnMoveKeyPress += OnMoveKeyPress;    // space (up) key == continue

        continueButton.onClick.AddListener(ContinueClicked);
    }

    private void OnDisable()
    {
        GameEvents.OnMoveKeyPress -= OnMoveKeyPress;

        continueButton.onClick.RemoveListener(ContinueClicked);
    }

    // space (jump) key == continue
    private void OnMoveKeyPress(Vector2 direction, bool down)
    {
        if (down && direction == Vector2.up)    // space bar
            ContinueClicked();
    }

    private void ContinueClicked()
    {
        GameEvents.OnReloadLevel?.Invoke();
    }

    public void SetGameTime(int gameHundredths, int totalHundredths)
    {
        string tryAgain;
        while ((tryAgain = RandomTryAgain) == lastTryAgain) { }

        tryAgainText.text = lastTryAgain = tryAgain;
        gameTimeText.text = "Time: " + Utility.FormatTime(gameHundredths);
        totalTimeText.text = "Total Play Time: " + Utility.FormatTime(totalHundredths);
    }
}
