using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameUI : MonoBehaviour
{
    public MenuUI MenuPanel;        // currently just reset button
    public TimerUI TimerPanel;
    public PasswordUI PasswordPanel;
    public WinUI WinPanel;
    public LoseUI LosePanel;
    //public StatsUI StatsPanel;
    public LeaderboardUI LeaderboardPanel;
    public LevelsUI LevelsPanel;
    public CreditsUI CreditsPanel;

    public CanvasGroup HintPanel;

    public Button MenuButton;

    public TextMeshProUGUI playerNameText;
    public TextMeshProUGUI gameNameText;

    private float hintFadeTime = 0.5f;
    private float hintShowTime = 5f;

    private GameData currentGame;


    private void OnEnable()
    {
        GameEvents.OnInitLevels += OnInitLevels;
        GameEvents.OnLevelSelected += OnLevelSelected;

        GameEvents.OnGameStart += OnGameStart;
        GameEvents.OnGameWon += OnGameWon;
        GameEvents.OnNewBestTime += OnNewBestTime;
        GameEvents.OnGameLost += OnGameLost;

        GameEvents.OnFireStorePlayerSet += OnFireStorePlayerSet;
        GameEvents.OnFireStoreLevelStatsChanged += OnLevelStatsChanged;      // any level - handle only if this level changed

        GameEvents.OnSkipPassword += OnSkipPassword;       // hide PasswordUI

        MenuButton.onClick.AddListener(ToggleMenu);
    }

    private void OnDisable()
    {
        GameEvents.OnInitLevels -= OnInitLevels;
        GameEvents.OnLevelSelected -= OnLevelSelected;

        GameEvents.OnGameStart -= OnGameStart;
        GameEvents.OnGameWon -= OnGameWon;
        GameEvents.OnNewBestTime -= OnNewBestTime;
        GameEvents.OnGameLost -= OnGameLost;

        GameEvents.OnFireStorePlayerSet -= OnFireStorePlayerSet;
        GameEvents.OnFireStoreLevelStatsChanged -= OnLevelStatsChanged;      // any level - handle only if this level changed

        GameEvents.OnSkipPassword -= OnSkipPassword;        // hide PasswordUI

        MenuButton.onClick.RemoveListener(ToggleMenu);
    }


    private void Awake()
    {
        playerNameText.text = "";
        gameNameText.text = "";
    }

    // before game start (for levels list in menu UI)
    private void OnInitLevels(List<GameData> levels, GameData currentGame)
    {
        LevelsPanel.PopulateLevels(levels, true);
        LevelsPanel.SetCurrentGame(currentGame);
    }

    private void OnGameStart(GameData gameData, int playCount, bool promptPassword)
    {
        currentGame = gameData;
        gameNameText.text = "- " + gameData.GameName + " -";

        WinPanel.gameObject.SetActive(false);
        LosePanel.gameObject.SetActive(false);
        //StatsPanel.gameObject.SetActive(false);
        MenuPanel.gameObject.SetActive(false);
        LeaderboardPanel.gameObject.SetActive(false);            // populates OnEnable

        if (promptPassword)      // no password until first success or if already skipped!
        {
            ShowPasswordPanel();           // TODO: here?  or full PlayerUI???
        }

        CreditsPanel.SetCredits(gameData.GameName, gameData.TeamName, gameData.SchoolName, gameData.Credits);

        if (playCount == 1)
            ShowHint();               // TODO: re-enable?  keys/buttons or swipe?
    }

    private void OnGameWon(int gameHundredths, int bestHundredths, int bousHundredths, string nextLevelName, GameData game)
    {
        LosePanel.gameObject.SetActive(false);

        WinPanel.gameObject.SetActive(true);
        WinPanel.SetWinText(gameHundredths, bestHundredths, game.levelStats.PlayTime, bousHundredths, nextLevelName);

        LeaderboardPanel.gameObject.SetActive(true);            // populates OnEnable
        GameEvents.OnDebugText?.Invoke($"OnGameWon: leaderboard enabled!");

        //StatsPanel.gameObject.SetActive(true);
        //StatsPanel.SetStatsText(Utility.GameStats(game));
    }


    private void OnNewBestTime(GameData gameData, int newBestTime, bool onWin, bool newPlayer)
    {
        GameEvents.OnDebugText?.Invoke($"OnNewBestTime: onWin: {onWin} newPlayer: {newPlayer} newBestTime: {newBestTime}");

        if (!onWin)
            return;

        WinPanel.gameObject.SetActive(true);        // to make sure, before coroutine starts
        WinPanel.NewPB(newBestTime, true);

        if (newPlayer)
        {
            LeaderboardPanel.NewPlayerPrompt();
        }
    }

    private void OnGameLost(int gameHundredths, GameData game)
    {
        WinPanel.gameObject.SetActive(false);

        LosePanel.gameObject.SetActive(true);
        LosePanel.SetGameTime(gameHundredths, game.levelStats.PlayTime);

        //StatsPanel.gameObject.SetActive(true);
        //StatsPanel.SetStatsText(Utility.GameStats(game));
    }

    private void OnLevelSelected(GameData gameData, int gameIndex)
    {
        MenuPanel.gameObject.SetActive(false);
        LeaderboardPanel.gameObject.SetActive(false);
        GameEvents.OnMenuShowing?.Invoke(false);
    }

    // change event fired on any level changed, so only update top time if the current game was changed
    private void OnLevelStatsChanged(string json)
    {
        LevelStats stats = JsonUtility.FromJson<LevelStats>(json);

        if (stats.LevelName == currentGame.GameName)
            TimerPanel.SetTopTime(stats.BestTime);
    }

    // after validation of password or new player registration
    private void OnFireStorePlayerSet(PlayerData playerData)
    {
        playerNameText.text = Utility.FullPlayerName(playerData.PlayerName, playerData.SchoolName, false);
        HidePasswordPanel();
    }

    private void OnSkipPassword()
    {
        HidePasswordPanel();
    }

    private void ShowPasswordPanel()
    {
        var passwordScaler = PasswordPanel.GetComponent<Scaler>();

        if (passwordScaler != null)
            passwordScaler.ScaleUpX();
        else
            PasswordPanel.gameObject.SetActive(true);
    }

    private void HidePasswordPanel()
    {
        var passwordScaler = PasswordPanel.GetComponent<Scaler>();

        if (passwordScaler != null)
            passwordScaler.ScaleDownX();
        else
            PasswordPanel.gameObject.SetActive(false);
    }

    private void OnPlayerDataSavedToFile(PlayerData playerData)
    {
        playerNameText.text = Utility.FullPlayerName(playerData.PlayerName, playerData.SchoolName, false);
    }

    private void OnPlayerDataLoadedFromFile(PlayerData playerData)
    {
        playerNameText.text = Utility.FullPlayerName(playerData.PlayerName, playerData.SchoolName, false);
    }

    private void ToggleMenu()
    {
        MenuPanel.gameObject.SetActive(! MenuPanel.gameObject.activeSelf);
        LeaderboardPanel.gameObject.SetActive(MenuPanel.gameObject.activeSelf);

        GameEvents.OnMenuShowing?.Invoke(MenuPanel.gameObject.activeSelf);
        GameEvents.OnGamePause?.Invoke(MenuPanel.gameObject.activeSelf);
    }

    private void ShowHint()
    {
        HintPanel.alpha = 0f;
        HintPanel.gameObject.SetActive(true);

        LeanTween.value(HintPanel.gameObject, 0f, 1f, hintFadeTime)
                    .setOnUpdate((float f) => HintPanel.alpha = f)
                    //.setOnUpdate(UpdateHintPanelAlpa)     // does same as above
                    .setEaseOutQuad()
                    .setOnComplete(() =>
                    {
                        LeanTween.value(HintPanel.gameObject, 1f, 0f, hintFadeTime)
                                    .setDelay(hintShowTime)
                                    .setOnUpdate((float f) => HintPanel.alpha = f)
                                    .setEaseInQuad()
                                    .setOnComplete(() => HintPanel.gameObject.SetActive(false));
                    });
    }

    //private void UpdateHintPanelAlpha(float f)
    //{
    //    HintPanel.alpha = f;
    //}
}
