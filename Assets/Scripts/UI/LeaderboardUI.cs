using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using SimpleJSON;

public class LeaderboardUI : MonoBehaviour
{
    public PlayerUI PlayerPanel;            // player name and school input
    public Transform ScrollContent;         // children are LeaderboardButtons

    public int TopN = 100;                  // limits entries in leaderboard

    [Space]
    public LeaderboardButton LeaderboardButtonPrefab;

    [Header("Leaderboard Colours")]
    public Color GoldColour;
    public Color SilverColour;
    public Color BronzeColour;
    public Color EntryColour;

    private bool fillingBoard;


    private void OnEnable()
    {
        GameEvents.OnFireStoreBestTimesChanged += OnBestTimesChanged;    // populate leaderboard
        GameEvents.OnFireStorePlayerSet += OnPlayerSet;                  // saved via PlayerUI

        GameEvents.OnSkipRegisterPlayer += OnSkipRegisterPlayer;       // hide PlayerUI

        // start listening for db changes to repopulate
        GameEvents.OnFireStoreStartListenBestTimesChanged?.Invoke();
    }

    private void OnDisable()
    {
        GameEvents.OnFireStoreBestTimesChanged -= OnBestTimesChanged;
        GameEvents.OnFireStorePlayerSet -= OnPlayerSet;                  // saved via PlayerUI

        GameEvents.OnSkipRegisterPlayer -= OnSkipRegisterPlayer;       // hide PlayerUI

        // stop listening for db changes
        GameEvents.OnFireStoreStopListenBestTimesChanged?.Invoke();
    }

    public void NewPlayerPrompt()
    {
        // prompt for new player name / password / school input
        ShowPlayerPanel();
    }

    private void OnSkipRegisterPlayer()
    {
        HidePlayerPanel();
    }

    // player saved to firestore db
    private void OnPlayerSet(PlayerData playerData)
    {
        HidePlayerPanel();
    }

    private void ShowPlayerPanel()
    {
        var playerScaler = PlayerPanel.GetComponent<Scaler>();

        if (playerScaler != null)
            playerScaler.ScaleUpX();
        else
            PlayerPanel.gameObject.SetActive(true);
    }

    private void HidePlayerPanel()
    {
        var playerScaler = PlayerPanel.GetComponent<Scaler>();

        if (playerScaler != null)
            playerScaler.ScaleDownX();
        else
            PlayerPanel.gameObject.SetActive(false);
    }

    private void OnBestTimesChanged(string bestTimes)
    {
        ClearEntryButtons();
        PopulateLeaderboard(bestTimes, true);
    }

    // Populate leaderboard after retrieving firestore json data.
    // Deserialize it into a LeaderboardEntry list and fill ScrollContent with buttons
    private void PopulateLeaderboard(string bestTimesJson, bool clear)
    {
        if (fillingBoard)
            return;

        List<LeaderboardEntry> entries = ParseBestTimes(bestTimesJson);
        StartCoroutine(FillLeaderboard(entries, clear));
    }

    // assumes entries are sorted in ascending rank order
    private IEnumerator FillLeaderboard(List<LeaderboardEntry> entries, bool clear)
    {
        if (fillingBoard)
            yield break;

        fillingBoard = true;

        if (clear)
            ClearEntryButtons();

        int rank = 1;

        foreach (LeaderboardEntry entry in entries)
        {
            //Debug.Log($"PopulateLeaderboard: level: {entry.GameName} player: {entry.PlayerName} school: {entry.SchoolName} bestTime: {entry.BestTime}");

            LeaderboardButton newButton = Instantiate(LeaderboardButtonPrefab, ScrollContent);
            newButton.Init(entry, rank, (rank == 1 ? GoldColour : (rank == 2 ? SilverColour : (rank == 3 ? BronzeColour : EntryColour))));
            rank++;

            yield return new WaitForSeconds(newButton.scaleUpTime);
        }

        fillingBoard = false;
        yield return null;
    }

    // OMG! Why was deserializing json such a f***ing nightmare??!
    private List<LeaderboardEntry> ParseBestTimes(string json)
    {
        List<LeaderboardEntry> levelPlayers = new();
        JSONNode node = JSON.Parse(json);

        foreach (KeyValuePair<string, JSONNode> value in node)
        {
            string playerName = value.Key;
            JSONNode entryNode = value.Value;

            var entry = new LeaderboardEntry(entryNode["GameName"], entryNode["PlayerName"], entryNode["SchoolName"], entryNode["BestTime"]);
            levelPlayers.Add(entry);
        }

        return levelPlayers.OrderBy(i => i.BestTime).Take(TopN).ToList();
    }

    private void ClearEntryButtons()
    {
        foreach (Transform entry in ScrollContent)
        {
            Destroy(entry.gameObject);
        }
    }
}
