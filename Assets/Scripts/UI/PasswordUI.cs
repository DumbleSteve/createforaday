using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;
using SimpleJSON;


public class PasswordUI : MonoBehaviour
{
    public TMP_InputField PasswordInput;
    public TextMeshProUGUI FeedbackText;

    public Button ConfirmButton;            // to check password
    public Button SkipButton;               // stay as 'new player'

    private string password;                // as entered

//    private void Awake()
//    {
//#if !UNITY_EDITOR && UNITY_WEBGL
//        WebGLInput.captureAllKeyboardInput = false;
//#endif
//    }

    private void OnEnable()
    {
#if !UNITY_EDITOR && UNITY_WEBGL
        WebGLInput.captureAllKeyboardInput = false;
#endif

        GameEvents.OnFireStorePasswordNotFound += OnFireStorePasswordNotFound;
  
        ConfirmButton.onClick.AddListener(ConfirmPassword);
        SkipButton.onClick.AddListener(SkipPassword);

        ConfirmButton.gameObject.SetActive(true);               // to check if password is valid
        ConfirmButton.interactable = true;
        FeedbackText.text = "";
    }

    private void OnDisable()
    {
        GameEvents.OnFireStorePasswordNotFound -= OnFireStorePasswordNotFound;

        ConfirmButton.onClick.RemoveListener(ConfirmPassword);
        SkipButton.onClick.RemoveListener(SkipPassword);

#if !UNITY_EDITOR && UNITY_WEBGL
        WebGLInput.captureAllKeyboardInput = true;
#endif
    }


    private bool ValidatePassword()
    {
        password = PasswordInput.text = Utility.FirebaseClean(PasswordInput.text);       // replace / and . with _

        if (string.IsNullOrEmpty(password))
        {
            FeedbackText.text = "Please enter your Leaderboard Access Key";
            return false;
        }

        // lookup in firestore db (and get PlayerData)
        FeedbackText.text = "Checking Access Key...";
        GameEvents.OnFireStoreGetPassword?.Invoke(password);
        return true;
    }

    // on 'confirm' button click...
    private void ConfirmPassword()
    {
        ConfirmButton.interactable = false;

        if (! ValidatePassword())
        {
            ConfirmButton.interactable = true;
        }
    }

    private void SkipPassword()
    {
        GameEvents.OnSkipPassword?.Invoke();        // hide this UI, stay as 'new player' (no PB or leaderboard entries)
    }

    // firebase callbacks

    private void OnFireStorePasswordNotFound()
    {
        FeedbackText.text = "Access Key '" + password + "' is not in use!";
        PasswordInput.text = "";
        ConfirmButton.interactable = true;
    }

    // NOTE: this UI hidden by GameUI if password found in DB
}

