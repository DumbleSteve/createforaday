using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scaler : MonoBehaviour
{
    public float ScaleTime = 0.25f;

    private float scaleX;
    private float scaleY;

    public void ScaleUpX()
    {
        scaleX = transform.localScale.x;

        transform.localScale = new Vector2(0f, transform.localScale.y);
        gameObject.SetActive(true);

        LeanTween.scaleX(gameObject, scaleX, ScaleTime)
                    .setEaseOutBack();
    }

    public void ScaleDownX()
    {
        LeanTween.scaleX(gameObject, 0f, ScaleTime)
                    .setEaseInQuart()
                    .setOnComplete(() =>
                    {
                        gameObject.SetActive(false);
                        transform.localScale = new Vector2(scaleX, transform.localScale.y);
                    });
    }

    public void ScaleUpY()
    {
        scaleY = transform.localScale.y;

        transform.localScale = new Vector2(transform.localScale.x, 0f);
        gameObject.SetActive(true);

        LeanTween.scaleY(gameObject, scaleX, ScaleTime)
                    .setEaseOutBack();
    }

    public void ScaleDownY()
    {
        LeanTween.scaleY(gameObject, 0f, ScaleTime)
                    .setEaseInQuart()
                    .setOnComplete(() =>
                    {
                        gameObject.SetActive(false);
                        transform.localScale = new Vector2(transform.localScale.x, scaleY);
                    });
    }
}
