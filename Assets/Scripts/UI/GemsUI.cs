using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class GemsUI : MonoBehaviour
{
    public TextMeshProUGUI gemValueText;
    public TextMeshProUGUI timeBonusText;

    private bool pulsing = false;
    private Vector2 startScale;
    private float pulseScale = 1.25f;
    private float pulseTime = 0.1f;

    private void OnEnable()
    {
        GameEvents.OnTotalGemValueChanged += OnTotalGemValueChanged;
    }

    private void OnDisable()
    {
        GameEvents.OnTotalGemValueChanged -= OnTotalGemValueChanged;
    }

    private void Awake()
    {
        startScale = timeBonusText.transform.localScale;
    }

    private void OnTotalGemValueChanged(float collectedGemValue, float totalCollectibleValue, int bonusHundredths)
    {
        gemValueText.text = (int)collectedGemValue + " / " + (int)totalCollectibleValue;
        timeBonusText.text = $"Bonus:  {Utility.FormatTime(bonusHundredths)}";

        Pulse();
    }

    private void Pulse()
    {
        if (pulsing)
            return;

        pulsing = true;

        LeanTween.scale(timeBonusText.gameObject, startScale * pulseScale, pulseTime)
                    .setEaseOutCubic()
                    .setOnComplete(() =>
                    {
                        LeanTween.scale(timeBonusText.gameObject, startScale, pulseTime)
                                    .setEaseInCubic()
                                    .setOnComplete(() => pulsing = false);
                    });
    }
}
