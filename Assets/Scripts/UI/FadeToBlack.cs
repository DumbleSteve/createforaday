using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// Event-driven class to fade the 'blackout' UI image to black (could be any colour) using a LeanTween curve.
// Calls the given 'onFade' function when blacked out (eg. switch scene)

public class FadeToBlack : MonoBehaviour
{
    public Image blackout;
    public Color blackoutColour = Color.black;

    public float fadeTime = 1.25f;

    private void OnEnable()
    {
        GameEvents.OnFadeOut += OnFadeOut;
        GameEvents.OnFadeIn += OnFadeIn;
    }

    private void OnDisable()
    {
        GameEvents.OnFadeOut -= OnFadeOut;
        GameEvents.OnFadeIn -= OnFadeIn;
    }

    private void Awake()
    {
        blackout.color = blackoutColour;       // just to make sure!
        blackout.enabled = false;
    }


    // the optional 'onFade' Action (reference to a function) will typically be used to change to another scene
    private void OnFadeOut(Action onFade, bool toClear)
    {
        // fade from clear to black
        blackout.color = Color.clear;       // just to make sure!
        blackout.enabled = true;

        // fade to black, call optional onFade function on complete, optionally fade back to clear
        LeanTween.value(gameObject, Color.clear, blackoutColour, fadeTime)
                    .setEaseInQuad()
                    .setOnUpdate((Color c) => blackout.color = c)
                    .setOnComplete(() =>
                    {
                        onFade?.Invoke();

                        if (toClear)
                            FadeToClear(null);
                    });
    }

    private void OnFadeIn(Action onFade)
    {
        // fade from black to clear
        FadeToClear(onFade);
    }

    private void FadeToClear(Action onFade)
    {
        blackout.color = blackoutColour;       // just to make sure!
        blackout.enabled = true;

        LeanTween.value(gameObject, blackoutColour, Color.clear, fadeTime)
                    .setEaseInQuad()
                    .setOnUpdate((Color c) => blackout.color = c)
                    .setOnComplete(() =>
                    {
                        onFade?.Invoke();
                        blackout.enabled = false;
                    });
    }
}
