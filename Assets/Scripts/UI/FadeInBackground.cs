using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeInBackground : MonoBehaviour
{
    public Image Background;
    public float FadeTime = 1f;

    private Color bkgColour;

    private void Awake()
    {
        bkgColour = Background.color;
    }

    public void OnEnable()
    {
        FadeIn();
    }

    private void FadeIn()
    {
        // fade from clear
        Background.color = Color.clear;

        // fade to black, call optional onFade function on complete, optionally fade back to clear
        LeanTween.value(gameObject, Color.clear, bkgColour, FadeTime)
                    .setEaseInQuad()
                    .setOnUpdate((Color c) => Background.color = c);
    }
}
