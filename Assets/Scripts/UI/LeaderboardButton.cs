using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(Button))]

public class LeaderboardButton : MonoBehaviour
{
    public TextMeshProUGUI rankText;
    public TextMeshProUGUI playerNameText;
    public TextMeshProUGUI schoolNameText;
    public TextMeshProUGUI bestTimeText;

    public AudioClip entryAudio;
    //public AudioManager.ClipTag leaderboardTag = AudioManager.ClipTag.Leaderboard;

    private LeaderboardEntry entry;
    private Button entryButton;

    public float scaleUpTime = 0.15f;

    private float entryScaleY;

    private void Awake()
    {
        entryButton = GetComponent<Button>();
        entryScaleY = transform.localScale.y;
    }

    private void OnEnable()
    {
        entryButton.onClick.AddListener(EntrySelected);
    }

    private void OnDisable()
    {
        entryButton.onClick.RemoveListener(EntrySelected);
    }

    public void Init(LeaderboardEntry entry, int rank, Color colour)
    {
        this.entry = entry;

        rankText.text = rank.ToString();
        playerNameText.text = entry.PlayerName;
        schoolNameText.text = entry.SchoolName;

        SetBestTime(entry.BestTime);
        SetColour(colour);

        ScaleUp();
    }

    private void SetColour(Color colour)
    {
        GetComponent<Image>().color = colour;
    }

    public void SetBestTime(int bestTime)
    {
        bestTimeText.text = Utility.FormatTime(bestTime);
    }

    private void EntrySelected()
    {
        //GameEvents.OnLeaderboardEntrySelected?.Invoke(gameData, levelIndex);       // TODO what if clicked??
    }

    private void ScaleUp()
    {
        transform.localScale = new Vector2(transform.localScale.x, 0f);
        LeanTween.scaleY(gameObject, entryScaleY, scaleUpTime)
                    .setEaseOutQuint()
                    .setOnComplete(() => AudioSource.PlayClipAtPoint(entryAudio, transform.position));
    }
}
