using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(CapsuleCollider2D))]
[RequireComponent(typeof(CharacterController))]

// looks left/right for wall on character collision
public class WallJump : MonoBehaviour
{
    public Transform RayCastOrigin;
    public LayerMask WallLayers;                // to raycast for wall hits

    public float wallDistance = 0.2f;           // left/right raycast
    public float circleCastRadius = 0.5f;       // for against wall
    //private Vector2 colliderSize;             // for capsule raycast

    public enum WallDirection
    {
        None,
        OnLeft,
        OnRight
    }
    public WallDirection AgainstWallDirection { get; private set; }

    public Wall AgainstWall { get; private set; }                 // for wall jump (if not grounded)


    private CharacterController character;      // required
    //private CapsuleCollider2D characterCollider;

    private void Awake()
    {
        character = GetComponent<CharacterController>();
        //characterCollider = GetComponent<CapsuleCollider2D>();

        //colliderSize = characterCollider.size * transform.localScale.x;       // ?? strange size..!
    }

    private void OnEnable()
    {
        GameEvents.OnCharacterGrounded += OnCharacterGrounded;
    }

    private void OnDisable()
    {
        GameEvents.OnCharacterGrounded -= OnCharacterGrounded;
    }

    // keep checking for wall hit while against wall
    private void Update()
    {
        if (AgainstWall == null)
            return;

        switch (AgainstWallDirection)
        {
            case WallDirection.OnLeft:
                RaycastForWall(WallDirection.OnLeft);
                break;

            case WallDirection.OnRight:
                RaycastForWall(WallDirection.OnRight);
                break;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (character.OnGround)
            return;

        if (AgainstWall != null)
            return;

        if (collision.gameObject.CompareTag("Wall"))
        {
            var wall = collision.gameObject.GetComponent<Wall>();

            if (wall != null && wall.CanWallJump)
                CheckAgainstWall(wall);     // raycast to see if wall is on left or right
        }
    }

    //private void OnCollisionExit2D(Collision2D collision)
    //{
    //    if (collision.gameObject.CompareTag("Wall"))
    //    {
    //        againstLeftWall = false;
    //        againstRightWall = false;
    //    }
    //}

    // raycast to see if wall is on left or right
    private void CheckAgainstWall(Wall wall)
    {
        if (wall.CanJumpRightSide && RaycastForWall(WallDirection.OnLeft))
            return;

        if (wall.CanJumpLeftSide)
             RaycastForWall(WallDirection.OnRight);
    }

    private bool RaycastForWall(WallDirection wallDirection)
    {
        Vector2 raycastDirection = (wallDirection == WallDirection.OnLeft) ? Vector2.left : Vector2.right;
        Wall hitWall = null;

        var hit = Physics2D.CircleCast(RayCastOrigin.position, circleCastRadius, raycastDirection, wallDistance, WallLayers);
        //var hit = Physics2D.CapsuleCast(RayCastOrigin.position, colliderSize, characterCollider.direction, 0f, raycastDirection, wallDistance, WallLayers);

        if (hit.collider != null)           // against a wall
            hitWall = hit.collider.GetComponent<Wall>();

        if (hitWall != null)
        {
            if (AgainstWall != null && hitWall != AgainstWall)      // against a different wall!
                AgainstWall.SetCanJumpColour(false);                // no longer against that wall

            AgainstWall = hitWall;
            AgainstWall.SetCanJumpColour(true);

            AgainstWallDirection = wallDirection;
 
            //Debug.Log($"RaycastForWall: wallDirection = {wallDirection} AgainstWall = {AgainstWall.name} AgainstWallDirection = {AgainstWallDirection}");
        }
        else        // not against a wall
        {
            if (AgainstWall != null)
                AgainstWall.SetCanJumpColour(false);                // no longer against that wall

            AgainstWall = null;
            AgainstWallDirection = WallDirection.None;

            //Debug.Log($"RaycastForWall: AgainstWall = null");
        }

        return hitWall != null;
    }

    private void OnCharacterGrounded(CharacterController character, bool isGrounded, bool wasGrounded, Collider2D groundedOn)
    {
        if (isGrounded && !wasGrounded)
        {
            if (AgainstWall != null)
                AgainstWall.SetCanJumpColour(false);

            AgainstWall = null;
            AgainstWallDirection = WallDirection.None;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(RayCastOrigin.position, circleCastRadius);

        var leftWallPoint = new Vector3(RayCastOrigin.position.x - wallDistance, RayCastOrigin.position.y, RayCastOrigin.position.z);
        Gizmos.DrawLine(RayCastOrigin.position, leftWallPoint);
        Gizmos.DrawWireSphere(leftWallPoint, circleCastRadius);

        var rightWallPoint = new Vector3(RayCastOrigin.position.x + wallDistance, RayCastOrigin.position.y, RayCastOrigin.position.z);
        Gizmos.DrawLine(RayCastOrigin.position, rightWallPoint);
        Gizmos.DrawWireSphere(rightWallPoint, circleCastRadius);
    }
}
