using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class StartScene : MonoBehaviour
{
    public GameLibrary gameLibrary;
    public float SceneDelay = 2f;

    private IEnumerator Start()
    {
        gameLibrary.PasswordSkipCount = 0;              // will prompt on first play

        yield return new WaitForSeconds(SceneDelay);
        SceneManager.LoadScene(gameLibrary.CurrentGame.SceneName);
    }
}
