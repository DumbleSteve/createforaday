
using UnityEngine;
using UnityEngine.Audio;
using System.Linq;

// class to handle OnPlayAudio events fired from anywhere in the scene
// looks up the clip to play from the audio library
// and plays it through the AudioSource provided by the event

public class AudioManager : MonoBehaviour
{
    // reference to audio library SO
    public AudioLibrary audioLibrary;

    // list of possible audio clips, used to lookup the clip to play
    // enum better than string (possible typing errors)
    public enum ClipTag
    {
        CharacterHit,   // platform
        DoorOpen,
        DoorClose,
        DoorLock,       // switch
        DoorUnlock,     // switch
        Faint,          
        Success,        // level completion
        Collectible,    // picked up
        Teleport,
        Bounce,         // bouncy platform
        Laser,          // pitch randomised
        LaserFry,       // hit by laser
        Spawn,          // at start of level
        Footstep,       // while moving (animation event)
        Jump,           // leave ground
        Idle,           // animation state
        Walk,           // animation state
        Run,            // animation state
        Fly,            // animation state
        PlatformGlitch,   // platforms fades then explodes
        PlatformDegraded,   // platforms fades then explodes
    }

    [Header("Random Pitch Range")]
    public float pitchMin = 0.75f;
    public float pitchMax = 1.5f;
    private float RandomPitch => Random.Range(pitchMin, pitchMax);

    //// audio mixer
    //[Header("Audio Mixer")]
    //public AudioMixer Mixer;
    //public float DefaultFadeTime = 1f;


    public void OnEnable()
    {
        GameEvents.OnPlayAudio += OnPlayAudio;     // listen for event

        //GameEvents.OnFadeAudio += OnFadeAudio;
        //GameEvents.OnCrossFadeAudio += OnCrossFadeAudio;
    }

    public void OnDisable()
    {
        GameEvents.OnPlayAudio -= OnPlayAudio;     // stop listening

        //GameEvents.OnFadeAudio -= OnFadeAudio;
        //GameEvents.OnCrossFadeAudio -= OnCrossFadeAudio;
    }


    // event handler to play audio clips
    private void OnPlayAudio(AudioSource source, ClipTag clipTag, bool randomPitch, float pitchOverride = 0f, bool checkForChange = false)
    {
        //Debug.Log($"OnPlayAudio: {clipTag} {randomPitch} {pitchOverride}");

        if (source == null)
        {
            Debug.LogError($"OnPlayAudio: '{source.gameObject.name}' has no AudioSource!");
            return;
        }

        // lookup the clip in the audio library
        GameAudioClip libraryClip = audioLibrary.audioClips.FirstOrDefault(clip => clip.ClipTag == clipTag);

        // set the audio source properties from the library clip and play it
        if (libraryClip != null)
        {
            if ( checkForChange && source.clip == libraryClip.Clip)        // no change in audio clip!
                return;

            source.clip = libraryClip.Clip;
            source.loop = libraryClip.Loop;

            source.volume = libraryClip.Volume > 0 ? libraryClip.Volume : 1f;

            float libraryPitch = source.pitch = libraryClip.Pitch > 0 ? libraryClip.Pitch : 1f;

            if (pitchOverride > 0)
                source.pitch = pitchOverride;
            else if (randomPitch)
                source.pitch = libraryPitch * RandomPitch;
            else
                source.pitch = libraryPitch;

            source.Play();
        }
        else
        {
            // clip tag not used in this scene
            //Debug.LogError($"OnPlayAudio: '{clipTag}' not found in audio library!");
        }
    }

    //// mixer

    //private void OnFadeAudio(string mixerParam, float toVolume, float fadeTime)
    //{
    //    Fade(mixerParam, toVolume, fadeTime);
    //}

    //private void OnCrossFadeAudio(string outParam, string inParam, float fadeTime)
    //{
    //    CrossFade(outParam, inParam, fadeTime);
    //}

    //private void SetMasterVolume(float volume, float fadeTime)
    //{
    //    Fade("MusicVolume", volume, fadeTime);
    //}

    //private void SetSFXVolume(float volume, float fadeTime)
    //{
    //    Fade("SFXVolume", volume, fadeTime);
    //}

    //// fades mixerParam to toVolume in fadeTime seconds
    //private void Fade(string mixerParam, float toVolume, float fadeTime)
    //{
    //    Mixer.GetFloat(mixerParam, out float currentVolume);

    //    LeanTween.value(currentVolume, toVolume, fadeTime > 0 ? fadeTime : DefaultFadeTime)
    //                .setOnUpdate((float v) => Mixer.SetFloat(mixerParam, v))
    //                .setEaseInOutQuad();
    //}

    //// fades outParam out and inParam in to outParam volume
    //private void CrossFade(string outParam, string inParam, float fadeTime)
    //{
    //    Mixer.GetFloat(outParam, out float fadeVolume);

    //    Fade(outParam, 0f, fadeTime);
    //    Fade(inParam, fadeVolume, fadeTime);
    //}
}
