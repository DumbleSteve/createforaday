using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// data for GameLibrary list, to assign a tag to each clip in the scene

[System.Serializable]
public class GameAudioClip
{
    public AudioManager.ClipTag ClipTag;       // lookup key
    public AudioClip Clip;                      // audio to be played

    public bool Loop = false;

    [Range(0f, 1f)]
    public float Volume = 1f;

    [Range(0.75f, 1.5f)]
    public float Pitch = 1f;
}
