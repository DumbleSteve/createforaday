using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Audio Library", fileName = "Audio Library")]

// scriptable object (data asset) used as a central repository for all audio clips in the scene
// for looking up clips when audio play events are fired by objects that have an AudioSource

public class AudioLibrary : ScriptableObject
{
    public List<GameAudioClip> audioClips;
}
