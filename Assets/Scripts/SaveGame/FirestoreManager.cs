
using UnityEngine;
using FirebaseWebGL.Examples.Utils;
using FirebaseWebGL.Scripts.FirebaseBridge;
using FirebaseWebGL.Scripts.Objects;
using System.Collections.Generic;
using System;


// Event-driven manager for all Firestore database interaction and callbacks

public class FirestoreManager : MonoBehaviour
{
    public GameLibrary gameLibrary;         // to get current level (game) and player names

    private const string levelsPath = "/Levels/";
    private const string bestTimes = "/BestTimes/";

    private const string schoolsPath = "/Schools/";
    private const string playersPath = "/Players/";
    private const string passwordsPath = "/Passwords/";

    private const string noWebGLMessage = "FirestoreManager is not running on a WebGL build\nThe Javascript SDK functions will not be recognized";

    private string currentGameName => gameLibrary.CurrentGame.GameName;
    private string currentBestTimesPath => levelsPath + currentGameName + bestTimes;
    private string playerName => gameLibrary.playerData.PlayerName;
    private string schoolName => gameLibrary.playerData.SchoolName;

    public static bool IsWebGL { get; private set; }

    private int newBestTime;

    private PlayerData playerToSet;             // cache until player set callback received
    private LeaderboardEntry bestTimeToSet;     // cache until best time set callback received

    private void OnEnable()
    {
        GameEvents.OnFireStoreSetPlayerLevelBestTime += OnSetPlayerLevelBestTime;      // BestTimeSet callback event   
        GameEvents.OnFireStoreGetBestTimes += OnGetBestTimes;    // BestTimesRetrieved callback event

        GameEvents.OnFireStoreGetSchools += OnGetSchools;        // SchoolsRetrieved callback event

        GameEvents.OnFireStoreGetPlayers += OnGetPlayers;        // PlayersRetrieved callback event
        //GameEvents.OnFireStoreGetPlayer += OnGetPlayer;          // PlayerRetrieved callback event
        GameEvents.OnFireStoreSetPlayer += OnSetPlayer;          // PlayerSet callback event

        GameEvents.OnFireStoreGetPassword += OnGetPassword;        // PasswordRetrieved callback event gets associated PlayerData  
        GameEvents.OnFireStoreGetPasswords += OnGetPasswords;        // PasswordsRetrieved callback event
        GameEvents.OnFireStoreStartListenPasswordsChanged += OnStartListenPasswordsChanged;
        GameEvents.OnFireStoreStopListenPasswordsChanged += OnStopListenPasswordsChanged;

        GameEvents.OnFirestoreSetPlayerLevelStats += SetPlayerLevelStats;   // PlayerLevelStatsSet callback

        //GameEvents.OnFireStoreGetLevelStats += OnGetLevelStats;  // LevelStatsRetrieved callback event
        GameEvents.OnFireStoreGetPlayerLevelStats += OnGetPlayerLevelStats;          // PlayerLevelStatsRetrieved callback event

        GameEvents.OnFireStoreIncrementLevelStats += OnIncrementLevelStats;
        GameEvents.OnFireStoreIncrementPlayerLevelStats += OnIncrementPlayerLevelStats;

        GameEvents.OnFireStoreIncrementPlayerPB += OnIncrementPlayerPB;

        GameEvents.OnFireStoreStartListenBestTimesChanged += OnStartListenBestTimesChanged;
        GameEvents.OnFireStoreStopListenBestTimesChanged += OnStopListenBestTimesChanged;

        GameEvents.OnFireStoreStartListenSchoolsChanged += OnStartListenSchoolsChanged;
        GameEvents.OnFireStoreStopListenSchoolsChanged += OnStopListenSchoolsChanged;

        GameEvents.OnFireStoreStartListenPlayersChanged += OnStartListenPlayersChanged;
        GameEvents.OnFireStoreStopListenPlayersChanged += OnStopListenPlayersChanged;

        GameEvents.OnFireStoreStartListenLevelStatsChanged += OnStartListenLevelStatsChanged;
        GameEvents.OnFireStoreStopListenLevelStatsChanged += OnStopListenLevelStatsChanged;
    }

    private void OnDisable()
    {
        GameEvents.OnFireStoreSetPlayerLevelBestTime -= OnSetPlayerLevelBestTime;
        GameEvents.OnFireStoreGetBestTimes -= OnGetBestTimes;

        GameEvents.OnFireStoreGetSchools -= OnGetSchools;

        GameEvents.OnFireStoreGetPlayers -= OnGetPlayers;        // PlayersRetrieved callback event
        //GameEvents.OnFireStoreGetPlayer -= OnGetPlayer;
        GameEvents.OnFireStoreSetPlayer -= OnSetPlayer;

        GameEvents.OnFireStoreGetPassword -= OnGetPassword;        // PasswordRetrieved callback event
        GameEvents.OnFireStoreGetPasswords -= OnGetPasswords;        // PasswordsRetrieved callback event
        GameEvents.OnFireStoreStartListenPasswordsChanged -= OnStartListenPasswordsChanged;
        GameEvents.OnFireStoreStopListenPasswordsChanged -= OnStopListenPasswordsChanged;

        GameEvents.OnFirestoreSetPlayerLevelStats -= SetPlayerLevelStats; 

        //GameEvents.OnFireStoreGetLevelStats -= OnGetLevelStats;
        GameEvents.OnFireStoreGetPlayerLevelStats -= OnGetPlayerLevelStats;          // PlayerLevelStatsRetrieved callback event

        GameEvents.OnFireStoreIncrementLevelStats -= OnIncrementLevelStats;
        GameEvents.OnFireStoreIncrementPlayerPB -= OnIncrementPlayerPB;

        GameEvents.OnFireStoreStartListenBestTimesChanged -= OnStartListenBestTimesChanged;
        GameEvents.OnFireStoreStopListenBestTimesChanged -= OnStopListenBestTimesChanged;

        GameEvents.OnFireStoreStartListenSchoolsChanged -= OnStartListenSchoolsChanged;
        GameEvents.OnFireStoreStopListenSchoolsChanged -= OnStopListenSchoolsChanged;

        GameEvents.OnFireStoreStartListenPlayersChanged -= OnStartListenPlayersChanged;
        GameEvents.OnFireStoreStopListenPlayersChanged -= OnStopListenPlayersChanged;

        GameEvents.OnFireStoreStartListenLevelStatsChanged -= OnStartListenLevelStatsChanged;
        GameEvents.OnFireStoreStopListenLevelStatsChanged -= OnStopListenLevelStatsChanged;
    }


    private void Awake()
    {
        IsWebGL = Application.platform == RuntimePlatform.WebGLPlayer;

        if (!IsWebGL)
            WebGLError();
    }

    // firestore api functions

    // best times collection for current level 
    private void GetBestTimes() => FirebaseFirestore.GetDocumentsInCollection(currentBestTimesPath, gameObject.name, "BestTimesRetrieved", "DisplayError");

    // best times for current level
    private void ListenForBestTimesChange() => FirebaseFirestore.ListenForCollectionChange(currentBestTimesPath, false, gameObject.name, "BestTimesChanged", "DisplayError");
    private void StopListeningForBestTimesChange() => FirebaseFirestore.StopListeningForCollectionChange(currentBestTimesPath, gameObject.name, "DisplayInfo", "DisplayError");

    // save best time for current level and player
    private void SetPlayerLevelBestTime(string bestTimeData) => FirebaseFirestore.SetDocument(currentBestTimesPath, playerName, bestTimeData, gameObject.name, "BestTimeSet", "DisplayError");
    //private void SetPlayerLevelBestTime(string levelStatsData) => FirebaseFirestore.SetDocument(playersPath + playerName + levelsPath, currentGameName, levelStatsData, gameObject.name, "PlayerBestTimeSet", "DisplayError");

    // increment stats for player and current level
    private void IncrementPlayerLevelStats(bool playCount, bool successCount, bool failCount, int gameHundredths)
    {
        if (gameLibrary.IsNewPlayer)        // no player name set yet
            return;

        if (playCount)
        {
            FirebaseFirestore.IncrementFieldValue(playersPath + playerName + levelsPath, currentGameName, "PlayCount", 1, gameObject.name, "PlayCountIncremented", "DisplayError");
        }
        if (successCount)
        {
            FirebaseFirestore.IncrementFieldValue(playersPath + playerName + levelsPath, currentGameName, "SuccessCount", 1, gameObject.name, "SuccessCountIncremented", "DisplayError");
        }
        if (failCount)
        {
            FirebaseFirestore.IncrementFieldValue(playersPath + playerName + levelsPath, currentGameName, "FailCount", 1, gameObject.name, "FailCountIncremented", "DisplayError");
        }

        if (gameHundredths > 0)
        {
            FirebaseFirestore.IncrementFieldValue(playersPath + playerName + levelsPath, currentGameName, "PlayTime", gameHundredths, gameObject.name, "PlayTimeIncremented", "DisplayError");
        }
    }

    // best time for current level and player
    //private void ListenForBestTimeChange() => FirebaseFirestore.ListenForDocumentChange(currentBestTimesPath, playerName, false, gameObject.name, "BestTimeChanged", "DisplayError");
    //private void StopListeningForBestTimeChange() => FirebaseFirestore.StopListeningForDocumentChange(currentBestTimesPath, playerName, gameObject.name, "DisplayInfo", "DisplayError");

    //// stats for all levels
    //private void GetLevels() => FirebaseFirestore.GetDocumentsInCollection(levelsPath, gameObject.name, "LevelsRetrieved", "DisplayError");

    // stats for a given level
    private void GetLevelStats(string levelName) => FirebaseFirestore.GetDocument(levelsPath, levelName, gameObject.name, "LevelStatsRetrieved", "DisplayError");

    // stats (PB) for current player, all levels
    private void GetPlayerLevelStats() => FirebaseFirestore.GetDocumentsInCollection(playersPath + playerName + levelsPath, gameObject.name, "PlayerLevelStatsRetrieved", "NoPlayerLevelStatsFound");

    // set stats for current player and level
    private void SetPlayerLevelStats(LevelStats newPlayerLevelStats)
    {
        newPlayerLevelStats.LevelName = currentGameName;
        newPlayerLevelStats.PlayTime = newPlayerLevelStats.BestTime;

        string levelStatsJson = JsonUtility.ToJson(newPlayerLevelStats);

        FirebaseFirestore.SetDocument(playersPath + playerName + levelsPath, currentGameName, levelStatsJson, gameObject.name, "PlayerLevelStatsSet", "DisplayError");
    }

    // increment stats for current level
    private void IncrementLevelStats(bool playCount, bool successCount, bool failCount, int gameHundredths)
    {
        if (playCount)
        {
            FirebaseFirestore.IncrementFieldValue(levelsPath, currentGameName, "PlayCount", 1, gameObject.name, "PlayCountIncremented", "DisplayError");
        }
        if (successCount)
        {
            FirebaseFirestore.IncrementFieldValue(levelsPath, currentGameName, "SuccessCount", 1, gameObject.name, "SuccessCountIncremented", "DisplayError");
        }
        if (failCount)
        {
            FirebaseFirestore.IncrementFieldValue(levelsPath, currentGameName, "FailCount", 1, gameObject.name, "FailCountIncremented", "DisplayError");
        }

        if (gameHundredths > 0)
        {
            FirebaseFirestore.IncrementFieldValue(levelsPath, currentGameName, "PlayTime", gameHundredths, gameObject.name, "PlayTimeIncremented", "DisplayError");
        }
    }

    // decrement best time to set new best time
    private void SetPlayerPersonalBest(int timeIncrement)
    {
        GameEvents.OnDebugText?.Invoke($"SetPlayerPersonalBest: timeIncrement: {timeIncrement} playerName {playerName}");

        if (!gameLibrary.IsNewPlayer)        // must have player name set
        {
            FirebaseFirestore.IncrementFieldValue(playersPath + playerName + levelsPath, currentGameName, "BestTime", timeIncrement, gameObject.name, "PlayerPBIncremented", "DisplayError");
        }
    }

    // set stats for current level
    private void SetLevelStats(string levelStatsData) => FirebaseFirestore.SetDocument(levelsPath, currentGameName, levelStatsData, gameObject.name, "LevelStatsSet", "DisplayError");

    // listen for changes to a given level's stats
    private void ListenForLevelStatsChange(string levelName) => FirebaseFirestore.ListenForDocumentChange(levelsPath, levelName, false, gameObject.name, "LevelStatsChanged", "DisplayError");
    private void StopListeningForLevelStatsChange(string levelName) => FirebaseFirestore.StopListeningForDocumentChange(levelsPath, levelName, gameObject.name, "DisplayInfo", "DisplayError");


    // all schools
    private void GetSchools() => FirebaseFirestore.GetDocumentsInCollection(schoolsPath, gameObject.name, "SchoolsRetrieved", "DisplayError");

    // listen for school changes to sync validation list
    private void ListenForSchoolsChange() => FirebaseFirestore.ListenForCollectionChange(schoolsPath, false, gameObject.name, "SchoolsChanged", "DisplayError");
    private void StopListeningForSchoolsChange() => FirebaseFirestore.StopListeningForCollectionChange(schoolsPath, gameObject.name, "DisplayInfo", "DisplayError");


    // all players
    private void GetPlayers() => FirebaseFirestore.GetDocumentsInCollection(playersPath, gameObject.name, "PlayersRetrieved", "DisplayError");

    // listen for player changes to sync validation list
    private void ListenForPlayersChange() => FirebaseFirestore.ListenForCollectionChange(playersPath, false, gameObject.name, "PlayersChanged", "DisplayError");
    private void StopListeningForPlayersChange() => FirebaseFirestore.StopListeningForCollectionChange(playersPath, gameObject.name, "DisplayInfo", "DisplayError");

    // get player
    private void GetPlayer(string name) => FirebaseFirestore.GetDocument(playersPath, name, gameObject.name, "PlayerRetrieved", "NoPlayer");

    // save player
    private void SetPlayer(string name, string playerJson) => FirebaseFirestore.SetDocument(playersPath, name, playerJson, gameObject.name, "PlayerSet", "DisplayError");
    private void SetPassword(string password, string playerJson) => FirebaseFirestore.SetDocument(passwordsPath, password, playerJson, gameObject.name, "PasswordSet", "DisplayError");

    // player password (for WebGL leaderboard)
    private void GetPasswords() => FirebaseFirestore.GetDocumentsInCollection(passwordsPath, gameObject.name, "PasswordsRetrieved", "DisplayError");
    //private void SetPlayerPassword(string password, string playerName) => FirebaseFirestore.SetDocument(passwordsPath, password, playerName, gameObject.name, "PasswordSet", "DisplayError");
    private void GetPasswordPlayer(string password) => FirebaseFirestore.GetDocument(passwordsPath, password, gameObject.name, "PasswordRetrieved", "PasswordNotFound");

    // listen for passwords changes to sync validation list
    private void ListenForPasswordsChange() => FirebaseFirestore.ListenForCollectionChange(passwordsPath, false, gameObject.name, "PasswordsChanged", "DisplayError");
    private void StopListeningForPasswordsChange() => FirebaseFirestore.StopListeningForCollectionChange(passwordsPath, gameObject.name, "DisplayInfo", "DisplayError");

    #region passwords

    // callback on successful pasword collection retrieval
    private void PasswordsRetrieved(string passwordsJson)
    {
        GameEvents.OnDebugText?.Invoke("PasswordsRetrieved: " + passwordsJson);
        GameEvents.OnFireStorePasswordsRetrieved?.Invoke(passwordsJson);      // TODO:?
    }

    // callback on players collection changed
    public void PasswordsChanged(string passwordsJson)
    {
        GameEvents.OnDebugText?.Invoke("PasswordsChanged: " + passwordsJson);
        GameEvents.OnFireStorePasswordsChanged?.Invoke(passwordsJson);
    }

    // callback on successful player and password set
    private void PasswordSet(string message)
    {
        GameEvents.OnDebugText?.Invoke($"PasswordSet: message: {message}");

        if (playerToSet != null)
        {
            GameEvents.OnFireStorePlayerSet?.Invoke(playerToSet);           // hide new player panel & show player and school in UI
            playerToSet = null;
        }
    }

    // callback on successful password get
    private void PasswordRetrieved(string playerDataJson)
    {
        if (string.IsNullOrEmpty(playerDataJson) || playerDataJson == "null")           // strange way of handling firestore document not found...
        {
            GameEvents.OnFireStorePasswordNotFound?.Invoke();
        }
        else
        {
            GameEvents.OnDebugText?.Invoke($"PasswordRetrieved: {playerDataJson}");
            PlayerData playerData = JsonUtility.FromJson<PlayerData>(playerDataJson);       // TODO: simple json to get PlayerData??
            GameEvents.OnFireStorePlayerSet?.Invoke(playerData);           // hide new player panel & show player and school in UI
        }
    }

    // fallback on unsuccessful password get
    private void PasswordNotFound(string message)
    {
        GameEvents.OnFireStorePasswordNotFound?.Invoke(); 
    }

    #endregion

    #region best times

    // get best times from firestore for current level and player
    private void OnGetBestTimes()
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        GetBestTimes();
    }

    // callback on successful best time document collection retrieval
    private void BestTimesRetrieved(string bestTimeJson)
    {
        //GameEvents.OnDebugText?.Invoke("BestTimesRetrieved: " + bestTimeJson);
        GameEvents.OnFireStoreBestTimesRetrieved?.Invoke(bestTimeJson);
    }

    // save best time to firestore for current level and player
    private void OnSetPlayerLevelBestTime(int bestTime)
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        bestTimeToSet = new LeaderboardEntry(currentGameName, playerName, schoolName, bestTime);
        SetPlayerLevelBestTime(JsonUtility.ToJson(bestTimeToSet));         // saved as new best time for current player and level

        //SetPlayerLevelBestTime(JsonUtility.ToJson(playerLevelStats));   // saved as new best time for current player and level
    }

    //private void OnSetBestTime(LevelStats playerLevelStats)
    //{
    //    if (!IsWebGL)
    //    {
    //        WebGLError();
    //        return;
    //    }

    //    bestTimeToSet = new LeaderboardEntry(currentGameName, playerName, schoolName, playerLevelStats.BestTime);
    //    SetBestTime(JsonUtility.ToJson(bestTimeToSet));         // only saved if best time is better than current leaderbaord #1

    //    // TODO: ????????
    //    SetPlayerLevelBestTime(JsonUtility.ToJson(playerLevelStats));   // saved as new best time for current player and level
    //}

    // callback on successful best time document set
    private void BestTimeSet(string message)
    {
        if (bestTimeToSet != null)
        {
            //GameEvents.OnBestTimeSet?.Invoke(message);

            // save best time, get level stats for the level and update if new best time is better than top 
            newBestTime = bestTimeToSet.BestTime;
            GetLevelStats(bestTimeToSet.GameName);    // LevelStatsRetrieved callback

            bestTimeToSet = null;
        }
    }

    //private void PlayerBestTimeSet(string message)
    //{
        //GameEvents.OnPlayerBestTimeSet?.Invoke(message);
    //}

    // callback on best times collection changed
    public void BestTimesChanged(string timesCollection)
    {
        GameEvents.OnFireStoreBestTimesChanged?.Invoke(timesCollection);
    }

    // callback on best time document changed
    public void BestTimeChanged(string bestTimeData)
    {
        //GameEvents.OnBestTimeChanged?.Invoke(bestTimeData);
    }

    // start/stop listening for changes in best times for current level
    private void OnStartListenBestTimesChanged()
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        ListenForBestTimesChange();
    }

    private void OnStopListenBestTimesChanged()
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        StopListeningForBestTimesChange();
    }

    #endregion


    #region level stats

    // leaderboard stats (?)
    //private void OnGetLevelStats(string levelName)
    //{
    //    if (!IsWebGL)
    //    {
    //        WebGLError();
    //        return;
    //    }

    //    GetLevelStats(levelName);       // LevelStatsRetrieved callback
    //}


    // stats(PB) for current player, all levels
    private void OnGetPlayerLevelStats()
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        GetPlayerLevelStats();          // PlayerLevelStatsRetrieved
    }


    // callback on successful levels collection retrieval
    //private void LevelsRetrieved(string levelsJson)
    //{
    //    GameEvents.OnDebugText?.Invoke("LevelsRetrieved: " + levelsJson);
    //    GameEvents.OnLevelsRetrieved?.Invoke(levelsJson);
    //}

    // callback on successful level stats document retrieval
    private void LevelStatsRetrieved(string levelStatsJson)
    {
        if (!string.IsNullOrEmpty(levelStatsJson) && levelStatsJson != "null" && newBestTime > 0)
        {
            // check if newBestTime is better than level's current top time
            var levelStats = JsonUtility.FromJson<LevelStats>(levelStatsJson);
            if (levelStats.BestTime == 0 || newBestTime < levelStats.BestTime)
            {
                // update and save level stats with new top time
                levelStats.BestTime = newBestTime;
                SetLevelStats(JsonUtility.ToJson(levelStats));
            }

            newBestTime = 0;
        }
    }

    // callback on successful player level stats collection retrieval
    private void PlayerLevelStatsRetrieved(string playerLevelStatsJson)
    {
        GameEvents.OnFireStorePlayerLevelStatsRetrieved?.Invoke(playerLevelStatsJson);       // start game
    }

    // fallback on unsuccessful player level stats collection retrieval
    private void NoPlayerLevelStatsFound(string message)
    {
        GameEvents.OnFireStoreNoPlayerLevelStats?.Invoke();                                  // start game
    }

    private void OnIncrementLevelStats(bool play, bool success, bool fail, int gameHundredths)
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        IncrementLevelStats(play, success, fail, gameHundredths);
    }

    private void OnIncrementPlayerLevelStats(bool play, bool success, bool fail, int gameHundredths)
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        IncrementPlayerLevelStats(play, success, fail, gameHundredths);
    }

    private void OnIncrementPlayerPB(int timeIncrement)
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        // IncrementFieldValue (actually decrements)
        SetPlayerPersonalBest(timeIncrement);
    }

    // callback on successful level stats document set
    private void LevelStatsSet(string message)
    {
        // TODO: fire event?
    }

    // callback on successful player level stats document set
    private void PlayerLevelStatsSet(string message)
    {
        // TODO: fire event?
    }

    private void PlayCountIncremented(string message)
    {
        // TODO: fire event?
    }

    private void SuccessCountIncremented(string message)
    {
        // TODO: fire event?
    }

    private void FailCountIncremented(string message)
    {
        // TODO: fire event?
    }

    private void PlayTimeIncremented(string message)
    {
        // TODO: fire event?
    }

    private void PlayerPBIncremented(string message)
    {
        // TODO: fire event?
    }


    private void OnStartListenLevelStatsChanged(string levelName)
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        ListenForLevelStatsChange(levelName);
    }

    private void OnStopListenLevelStatsChanged(string levelName)
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        StopListeningForLevelStatsChange(levelName);
    }

    // callback for level stats changed listener
    private void LevelStatsChanged(string levelStatsJson)
    {
        //GameEvents.OnDebugText?.Invoke("LevelStatsChanged: " + levelStatsJson);
        GameEvents.OnFireStoreLevelStatsChanged?.Invoke(levelStatsJson);
    }

    #endregion


    #region schools

    private void OnGetSchools()
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        GetSchools();
    }

    // callback on successful schools document collection retrieval
    private void SchoolsRetrieved(string schoolsCollection)
    {
        GameEvents.OnFireStoreSchoolsRetrieved?.Invoke(schoolsCollection);
    }

    // callback on schools collection changed
    public void SchoolsChanged(string schoolsCollection)
    {
        GameEvents.OnFireStoreSchoolsChanged?.Invoke(schoolsCollection);
    }

    // start/stop listening for changes in schools
    private void OnStartListenSchoolsChanged()
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        ListenForSchoolsChange();
    }

    private void OnStopListenSchoolsChanged()
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        StopListeningForSchoolsChange();
    }

    #endregion


    #region players

    private void OnGetPlayer(string playerName)
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        GetPlayer(playerName);
    }

    // stats for current player and level
    //private void OnGetPlayerLevelStats()
    //{
    //    if (!isWebGL)
    //    {
    //        WebGLError();
    //        return;
    //    }

    //    GetPlayerLevelStats();
    //}

    // callback on successful player get
    private void PlayerRetrieved(string playerJson)
    {
        //if (playerJson == null)
        //{
        //    GameEvents.OnFireStoreNoPlayerLevelStats?.Invoke();
        //}
        //else
        //{
            //PlayerData player = JsonUtility.FromJson<PlayerData>(playerJson);
            //GameEvents.OnPlayerRetrieved?.Invoke(player);
        //}
    }

    private void OnSetPlayer(string playerName, string schoolName, string password, string playerData)
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        playerToSet = JsonUtility.FromJson<PlayerData>(playerData);

        // set player and password documents
        SetPlayer(playerName, playerData);
        SetPassword(password, playerData);
    }

    // callback on successful player set
    private void PlayerSet(string message)
    {
        GameEvents.OnDebugText?.Invoke($"PlayerSet: {playerToSet} message: {message}");
    }

    // fallback on unsuccessful player document retrieval
    private void NoPlayer(string player)
    {
        GameEvents.OnFireStoreNoPlayer?.Invoke(player);
    }

    private void OnGetPlayers()
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        GetPlayers();
    }

    // callback on successful players document collection retrieval
    private void PlayersRetrieved(string playersCollection)
    {
        GameEvents.OnFireStorePlayersRetrieved?.Invoke(playersCollection);
    }

    // callback on players collection changed
    public void PlayersChanged(string playersCollection)
    {
        GameEvents.OnFireStorePlayersChanged?.Invoke(playersCollection);
    }

    // start/stop listening for changes in players
    private void OnStartListenPlayersChanged()
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        ListenForPlayersChange();
    }

    private void OnStopListenPlayersChanged()
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        StopListeningForPlayersChange();
    }

    #endregion

    #region passwords

    private void OnGetPassword(string password)
    {
        GameEvents.OnDebugText?.Invoke($"OnGetPassword: {password}");
        GetPasswordPlayer(password);
    }

    private void OnGetPasswords()
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        GetPasswords();
    }

    // start/stop listening for changes in passwords
    private void OnStartListenPasswordsChanged()
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        ListenForPasswordsChange();
    }

    private void OnStopListenPasswordsChanged()
    {
        if (!IsWebGL)
        {
            WebGLError();
            return;
        }

        StopListeningForPasswordsChange();
    }

    #endregion


    #region errors

    private void DisplayError(string error)
    {
        var parsedError = StringSerializationAPI.Deserialize(typeof(FirebaseError), error) as FirebaseError;
        GameEvents.OnFireStoreError?.Invoke(parsedError.message);
    }

    private void DisplayInfo(string info)
    {
        var parsedInfo = StringSerializationAPI.Deserialize(typeof(FirebaseError), info) as FirebaseError;
        GameEvents.OnFireStoreInfo?.Invoke(parsedInfo.message);
    }

    private void WebGLError()
    {
        GameEvents.OnFireStoreError?.Invoke(noWebGLMessage);
    }

    #endregion
}
