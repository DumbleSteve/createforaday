
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;


// class to handle file input/output of saved data
// responds to events from within game

public class SaveManager : MonoBehaviour
{
    private string fileName = "SAEByronGameJam1";
    private string PlayerFilePath => FirestoreManager.IsWebGL ? ("idbfs/" + fileName) : Application.persistentDataPath + "/" + fileName + ".dat";

    private void OnEnable()
    {
        GameEvents.OnLoadPlayerDataFromFile += OnLoadPlayerDataFromFile;    // player data loaded at start of game
        GameEvents.OnSavePlayerDataToFile += OnSavePlayerDataToFile;

        //GameEvents.OnLoadAllGameData += OnLoadAllGameData;  // data loaded at start of game
        //GameEvents.OnSaveGameData += OnSaveGameData;        // data saved on game over (or after new hi-score)
        //GameEvents.OnResetGameData += OnResetGameData;      // data deleted and recreated
    }

    private void OnDisable()
	{
        GameEvents.OnLoadPlayerDataFromFile -= OnLoadPlayerDataFromFile;    // player data loaded at start of game
        GameEvents.OnSavePlayerDataToFile -= OnSavePlayerDataToFile;

        //GameEvents.OnLoadAllGameData -= OnLoadAllGameData;        // data loaded at start of game
        //GameEvents.OnSaveGameData -= OnSaveGameData;
        //GameEvents.OnResetGameData -= OnResetGameData;
	}

    // loads json from player file into given PlayerData object
    private void OnLoadPlayerDataFromFile(out PlayerData playerData)
    {
        if (!File.Exists(PlayerFilePath))
        {
            GameEvents.OnDebugText?.Invoke($"OnLoadPlayerJsonFromFile: no file: {PlayerFilePath}");
            playerData = new PlayerData();
            return;
        }

        string json = File.ReadAllText(PlayerFilePath);
        playerData = JsonUtility.FromJson<PlayerData>(json);
        GameEvents.OnDebugText?.Invoke($"OnLoadPlayerJsonFromFile: file: {PlayerFilePath}  player: {playerData.PlayerName} school: {playerData.SchoolName}");

        GameEvents.OnPlayerDataLoadedFromFile?.Invoke(playerData);      // can now set player & school in UI
    }

    // saves player data to file as a json string
    private void OnSavePlayerDataToFile(PlayerData playerData)
    {
        string json = JsonUtility.ToJson(playerData);
        File.WriteAllText(PlayerFilePath, json);
        GameEvents.OnDebugText?.Invoke($"OnSavePlayerJsonToFile: player: {playerData.PlayerName} school: {playerData.SchoolName}");

        GameEvents.OnPlayerDataSavedToFile?.Invoke(playerData);         // show player name and school in UI
    }

    //// saves player data to file as a json string
    //private void SavePlayerJsonToFile(PlayerData playerData)
    //{
    //    string json = JsonUtility.ToJson(playerData);
    //    File.WriteAllText(PlayerFilePath, json);
    //    GameEvents.OnDebugText?.Invoke($"SavePlayerJsonToFile: player: {playerData.PlayerName} school: {playerData.SchoolName}");

    //    GameEvents.OnPlayerDataSavedToFile?.Invoke(playerData);
    //}

    //// loads json from player file into given PlayerData class
    //private void LoadPlayerJsonFromFile(out PlayerData playerData)
    //{
        //if (!File.Exists(PlayerFilePath))
        //{
        //    playerData = new PlayerData("", "");
        //    return;
        //}

        //string json = File.ReadAllText(PlayerFilePath);
        //playerData = JsonUtility.FromJson<PlayerData>(json);
        //GameEvents.OnDebugText?.Invoke($"LoadPlayerJsonFromFile: player: {playerData.PlayerName} school: {playerData.SchoolName}");

        //GameEvents.OnPlayerDataLoadedFromFile?.Invoke(playerData);      // can now set in UI
    //}


    //  private void OnLoadGameData(string gameName, out SaveData saveData)
    //  {
    //		LoadJsonFromFile(gameName, out saveData);
    //  }

    //private void OnLoadAllGameData(List<GameData> games)
    //{
    //	foreach (var game in games)
    //	{
    //		LoadJsonFromFile(game.GameName, out game.saveData);
    //		//Debug.Log($"OnLoadAllGameData: {game.GameName} {game.saveData.BestTime}");
    //	}

    //	GameEvents.OnAllGameDataLoaded?.Invoke(games);		// can now populate LevelUI
    //}

    //private void OnSaveGameData(SaveData saveData)
    //   {
    //	SaveGameJsonToFile(saveData);
    //}

    //private void OnResetGameData(string gameName)
    //{
    //	ResetGameData(gameName);
    //}

    // saves data to file as a json string
    //private void SaveGameJsonToFile(SaveData data)
    //{
    //	string json = JsonUtility.ToJson(data);
    //	string filePath = FileDir + data.GameName + ".dat";
    //	File.WriteAllText(filePath, json);
    //}

    // loads json from file into given SaveData class
    //private void LoadJsonFromFile(string gameName, out SaveData data)
    //{
    //	string filePath = FileDir + gameName + ".dat";

    //	if (! File.Exists(filePath))
    //	{
    //		data = new SaveData(gameName); //, playerName);
    //		SaveGameJsonToFile(data);
    //	}
    //	else
    //	{
    //		string json = File.ReadAllText(filePath);
    //		data = JsonUtility.FromJson<SaveData>(json);
    //	}

    //	//Debug.Log($"LoadJsonFromFile: {gameName} {data.BestTime}");
    //}

    //// overwrites with empty data
    //private void ResetGameData(string gameName)
    //{
    //	SavePlayerJsonToFile(new PlayerData("", ""));
    //	SaveGameJsonToFile(new SaveData(gameName));
    //	//Debug.Log($"Data reset {gameName}.dat complete!");
    //}
}
