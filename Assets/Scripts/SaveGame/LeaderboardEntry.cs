﻿
using System;


// simple structure used for firestore leaderboard (best times)

[Serializable]
public class LeaderboardEntry
{
    public string GameName;         // level
    public string PlayerName;  
    public string SchoolName; 
    public int BestTime;            // player PB - hundredths
    //public DateTime TimeStamp;

    public LeaderboardEntry(string gameName, string playerName, string schoolName, int bestTime)
    {
        GameName = gameName;        // level
        PlayerName = playerName;
        SchoolName = schoolName;
        BestTime = bestTime;
        //TimeStamp = DateTime.Now;
    }
}
