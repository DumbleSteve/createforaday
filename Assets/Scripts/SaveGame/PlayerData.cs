﻿


// simple structure used for player profile data (saved to file and firestore)

[System.Serializable]
public class PlayerData
{
    public string PlayerName;       // for leaderboard, set from PlayerPrefs for convenience
    public string SchoolName;       // for leaderboard, set from PlayerPrefs for convenience
    public string Password;         // to lookup player in db

    public PlayerData(string playerName, string schoolName, string password)
    {
        PlayerName = playerName;
        SchoolName = schoolName;
        Password = password;
    }

    public PlayerData()
    {
        Reset();
    }

    public void Reset()
    {
        PlayerName = "";
        SchoolName = "";
        Password = "";
    }
}
