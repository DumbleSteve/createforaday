using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SaveDataTest : MonoBehaviour
{
    private string TestPlayerName = "Cyril";
    private string TestSchoolName = "SAEByron";
    private string TestPassword = "Obergon";

    public bool FirestoreSave = false;
    public bool PlayerPrefsSave = false;
    public bool FileSave = true;
    public bool FileLoad = true;

    private void OnEnable()
    {
        //if (FileSave)
        {
            GameEvents.OnPlayerDataLoadedFromFile += OnPlayerDataLoadedFromFile;
            GameEvents.OnPlayerDataSavedToFile += OnPlayerDataSavedToFile;
        }
    }

    private void OnDisable()
    {
        //if (FileSave)
        {
            GameEvents.OnPlayerDataLoadedFromFile -= OnPlayerDataLoadedFromFile;
            GameEvents.OnPlayerDataSavedToFile -= OnPlayerDataSavedToFile;
        }
    }

    private void Start()
    {
        // save player to firestore db
        if (FirestoreSave)
        {
            var newPlayerData = new PlayerData(TestPlayerName, TestSchoolName, TestPassword);
            GameEvents.OnFireStoreSetPlayer?.Invoke(TestPlayerName, TestSchoolName, TestPassword, JsonUtility.ToJson(newPlayerData));
        }

        // save player to firestore db
        if (FileSave)
        {
            var newPlayerData = new PlayerData(TestPlayerName, TestSchoolName, TestPassword);
            GameEvents.OnDebugText?.Invoke($"FileSave: player: {newPlayerData.PlayerName} school: {newPlayerData.SchoolName}");
            GameEvents.OnSavePlayerDataToFile?.Invoke(newPlayerData);
        }

        if (FileLoad)
        {
            var newPlayerData = new PlayerData();
            GameEvents.OnDebugText?.Invoke($"FileLoad");
            GameEvents.OnLoadPlayerDataFromFile?.Invoke(out newPlayerData);
        }

        // persist player name and school
        if (PlayerPrefsSave)
        {
            PlayerPrefs.SetString("PlayerName", TestPlayerName);
            PlayerPrefs.SetString("SchoolName", TestSchoolName);
            PlayerPrefs.Save();     // reqd for webGL!!

            GameEvents.OnDebugText?.Invoke($"Save PlayerPrefs: player: {PlayerPrefs.GetString("PlayerName")}  school: {PlayerPrefs.GetString("SchoolName")}");
        }
    }


    private void OnPlayerDataLoadedFromFile(PlayerData playerData)
    {
        GameEvents.OnDebugText?.Invoke($"OnPlayerDataLoadedFromFile: player: {playerData.PlayerName}  school: {playerData.SchoolName}");
    }

    private void OnPlayerDataSavedToFile(PlayerData playerData)
    {
        GameEvents.OnDebugText?.Invoke($"OnPlayerDataSavedToFile: player: {playerData.PlayerName}  school: {playerData.SchoolName}");

        // load back again
        var newPlayerData = new PlayerData();
        GameEvents.OnDebugText?.Invoke($"OnPlayerDataSavedToFile: FileLoad");
        GameEvents.OnLoadPlayerDataFromFile?.Invoke(out newPlayerData);
    }
}
