﻿

// simple structure used for game/level play stats data saved to firebase
// saved as a summary for all players against each level, including leaderboard #1
// and also for each player for each level, as that player's best time, play count, etc.

[System.Serializable]
public class LevelStats
{
    public string LevelName;

    public int BestTime;            // current leaderboard #1 / single player PB

    public int PlayTime;            // time played, aggregate total of all players or a single player, updated on level success / fail
    public int PlayCount;           // no. of times new game started
    public int SuccessCount;        // no. of times game successfully completed (through exit door)
    public int FailCount;           // no. of times game failed (character fainted)

    public int QuitCount => PlayCount - SuccessCount - FailCount;       // ie. started but didn't finish

    public void Reset()
    {
        BestTime = 0;

        PlayTime = 0;
        PlayCount = 0;
        SuccessCount = 0;
        FailCount = 0;
    }
}