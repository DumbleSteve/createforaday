using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// moves back and forth according to x & y distances, move time and pause time

public class Oscillate : MonoBehaviour
{
    public float xDistance = 0f;
    public float yDistance = 0.5f;

    public float oscillateTime = 2f;
    public float oscillatePause = 1f;
    private int oscillateTweenId = 0;

    public bool characterActivated = false;     // oscillate only while characterGrounded on platform
    private bool characterGrounded = false;     // on this platform

    private Vector2 startPosition;
    private Vector2 finishPosition;

    private Platform parentPlatform;             // for characterActivated platforms - sprite alpha reduced while paused

    private BoxCollider2D parentCollider;         // for gizmo
    private Vector2 startTopLeft;
    private Vector2 startTopRight;
    private Vector2 startBottomLeft;
    private Vector2 startBottomRight;
    private Vector2 finishTopLeft;
    private Vector2 finishTopRight;
    private Vector2 finishBottomLeft;
    private Vector2 finishBottomRight;

    private void OnEnable()
    {
        GameEvents.OnGameStart += OnGameStart;
        GameEvents.OnCharacterGrounded += OnCharacterGrounded;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStart -= OnGameStart;
        GameEvents.OnCharacterGrounded -= OnCharacterGrounded;
    }

    private void Awake()
    {
        startPosition = transform.position;
        finishPosition = startPosition + new Vector2(xDistance, yDistance);

        parentCollider = GetComponent<BoxCollider2D>();

        if (parentCollider != null)
            SetGizmoBounds();
    }

    private void Start()
    {
        // reduce platform alpha while paused (not grounded)
        if (characterActivated)
        {
            parentPlatform = GetComponent<Platform>();
            if (parentPlatform != null)
                parentPlatform.SetPaused(true);
        }
    }

    private void OnGameStart(GameData gameData, int playCount, bool promptPassword)
    {
        DoOscillate();

        if (characterActivated && !characterGrounded)
            LeanTween.pause(oscillateTweenId);
    }

    private void DoOscillate()
    {
        if (finishPosition == (Vector2)transform.position)
            return;

        oscillateTweenId = LeanTween.move(gameObject, finishPosition, oscillateTime)
                .setEaseInOutCubic()
                .setDelay(oscillatePause)
                .setOnComplete(() =>
                {
                    oscillateTweenId = LeanTween.move(gameObject, startPosition, oscillateTime)
                            .setEaseInOutCubic()
                            .setDelay(oscillatePause)
                            .setOnComplete(() =>
                            {
                                DoOscillate();      // repeat!
                            }).id;
                }).id;
    }

    private void OnCharacterGrounded(CharacterController character, bool isGrounded, bool wasGrounded, Collider2D groundedOn)
    {
        if (!characterActivated || groundedOn == null || groundedOn.gameObject != gameObject)     // not this platform
            return;

        characterGrounded = isGrounded;
        //Debug.Log($"OnCharacterGrounded {characterGrounded} oscillateTweenId = {oscillateTweenId}");

        if (oscillateTweenId != 0)
        {
            if (!characterGrounded)
                LeanTween.pause(oscillateTweenId);
            else
                LeanTween.resume(oscillateTweenId);
        }

        // reduce platform alpha while paused (not grounded)
        if (parentPlatform != null)
            parentPlatform.SetPaused(! characterGrounded);
    }


    // for gizmos
    private void SetGizmoBounds()
    {
        if (parentCollider == null)
            return;

        float halfWidth = parentCollider.bounds.extents.x;
        float halfHeight = parentCollider.bounds.extents.y;

        // draw rectangle for start position
        startTopLeft = new Vector2(startPosition.x - halfWidth, startPosition.y + halfHeight);
        startTopRight = new Vector2(startPosition.x + halfWidth, startPosition.y + halfHeight);
        startBottomLeft = new Vector2(startPosition.x - halfWidth, startPosition.y - halfHeight);
        startBottomRight = new Vector2(startPosition.x + halfWidth, startPosition.y - halfHeight);

        // draw rectangle for finish position
        finishTopLeft = new Vector2(finishPosition.x - halfWidth, finishPosition.y + halfHeight);
        finishTopRight = new Vector2(finishPosition.x + halfWidth, finishPosition.y + halfHeight);
        finishBottomLeft = new Vector2(finishPosition.x - halfWidth, finishPosition.y - halfHeight);
        finishBottomRight = new Vector2(finishPosition.x + halfWidth, finishPosition.y - halfHeight);
    }

    private void OnDrawGizmos()
    {
        if (parentCollider == null)
            return;

        Gizmos.color = Color.green;

        // start box
        Gizmos.DrawLine(startTopLeft, startTopRight);
        Gizmos.DrawLine(startTopRight, startBottomRight);
        Gizmos.DrawLine(startBottomRight, startBottomLeft);
        Gizmos.DrawLine(startBottomLeft, startTopLeft);

        // finish box
        Gizmos.DrawLine(finishTopLeft, finishTopRight);
        Gizmos.DrawLine(finishTopRight, finishBottomRight);
        Gizmos.DrawLine(finishBottomRight, finishBottomLeft);
        Gizmos.DrawLine(finishBottomLeft, finishTopLeft);

        // draw a line to connect start and finish
        Gizmos.DrawLine(startPosition, finishPosition);
    }
}
