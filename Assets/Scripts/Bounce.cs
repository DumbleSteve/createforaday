using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Collider2D))]

// makes a bouncy platform
// bounce on character grounded
// not on collision
public class Bounce : MonoBehaviour
{
    public float Bounciness = 10f;
    public bool Silent = false;
    private bool recoiling = false;

    public enum Direction
    {
        Reflect, Up //, Down, Left, Right, 
    };

    public Direction BounceDirection = Direction.Reflect;

    public float RecoilDistance = 0.5f;
    public float RecoilTime = 0.5f;

    private AudioSource bounceAudio;
    private Collider2D bounceCollider;


    private void OnEnable()
    {
        GameEvents.OnCharacterGrounded += OnCharacterGrounded;
    }

    private void OnDisable()
    {
        GameEvents.OnCharacterGrounded -= OnCharacterGrounded;
    }


    private void Start()
    {
        bounceAudio = GetComponent<AudioSource>();
        bounceCollider = GetComponent<Collider2D>();
    }

    // bounce up / reflect when character grounds on this object
    private void OnCharacterGrounded(CharacterController character, bool isGrounded, bool wasGrounded, Collider2D groundedOn)
    {
        if (Bounciness == 0)
            return;

        if (isGrounded && !wasGrounded && groundedOn == bounceCollider)
        {
            BounceCharacter(character);
        }
    }

    private void Recoil()
    {
        if (recoiling)
            return;

        recoiling = true;
        float startY = transform.localPosition.y;

        LeanTween.moveY(gameObject, startY - RecoilDistance, RecoilTime)
                    .setEaseOutSine()
                    .setOnComplete(() =>
                    {
                        LeanTween.moveY(gameObject, startY, RecoilTime)
                            .setEaseOutBounce()
                            .setOnComplete(() => recoiling = false);
                    });
    }


    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (Bounciness == 0)
    //        return;

    //    // only bounce when grounded - ie. not from sideways/upwards collisions
    //    if (collision.gameObject.CompareTag("Player"))
    //    {
    //        var character = collision.gameObject.GetComponent<CharacterController>();

    //        //Debug.Log($"Bounce: OnGround = {character.OnGround}");
    //        Debug.Log($"Bounce: bounce = {doBounce}");

    //        if (character != null && doBounce && !character.HasFainted && !character.IsTeleporting)
    //        {
    //            Vector2 collisionNormal = collision.contacts[0].normal;
    //            //Vector2 inVelocity = character.LastFrameVelocity;

    //            BounceCharacter(character, collisionNormal);
    //        }
    //    }
    //}

    private void BounceCharacter(CharacterController character) // Vector2 collisionNormal)
    {
        if (character != null && !character.HasFainted && !character.IsTeleporting)
        {
            Vector2 inVelocity = character.LastFrameVelocity;
            float bounceVelocity = inVelocity.magnitude * Bounciness;
            Vector3 reflectDirection;

            switch (BounceDirection)
            {
                case Direction.Reflect:
                default:
                    reflectDirection = Vector3.Reflect(inVelocity.normalized, Vector2.up);
                    break;

                case Direction.Up:
                    reflectDirection = Vector2.up;
                    break;
            }
            
            character.Push(reflectDirection * bounceVelocity);
            Recoil();

            //doBounce = false;

            //switch (BounceDirection)
            //{
            //    case Direction.Up:
            //        character.Push(Vector2.up * bounceVelocity);
            //        break;

            //    case Direction.Down:
            //        character.Push(Vector2.down * bounceVelocity);
            //        break;

            //    case Direction.Left:
            //        character.Push(Vector2.left * bounceVelocity);
            //        break;

            //    case Direction.Right:
            //        character.Push(Vector2.right * bounceVelocity);
            //        break;

            //    case Direction.Reflect:         // reflect at hit angle
            //        Vector3 reflectDirection = Vector3.Reflect(inVelocity.normalized, collisionNormal);  // in direction, in normal
            //        //Debug.Log($"Reflect: inVelocity {inVelocity} reflectDirection {reflectDirection}");
            //        character.Push(reflectDirection * bounceVelocity);
            //        break;
            //}

            if (!Silent)
                GameEvents.OnPlayAudio?.Invoke(bounceAudio, AudioManager.ClipTag.Bounce, false, 0f, false);
        }
    }
}
