using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(AudioSource))]

public class Switch : MonoBehaviour
{
    public DoorSwitch ParentDoorSwitch;

    public SpriteRenderer LockedSprite;    
    public SpriteRenderer UnlockedSprite;

    //public AudioManager.ClipTag lockAudioTag = AudioManager.ClipTag.DoorLock;
    //public AudioManager.ClipTag unlockAudioTag = AudioManager.ClipTag.DoorUnlock;

    private AudioSource switchAudio;

    private enum SwitchState
    {
        Locked,         // 'off'
        Unlocked        // 'on'
    };
    [SerializeField] private SwitchState currentState = SwitchState.Locked;


    private void OnEnable()
    {
        GameEvents.OnGameStart += OnGameStart;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStart -= OnGameStart;
    }

    private void Start()
    {
        switchAudio = GetComponent<AudioSource>();
    }


    private void OnGameStart(GameData gameData, int playCount, bool promptPassword)
    {
        currentState = SwitchState.Locked;
        SetSwitch(true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ToggleSwitch();
        }
    }

    private void ToggleSwitch()
    {
        switch (currentState)
        {
            case SwitchState.Locked:
                currentState = SwitchState.Unlocked;
                SetSwitch(false);
                break;

            case SwitchState.Unlocked:
                currentState = SwitchState.Locked;
                SetSwitch(false);
                break;
        }
    }


    private void SetSwitch(bool silent)
    {
        switch (currentState)
        {
            case SwitchState.Locked:
                LockedSprite.enabled = true;
                UnlockedSprite.enabled = false;

                if (!silent)
                {
                    GameEvents.OnPlayAudio?.Invoke(switchAudio, AudioManager.ClipTag.DoorLock, false, 0f, false);
                }

                ParentDoorSwitch.door.Lock(silent);
                break;

            case SwitchState.Unlocked:
                LockedSprite.enabled = false;
                UnlockedSprite.enabled = true;

                if (!silent)
                {
                    GameEvents.OnPlayAudio?.Invoke(switchAudio, AudioManager.ClipTag.DoorUnlock, false, 0f, false);
                }

                ParentDoorSwitch.door.UnLock(silent);
                break;
        }
    }
}
