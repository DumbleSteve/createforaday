using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

// encapsulates data that is saved to / loaded from file

[Serializable]
public class GameData
{
    [Space]
    public string GameName;
    public string SchoolName;
    public string TeamName;
    public string SceneName;
    [Space]
    public List<string> Credits;
    [Space]
    public Color LevelColour = Color.blue;

    [Header("Firestore Data")]
    public LevelStats levelStats;     // stats from firestore - eg. best time, play count etc.
    [Space]
    public bool Hidden;               // don't show in levels list
}
