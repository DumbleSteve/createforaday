using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using SimpleJSON;


public class GameManager : MonoBehaviour
{
    public GameLibrary gameLibrary;            // scriptable object
    public Transform collectibles;             // empty object containing all gems

    private float totalCollectibleValue;        // sum of all gem values (hundredths)
    private float collectedGemValue = 0f;       // total collected (hundredths)
    private float allCollectedBonus = 2f;       // bonus time multiplied if all collected

    private bool allCollected => collectedGemValue > 0 && collectedGemValue == totalCollectibleValue;
    public int BonusHundredths => (int)(collectedGemValue * (allCollected ? allCollectedBonus : 1f));      // hundredths

    public GameData CurrentGame => gameLibrary.CurrentGame;
    public int GameBestHundredths => CurrentGame.levelStats.BestTime;     // set from firestore

    private float reloadSceneDelay = 5f;      // after win/lose

    private int gameHundredths;         // without bonus subtracted
    private int GameNetHundredths => gameHundredths - BonusHundredths; 

    private bool timerRunning = false;
    private bool timerPaused = false;               // on game paused
    private float timerInterval = 0.01f;            // hundredths of a second
    private float timeSinceLastTimerTick = 0;

    private Coroutine reloadAfterPause;


    private void OnEnable()
    {
        GameEvents.OnReachedFinish += OnReachedFinish;
        GameEvents.OnCharacterFaint += OnCharacterFaint;
        GameEvents.OnGemCollected += OnGemCollected;

        GameEvents.OnLevelSelected += OnLevelSelected;
        GameEvents.OnLoadNextLevel += OnLoadNextLevel;
        GameEvents.OnReloadLevel += OnReloadLevel;

        GameEvents.OnNewBestTime += OnNewBestTime;

        GameEvents.OnGamePause += OnGamePause;

        //GameEvents.OnPlayerDataLoaded += OnPlayerDataLoaded;

        GameEvents.OnFireStorePlayerSet += OnPlayerSet;
        GameEvents.OnFireStorePlayerLevelStatsRetrieved += OnPlayerLevelStatsRetrieved;
        //GameEvents.OnPlayerLevelStatsLoaded += OnPlayerLevelStatsLoaded;
        GameEvents.OnFireStoreNoPlayerLevelStats += OnNoPlayerLevelStats;

        GameEvents.OnSkipPassword += OnSkipPassword;       // start timer

        GameEvents.OnResetGameData += OnResetGameData;
    }

    private void OnDisable()
    {
        GameEvents.OnReachedFinish -= OnReachedFinish;
        GameEvents.OnCharacterFaint -= OnCharacterFaint;
        GameEvents.OnGemCollected -= OnGemCollected;

        GameEvents.OnLevelSelected -= OnLevelSelected;
        GameEvents.OnLoadNextLevel -= OnLoadNextLevel;
        GameEvents.OnReloadLevel -= OnReloadLevel;

        GameEvents.OnNewBestTime -= OnNewBestTime;

        GameEvents.OnGamePause -= OnGamePause;

        //GameEvents.OnPlayerDataLoaded += OnPlayerDataLoaded;

        GameEvents.OnFireStorePlayerSet -= OnPlayerSet;
        GameEvents.OnFireStorePlayerLevelStatsRetrieved -= OnPlayerLevelStatsRetrieved;
        //GameEvents.OnPlayerLevelStatsLoaded -= OnPlayerLevelStatsLoaded;
        GameEvents.OnFireStoreNoPlayerLevelStats -= OnNoPlayerLevelStats;

        GameEvents.OnSkipPassword -= OnSkipPassword;       // start timer

        GameEvents.OnResetGameData -= OnResetGameData;
    }


    private void OnNewBestTime(GameData gameData, int newBestTime, bool onWin, bool newPlayer)
    {
        //if (newPlayer && (gameLibrary.NewPlayerLevelStats.BestTime == 0 || newBestTime < gameLibrary.NewPlayerLevelStats.BestTime))
        if (newPlayer)
        {
            gameLibrary.NewPlayerLevelStats.BestTime = newBestTime;        // saved for new player until new player name & school saved
        }
    }

    private void Start()
    {
        LeanTween.init(6400);

        // save player and school names in SO for easy access during game
        //gameLibrary.playerData.PlayerName = PlayerPrefs.GetString("PlayerName");
        //gameLibrary.playerData.SchoolName = PlayerPrefs.GetString("SchoolName");

        // load player name and school from browser file into SO for easy access during game
        //GameEvents.OnLoadPlayerDataFromFile?.Invoke(out gameLibrary.playerData);

        //GameEvents.OnDebugText?.Invoke($"Start: player: {gameLibrary.playerData.PlayerName}  school: {gameLibrary.playerData.SchoolName}");

        if (FirestoreManager.IsWebGL && !gameLibrary.IsNewPlayer)
        {
            GameEvents.OnFireStoreGetPlayerLevelStats?.Invoke();         // retrieve from firestore and store for convenience
        }
        else
        {
            // start game
            GameEvents.OnInitLevels?.Invoke(gameLibrary.VisibleGames, CurrentGame);       // for LevelsUI
            GameEvents.OnFadeIn?.Invoke(StartGame);
        }
    }

    private void StartGame()
    {
        // increment and save play count
        CurrentGame.levelStats.PlayCount++;         // sync SO with firebase - no need for syncing via changed event
        GameEvents.OnFireStoreIncrementLevelStats?.Invoke(true, false, false, 0);

        if (!gameLibrary.IsNewPlayer)
            GameEvents.OnFireStoreIncrementPlayerLevelStats?.Invoke(true, false, false, 0);
        else
            gameLibrary.NewPlayerLevelStats.PlayCount++;    // cache until player name set

        totalCollectibleValue = 0;
        collectedGemValue = 0;
        CalculateCollectibleValue(collectibles);        // recursive - total value of all gems in level

        bool promptPassword = CurrentGame.levelStats.SuccessCount > 0 && gameLibrary.PasswordSkipCount == 0;    // don't keep prompting if skipped
        GameEvents.OnGameStart?.Invoke(CurrentGame, CurrentGame.levelStats.PlayCount, promptPassword);
        GameEvents.OnTotalGemValueChanged?.Invoke(collectedGemValue, totalCollectibleValue, 0);           // for UI

        if (promptPassword)      // GameUI will prompt for password
            GameEvents.OnGamePause?.Invoke(true);         // until password entered / skipped. not for first play

        StartTimer();
        StartListenLevelStatsChanged();     // listen for all levels (eg. to update top time in UI)
    }

    private void Update()        
    {
        if (timerRunning && ! timerPaused)
            IncrementGameTimer();
    }

    private void StartTimer()
    {
        timerRunning = true;
        gameHundredths = 0;
        timeSinceLastTimerTick = 0;
        GameEvents.OnGameTimer?.Invoke(gameHundredths);
    }

    private void IncrementGameTimer()
    {
        timeSinceLastTimerTick += Time.deltaTime;

        if (timeSinceLastTimerTick >= timerInterval)
        {
            gameHundredths++;
            GameEvents.OnGameTimer?.Invoke(gameHundredths);

            timeSinceLastTimerTick = 0;             // reset
        }
    }

    private void StopTimer()
    {
        timerRunning = false;
    }

    private void OnGamePause(bool paused)
    {
        timerPaused = paused;        // pauses timer
    }

    // new best time for game
    private void SetGameNewBestTime(int bestHundredths)
    {
        GameEvents.OnDebugText?.Invoke($"SetGameNewBestTime: {bestHundredths} new player: {gameLibrary.IsNewPlayer} BestTime: {CurrentGame.levelStats.BestTime}");
        GameEvents.OnNewBestTime?.Invoke(CurrentGame, bestHundredths, true, gameLibrary.IsNewPlayer);       // for UI

        if (! gameLibrary.IsNewPlayer)
        {
            GameEvents.OnDebugText?.Invoke($"OnFireStoreSetPlayerPB: increment = {bestHundredths - CurrentGame.levelStats.BestTime}");
            GameEvents.OnFireStoreIncrementPlayerPB?.Invoke(bestHundredths - CurrentGame.levelStats.BestTime);        // for firestore leaderboard (decrement)
        }

        CurrentGame.levelStats.BestTime = bestHundredths;       // sync SO with firebase - no need for syncing via changed event
    }

    // new player and password saved in firestore db
    private void OnPlayerSet(PlayerData playerData)
    {
        // save new player and school names for use during game
        gameLibrary.playerData.PlayerName = playerData.PlayerName;
        gameLibrary.playerData.SchoolName = playerData.SchoolName;

        // persist player name and school
        //PlayerPrefs.SetString("PlayerName", playerData.PlayerName);
        //PlayerPrefs.SetString("SchoolName", playerData.SchoolName);
        //PlayerPrefs.Save();     // reqd for webGL!!

        //TODO: could use file save (and no password) for non-mobile webGL, but would get pretty messy
        //GameEvents.OnSavePlayerDataToFile?.Invoke(playerData);

        //GameEvents.OnDebugText?.Invoke($"OnPlayerSet: player: {PlayerPrefs.GetString("PlayerName")}  school: {PlayerPrefs.GetString("SchoolName")}");

        // save best time
        //if (newPlayerBestHundredths > 0)        // new player's time and level stats cached until player name registered
        // new player's time and level stats cached until player name registered
        //{
        GameEvents.OnFireStoreSetPlayerLevelBestTime?.Invoke(gameLibrary.NewPlayerLevelStats.BestTime);  
        GameEvents.OnFirestoreSetPlayerLevelStats?.Invoke(gameLibrary.NewPlayerLevelStats);

        CurrentGame.levelStats.BestTime = gameLibrary.NewPlayerLevelStats.BestTime;  // sync SO with firebase - no need for syncing via changed event

        gameLibrary.NewPlayerLevelStats = new();

        GameEvents.OnGamePause?.Invoke(false);
        //}
    }

    private void OnSkipPassword()
    {
        GameEvents.OnGamePause?.Invoke(false);
        gameLibrary.PasswordSkipCount++;         // don't keep prompting?
    }

    // retrieved from firestore - store in gameLibrary SO for convenience
    private void OnPlayerLevelStatsRetrieved(string playerLevelStatsJson)
    {
        //GameEvents.OnDebugText?.Invoke($"OnPlayerLevelStatsRetrieved: {playerLevelStatsJson}");

        JSONNode node = JSON.Parse(playerLevelStatsJson);

        foreach (KeyValuePair<string, JSONNode> value in node)
        {
            string levelName = value.Key;
            JSONNode statsNode = value.Value;

            var levelStats = new LevelStats
            {
                LevelName = levelName,

                BestTime = statsNode["BestTime"],

                PlayTime = statsNode["PlayTime"],
                PlayCount = statsNode["PlayCount"],
                SuccessCount = statsNode["SuccessCount"],
                FailCount = statsNode["FailCount"]
            };

            //GameEvents.OnDebugText?.Invoke($"OnPlayerLevelStatsRetrieved: {levelName} best time: {levelStats.BestTime}");

            // find the game library game for this level/game and set its LevelStats to the values retrieved from firestore (player PB, play count, etc)
            // this caching is for convenient access, since they will only be changed by this player and so don't need to be synced on db changes
            GameData gameData = gameLibrary.GetGameByName(levelName);
            if (gameData != null)
            {
                gameData.levelStats = levelStats;
                GameEvents.OnNewBestTime?.Invoke(CurrentGame, CurrentGame.levelStats.BestTime, false, false);       // for Timer UI
            }
        }

        // start game
        GameEvents.OnInitLevels?.Invoke(gameLibrary.VisibleGames, CurrentGame);       // for LevelsUI
        GameEvents.OnFadeIn?.Invoke(StartGame);
    }

    private void OnNoPlayerLevelStats()
    {
        // start game
        GameEvents.OnInitLevels?.Invoke(gameLibrary.VisibleGames, CurrentGame);       // for LevelsUI
        GameEvents.OnFadeIn?.Invoke(StartGame);
    }

    private void OnCharacterFaint(CharacterController character)
    {
        StopTimer();

        CurrentGame.levelStats.PlayTime += gameHundredths;      // sync SO with firebase - no need for syncing via changed event
        GameEvents.OnGameLost?.Invoke(gameHundredths, CurrentGame);

        CurrentGame.levelStats.FailCount++;            // sync SO with firebase - no need for syncing via changed event
        GameEvents.OnFireStoreIncrementLevelStats?.Invoke(false, false, true, GameNetHundredths);

        if (!gameLibrary.IsNewPlayer)
            GameEvents.OnFireStoreIncrementPlayerLevelStats?.Invoke(false, false, true, GameNetHundredths);
        else
            gameLibrary.NewPlayerLevelStats.FailCount++;

        // timeout if player doesn't hit continue...
        reloadAfterPause = StartCoroutine(ReloadSceneAfterPause());
    }

    private void OnReachedFinish()
    {
        StopTimer();

        if (GameBestHundredths == 0 || GameNetHundredths < GameBestHundredths)
            SetGameNewBestTime(GameNetHundredths);     // saved for current game

        CurrentGame.levelStats.PlayTime += gameHundredths;       // sync SO with firebase - no need for syncing via changed event
        GameEvents.OnGameWon?.Invoke(GameNetHundredths, GameBestHundredths, BonusHundredths, gameLibrary.NextGameName, CurrentGame);

        CurrentGame.levelStats.SuccessCount++;         // sync SO with firebase - no need for syncing via changed event
        GameEvents.OnFireStoreIncrementLevelStats?.Invoke(false, true, false, gameHundredths);

        if (!gameLibrary.IsNewPlayer)
            GameEvents.OnFireStoreIncrementPlayerLevelStats?.Invoke(false, true, false, gameHundredths);
        else
            gameLibrary.NewPlayerLevelStats.SuccessCount++;

        // wait for player to hit continue ... leaderboard shown!
    }


    private void OnGemCollected(float gemValue)
    {
        collectedGemValue += gemValue;
        GameEvents.OnTotalGemValueChanged?.Invoke(collectedGemValue, totalCollectibleValue, BonusHundredths);

        if (collectedGemValue == totalCollectibleValue)
            GameEvents.OnAllGemsCollected?.Invoke(collectedGemValue); 
    }

    // recursive
    private void CalculateCollectibleValue(Transform parent)
    {
        if (parent == null)
            return;

        foreach (Transform coll in parent)
        {
            if (!coll.gameObject.activeSelf)
                continue;

            var gem = coll.GetComponent<Gem>();
            if (gem != null)
                totalCollectibleValue += gem.GemValue;
            else
                CalculateCollectibleValue(coll);       // recursive
        }
    }

    private void OnResetGameData() //string gameName)
    {
        gameLibrary.ResetAllGameData();
        LoadCurrentScene();
    }

    // scenes

    private void OnLevelSelected(GameData gameData, int gameIndex)
    {
        gameLibrary.CurrentGameIndex = gameIndex;
        GameEvents.OnFadeOut?.Invoke(LoadCurrentScene, true);
    }

    private void LoadCurrentScene()
    {
        StopListenLevelStatsChanged();
        SceneManager.LoadScene(CurrentGame.SceneName);
    }

    private void OnLoadNextLevel()      // player clicked continue button on WinUI
    {
        GameEvents.OnFadeOut?.Invoke(LoadNextScene, true);
    }

    private void OnReloadLevel()        // player clicked continue button on LoseUI
    {
        StopListenLevelStatsChanged();

        if (reloadAfterPause != null)
            StopCoroutine(reloadAfterPause);

        GameEvents.OnFadeOut?.Invoke(ReloadScene, true);
    }

    private IEnumerator ReloadSceneAfterPause()
    {
        yield return new WaitForSeconds(reloadSceneDelay);
        GameEvents.OnFadeOut?.Invoke(ReloadScene, true);
    }

    private void ReloadScene()
    {
        StopListenLevelStatsChanged();

        var currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.name);
    }

    //private IEnumerator NextSceneAfterPause()
    //{
    //    yield return new WaitForSeconds(reloadSceneDelay);
    //    GameEvents.OnFadeOut?.Invoke(LoadNextScene, true);
    //}

    private void LoadNextScene()
    {
        StopListenLevelStatsChanged();

        string nextSceneName = gameLibrary.GoToNextGame().SceneName;
        SceneManager.LoadScene(nextSceneName);
    }


    private void StartListenLevelStatsChanged()
    {
        foreach (GameData level in gameLibrary.VisibleGames)
        {
            GameEvents.OnFireStoreStartListenLevelStatsChanged?.Invoke(level.GameName);
        }
    }

    private void StopListenLevelStatsChanged()
    {
        foreach (GameData level in gameLibrary.VisibleGames)
        {
            GameEvents.OnFireStoreStopListenLevelStatsChanged?.Invoke(level.GameName);
        }
    }
}
