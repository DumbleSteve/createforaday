using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CharacterController))]

public class CharacterAnimator : MonoBehaviour
{
    public AudioSource animationAudio;      // set in inspector - idle/walk/run/fly loops

    private enum AnimState
    {
        Idle, Walk, Run, Fly, Faint
    }
    private AnimState currentState = AnimState.Idle;

    private bool gameStarted = false;

    // looking down
    public Transform RayCastOrigin;
    public LayerMask LandingLayers;             // for ray cast 'grounded' hits

    public bool FlyWhenNotGrounded = false;      // eg. true when no arms and legs for 'air running'

    public float groundedDistance = 0.2f;       // down raycast
    //public float wallDistance = 0.2f;           // left/right raycast
    public float circleCastRadius = 0.2f;

    private bool isGrounded = false;
    private Collider2D groundedOn = null;
    private bool lastFrameGrounded = false;     // to check if just landed

    public float FlyMaxHorizSpeed = 1.5f;       // horiz speed below this triggers fly animation, else mid-air walk/run
    public float WalkSpeed = 1.0f;              // animator blend tree
    public float RunSpeed = 4.0f;                 // animator blend tree

    public ParticleSystem DustParticles;        // on landing
    public ParticleSystem TrailParticles;       // while flying

    private Animator anim;
    private CharacterController character;

    private Vector2 startScale;


    private void Awake()
    {
        anim = GetComponent<Animator>();
        character = GetComponent<CharacterController>();

        startScale = transform.localScale;

        Screen.fullScreenMode = FullScreenMode.FullScreenWindow;
        Screen.fullScreen = true;
    }

    private void OnEnable()
    {
        GameEvents.OnGameStart += OnGameStart;
        GameEvents.OnCharacterFaint += OnCharacterFaint;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStart -= OnGameStart;
        GameEvents.OnCharacterFaint -= OnCharacterFaint;
    }

    private void OnGameStart(GameData gameData, int playCount, bool promptPassword)
    {
        gameStarted = true;

        // assume don't start on ground
        isGrounded = lastFrameGrounded = false;
        GameEvents.OnCharacterGrounded?.Invoke(character, isGrounded, false, null);

        if (animationAudio != null)
            GameEvents.OnPlayAudio.Invoke(animationAudio, AudioManager.ClipTag.Idle, false, 0f, false);
    }

    private void Update()
    {
        if (!gameStarted)
            return;

        if (character.HasFainted)
            return;

        float horizSpeed = Mathf.Abs(character.SpeedHoriz);
        anim.SetFloat("Speed", horizSpeed);      // idle/walk/run blend tree

        bool wasGrounded = isGrounded;
        Collider2D wasGroundedOn = groundedOn;

        groundedOn = CheckOnGround();
        isGrounded = groundedOn != null;

        if (isGrounded != wasGrounded)
            GameEvents.OnCharacterGrounded?.Invoke(character, isGrounded, wasGrounded, isGrounded ? groundedOn : wasGroundedOn);

        if (character.IsTeleporting)
        {
            SetState(AnimState.Idle);               // idle/walk/run blend tree
            TrailParticles.Play();
            return;
        }

        if (isGrounded)
        {
            //Debug.Log($"GROUNDED: SpeedVert = {character.SpeedVert}");
            SetState(AnimState.Idle);               // idle/walk/run blend tree
            TrailParticles.Stop();

            if (! lastFrameGrounded)
            {
                // if just landed on a moving platform, attach to it without changing scale
                var platform = groundedOn.GetComponent<Platform>();
                if (platform != null)
                {
                    var oscillate = platform.GetComponent<Oscillate>();
                    if (oscillate != null && oscillate.enabled)
                        character.transform.SetParent(groundedOn.transform, true);
                }

                if (character.SpeedVert <= 0)     // just about to land (moving down)
                {
                    if (!character.IsTeleporting)
                    {
                        FootfallAudio();
                        DustParticles.Play();
                    }
                }
            }

            if (animationAudio != null)
            {
                if (horizSpeed > RunSpeed)
                    GameEvents.OnPlayAudio.Invoke(animationAudio, AudioManager.ClipTag.Run, false, 0f, true);
                else if (horizSpeed > WalkSpeed)
                    GameEvents.OnPlayAudio.Invoke(animationAudio, AudioManager.ClipTag.Walk, false, 0f, true);
                else
                    GameEvents.OnPlayAudio.Invoke(animationAudio, AudioManager.ClipTag.Idle, false, 0f, true);
            }
        }
        else        // not grounded
        {
            if (lastFrameGrounded)
            {
                character.JumpAudio();
                character.transform.parent = null;      // detach from platform
                character.transform.localScale = startScale;    // to make sure!
            }

            if (FlyWhenNotGrounded)
            {
                SetState(AnimState.Fly);
                TrailParticles.Play();
            }
            else if (Mathf.Abs(character.SpeedHoriz) >= FlyMaxHorizSpeed)      // not on ground - fast horiz speed
            {
                //Debug.Log($"IDLE/WALK/RUN: SpeedHoriz = {character.SpeedHoriz}  FlyMaxHorizSpeed = {FlyMaxHorizSpeed}");
                SetState(AnimState.Idle);               // idle/walk/run blend tree
                TrailParticles.Play();
            }
            else if (character.MovingVert)         // not on ground - slow horiz speed & moving vertically
            {
                //Debug.Log($"FLY: SpeedVert = {character.SpeedVert}");
                SetState(AnimState.Fly);
                TrailParticles.Play();
            }
            else                                  // not on ground - maybe bouncing?
            {
                //Debug.Log($"IDLE");
                SetState(AnimState.Idle);
                TrailParticles.Stop();
            }
        }

        if (character.MovingHoriz)
        {
            if (character.MovingLeft)
                character.SetFacing(CharacterController.Facing.Left);
            else if (character.MovingRight)
                character.SetFacing(CharacterController.Facing.Right);
        }

        lastFrameGrounded = isGrounded;
    }

    private Collider2D CheckOnGround()
    {
        var hit = Physics2D.CircleCast(RayCastOrigin.position, circleCastRadius, Vector3.down, groundedDistance, LandingLayers);
        return hit.collider;
    }

    private void SetState(AnimState newState)
    {
        if (currentState == newState)
            return;

        currentState = newState;

        switch (currentState)
        {
            case AnimState.Idle:        // blend tree (Speed)
                anim.ResetTrigger("Fly");       // in case started and immediately stopped flying
                anim.SetTrigger("Idle");
                if (animationAudio != null)
                    GameEvents.OnPlayAudio.Invoke(animationAudio, AudioManager.ClipTag.Idle, false, 0f, false);
                break;

            case AnimState.Walk:        // blend tree (Speed)
            case AnimState.Run:         // blend tree (Speed)
                break;

            case AnimState.Fly:
                if (animationAudio != null)
                    GameEvents.OnPlayAudio?.Invoke(animationAudio, AudioManager.ClipTag.Fly, false, 0f, false);
                anim.SetTrigger("Fly");
                break;

            case AnimState.Faint:
                anim.SetTrigger("Faint");
                break;
        }
    }

    private void OnCharacterFaint(CharacterController character)
    {
        SetState(AnimState.Faint);
    }

    // animation events
    private void FaintEnd()
    {
        SpriteFader fader = character.GetComponent<SpriteFader>();

        if (fader != null)
            fader.FadeOut(true);
    }

    private void FootfallAudio()
    {
        if (isGrounded)
            character.FootfallAudio();
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(RayCastOrigin.position, circleCastRadius);

        Gizmos.color = Color.magenta;
        var onGroundPoint = new Vector3(RayCastOrigin.position.x, RayCastOrigin.position.y - groundedDistance, RayCastOrigin.position.z);
        Gizmos.DrawLine(RayCastOrigin.position, onGroundPoint);
        Gizmos.DrawWireSphere(onGroundPoint, circleCastRadius);
    }
}
