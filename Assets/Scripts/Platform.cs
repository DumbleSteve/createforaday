using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System.Threading;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(SpriteRenderer))]

public class Platform : MonoBehaviour
{
    public bool IsToxic = false;

    public float DegradeTime = 0f;          // while player on platform
    public int DegradeSteps = 0;
    private float DegradeInterval => IsDegradeable ? (DegradeTime / DegradeSteps) : 0f;

    public ParticleSystem DegradedParticles;               // optional explosion
    public AudioSource degradedAudioSource;

    private float degradeTimeRemaining = 0f;
    //private CancellationTokenSource degradeCancelToken;     // to cancel async degrade task
    private Coroutine degradeCoroutine = null;

    private bool IsDegradeable => DegradeTime > 0 && DegradeSteps > 0;

    private int randomGlitches => Random.Range(4, 6);
    private float randomGlitchTime => Random.Range(0.1f, 0.2f);

    private bool playerOnPlatform = false;

    private Collider2D platformCollider;
    private SpriteRenderer platformSprite;
    private Color spriteColour;
    private Color pausedColour => new(spriteColour.r, spriteColour.g, spriteColour.b, 0.25f);

    private void Awake()
    {
        platformCollider = GetComponent<Collider2D>();
        platformSprite = GetComponent<SpriteRenderer>();
        spriteColour = platformSprite.color;

        degradeTimeRemaining = DegradeTime;
    }

    private void OnEnable()
    {
        GameEvents.OnCharacterGrounded += OnCharacterGrounded;
    }

    private void OnDisable()
    {
        GameEvents.OnCharacterGrounded -= OnCharacterGrounded;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            var character = collision.gameObject.GetComponent<CharacterController>();

            if (IsToxic)
            {
                if (character != null && !character.HasFainted) // && !character.IsTeleporting)
                    GameEvents.OnCharacterFaint?.Invoke(character);
            }
            else
            {
                character.HitAudio();
            } 
        }
    }

    // degradeable platforms degrade while is character grounded on them
    private void OnCharacterGrounded(CharacterController character, bool isGrounded, bool wasGrounded, Collider2D groundedOn)
    {
        if (!IsDegradeable)
            return;

        // if player not grounded and was on this platform, stop degrading
        if (! isGrounded && playerOnPlatform)
        {
            playerOnPlatform = false;
            //Debug.Log($"{name} OnCharacterGrounded: {playerOnPlatform}");

            StopDegrade();
        }
        // start degrading if player just landed on this platform
        else if (isGrounded && !wasGrounded && !playerOnPlatform && groundedOn != null && groundedOn.transform == transform)
        {
            playerOnPlatform = true;
            //Debug.Log($"{name} OnCharacterGrounded: {playerOnPlatform}");

            degradeCoroutine = StartCoroutine(Degrade());
        }
    }

    private IEnumerator Degrade()
    {
        if (! IsDegradeable)
            yield break;

        while (playerOnPlatform && degradeTimeRemaining > 0)
        {
            yield return new WaitForSeconds(DegradeInterval);

            degradeTimeRemaining -= DegradeInterval;

            yield return StartCoroutine(Glitch());

            if (degradeTimeRemaining <= 0f)
            {
                FullyDegraded();
                yield break;
            }
        }
    }

    private void StopDegrade()
    {
        if (degradeCoroutine != null)
        {
            StopCoroutine(degradeCoroutine);
            degradeCoroutine = null;
        }
    }

    private IEnumerator Glitch()
    {
        if (! IsDegradeable)
            yield break;

        if (degradedAudioSource != null)
            GameEvents.OnPlayAudio(degradedAudioSource, AudioManager.ClipTag.PlatformGlitch, false, 0f, false);

        for (int i = 0; i < randomGlitches; i++)
        {
            platformSprite.enabled = !platformSprite.enabled;
            yield return new WaitForSeconds(randomGlitchTime);
        }

        platformSprite.color = new Color(spriteColour.r, spriteColour.g, spriteColour.b, degradeTimeRemaining / DegradeTime);
        platformSprite.enabled = true;
    }

    private void FullyDegraded()
    {
        StopDegrade();

        degradeTimeRemaining = 0f;
        playerOnPlatform = false;

        platformCollider.enabled = false;
        platformSprite.enabled = false;

        if (DegradedParticles != null)
            DegradedParticles.Play();

        if (degradedAudioSource != null)
            GameEvents.OnPlayAudio(degradedAudioSource, AudioManager.ClipTag.PlatformDegraded, false, 0f, false);
    }

    public void SetPaused(bool paused)
    {
        platformSprite.color = paused ? pausedColour : spriteColour;
    }
}
