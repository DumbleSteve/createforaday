using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public float RotateSpeed = 20f;

    void Update()
    {
        transform.Rotate(Vector3.forward, RotateSpeed * Time.deltaTime);
    }
}
