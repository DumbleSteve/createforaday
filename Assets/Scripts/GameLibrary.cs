using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


[CreateAssetMenu(menuName = "GameLibrary")]

// asset containing data shared by several scripts
// persists between scene changes
// reset on each build
// encapsulates data that is saved to / loaded from file (not WebGL)

public class GameLibrary : ScriptableObject
{
    public PlayerData playerData;       // name and school

    public List<GameData> Games;        // data set in inspector - all levels
    public List<GameData> VisibleGames => Games.Where(game => !game.Hidden).ToList(); 

    public bool IsNewPlayer => string.IsNullOrEmpty(playerData.PlayerName);

    public int PlayCount => VisibleGames.Sum(game => game.levelStats.PlayCount);
    public int TotalPlayTime => VisibleGames.Sum(game => game.levelStats.PlayTime);

    public int CurrentGameIndex; // { get; private set; }

    // data cached for new player until new player name & school saved
    public LevelStats NewPlayerLevelStats = new();

    public int PasswordSkipCount = 0;       // so don't keep prompting during play session. reset by StartScene. game behaves as if new player

    //public bool ResetOnStart;           // reset all game data
    //public bool ResetPlayerOnStart;     // if ResetOnStart, reset player name and school

    public GameData CurrentGame => VisibleGames[ CurrentGameIndex ];


    public GameData GoToNextGame()
    {
        CurrentGameIndex++;

        if (CurrentGameIndex >= VisibleGames.Count)        // loop back to first
            CurrentGameIndex = 0;

        return CurrentGame;
    }

    public string NextGameName      // look ahead on win level
    {
        get
        {
            if (VisibleGames.Count == 1)       // no next!
                return "";

            int nextGameIndex = CurrentGameIndex + 1;

            if (nextGameIndex >= VisibleGames.Count)
                nextGameIndex = 0;

            return VisibleGames[nextGameIndex].GameName;
        }
    }

    public GameData GetGameByName(string name)
    {
        return VisibleGames.FirstOrDefault(game => game.GameName == name);     // null if not found
    }

    //public LevelStats GetLevelStatsByName(string levelName)
    //{
    //    var gameLevel = Games.FirstOrDefault(x => x.levelStats.LevelName == levelName);

    //    if (gameLevel != null)
    //        return gameLevel.levelStats;

    //    return null;
    //}

    public void ResetAllGameData()
    {
        CurrentGameIndex = 0;
        PasswordSkipCount = 0;
        NewPlayerLevelStats = new();

        for (int i = 0; i < Games.Count; i++)
        {
            //GameEvents.OnResetGameData?.Invoke(Games[i].GameName);      // save new files and reload scene
            Games[i].levelStats.Reset();          // best times etc.
        }

        //if (ResetPlayerOnStart)
        {
            playerData.Reset();                 // reset player name / school / password
            GameEvents.OnSavePlayerDataToFile?.Invoke(playerData);
        }

        //ResetOnStart = false;
        //ResetPlayerOnStart = false;
    }
}
