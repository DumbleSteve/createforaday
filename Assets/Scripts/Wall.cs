using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class Wall : MonoBehaviour
{
    public bool CanJumpLeftSide = true;
    public bool CanJumpRightSide = true;
    public Color CanJumpColour = Color.green;
    //public Color WallJumpColour = Color.red;
    public float WallJumpForce = 7.5f;

    public bool CanWallJump => CanJumpRightSide || CanJumpLeftSide;

    private float pulseScale = 1.4f;       // x axis - for wall jump
    private float pulseTime = 0.1f;
    private bool pulsing = false;

    private float startWidth;

    private SpriteRenderer wallSprite;
    private Color spriteColour;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        wallSprite = GetComponent<SpriteRenderer>();
        spriteColour = wallSprite != null ? wallSprite.color : Color.white;

        startWidth = transform.localScale.x;
    }

    public void WallJumpPulse()
    {
        if (pulsing)
            return;

        pulsing = true;

        //SetJumpColour(true);
        if (audioSource != null)
            GameEvents.OnPlayAudio?.Invoke(audioSource, AudioManager.ClipTag.Bounce, false, 0f, false);

        LeanTween.scaleX(gameObject, startWidth / pulseScale, pulseTime)
                    .setEaseOutCubic()
                    .setOnComplete(() =>
                    {
                        LeanTween.scaleX(gameObject, startWidth * pulseScale, pulseTime)
                                    .setEaseOutBounce()
                                    .setOnComplete(() =>
                                    {
                                        LeanTween.scaleX(gameObject, startWidth, pulseTime)
                                                    .setEaseOutBounce()
                                                    .setOnComplete(() =>
                                                    {
                                                        //SetJumpColour(false);
                                                        pulsing = false;
                                                    });
                                    });
                    });
    }

    //private void SetJumpColour(bool jump)
    //{
    //    if (wallSprite == null)
    //        return;

    //    wallSprite.color = jump ? WallJumpColour : spriteColour;
    //}

    public void SetCanJumpColour(bool canJump)
    {
        if (wallSprite == null)
            return;

        wallSprite.color = canJump ? CanJumpColour : spriteColour;
    }
}
