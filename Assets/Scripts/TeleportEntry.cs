using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]      // trigger
[RequireComponent(typeof(AudioSource))]

public class TeleportEntry : MonoBehaviour
{
    public Teleporter parentTeleporter;
    private bool isActive = false;      // player is inside trigger


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isActive)
            return;

        if (collision.gameObject.CompareTag("Player"))
        {
            isActive = true;
            var character = collision.gameObject.GetComponent<CharacterController>();

            if (character != null && !character.IsTeleporting)
            {
                parentTeleporter.Teleport(character, this);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            isActive = false;  
        }
    }
}
