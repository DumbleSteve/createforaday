using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindTunnel : MonoBehaviour
{
    [Header("Wind Collider")]
    public BoxCollider2D WindTrigger;
    public Transform WindEnd;
    private Vector2 WindDirection => (WindEnd.position - transform.position).normalized;
    public float MaxLength => Vector2.Distance(WindEnd.position, transform.position);

    public float WindRate = 240f;
    public float Width = 0.5f;

    [Header("Wind Cycle")]
    public float WindForce = 2;             // max at wind origin
    public float WindDelay = 2.5f;          // before first wind activation
    public float WindTime = 5f;             // activation time
    public float WindPause = 5f;            // time deactivated

    [Header("Wind Raycast")]
    public LayerMask WindLayers;            // for ray cast hits
    private Vector2 windHitPoint;           // ray cast hit point
    private float RayCastLength => Vector2.Distance(windHitPoint, transform.position);
    private float raycastInterval = 0.05f;
    private bool windOn = false;
    private Coroutine windCoroutine;

    private float lifetimeFactor = 5f;      // particles lifetime/length fudge factor

    [Header("Particles")]
    public ParticleSystem WindParticles;
    public Color WindColour = Color.cyan;

    private void OnEnable()
    {
        GameEvents.OnGameStart += OnGameStart;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStart -= OnGameStart;
    }

    public void Awake()
    {
        InitParticles(MaxLength);
        SetTunnelSize(MaxLength);
    }

    private void InitParticles(float length)
    {
        var main = WindParticles.main;
        main.startColor = WindColour;
        main.startLifetime = length / lifetimeFactor;

        var shape = WindParticles.shape;
        shape.scale = new Vector2(Width, shape.scale.y);

        var emission = WindParticles.emission;
        emission.rateOverTime = WindRate * Width;
    }

    // set trigger collider size
    private void SetTunnelSize(float length)
    {
        WindTrigger.size = new Vector2(Width, length);
        WindTrigger.offset = new Vector2(0, length / 2f);
    }

    private void OnGameStart(GameData gameData, int playCount, bool promptPassword)
    {
        StartCoroutine(CycleWind());
    }

    private IEnumerator CycleWind()
    {
        DeactivateWind();
        yield return new WaitForSeconds(WindDelay);

        while (true)
        {
            // on
            windCoroutine = StartCoroutine(ActivateWind());
            yield return new WaitForSeconds(WindTime);

            // off
            DeactivateWind();
            yield return new WaitForSeconds(WindPause);
        }
    }

    private void DeactivateWind()
    {
        WindParticles.Stop();
        WindTrigger.enabled = false;
        windOn = false;
        windHitPoint = transform.position;

        if (windCoroutine != null)
        {
            StopCoroutine(windCoroutine);
            windCoroutine = null;
        }
    }

    private IEnumerator ActivateWind()
    {
        if (windOn)
            yield break;

        windOn = true;
        WindParticles.Play();
        WindTrigger.enabled = true;

        windHitPoint = transform.position;
        RaycastHit2D hit;

        while (windOn)
        {
            hit = Physics2D.Raycast(transform.position, WindDirection, MaxLength, WindLayers);

            if (hit.collider != null)
                windHitPoint = hit.point;
            else            // no hit - full range
                windHitPoint = WindEnd.position;

            SetTunnelSize(RayCastLength);        // trigger collider size
            yield return new WaitForSeconds(raycastInterval);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            float distanceFromOrigin = Mathf.Abs(transform.position.y - collision.transform.position.y);

            if (MaxLength <= 0 || distanceFromOrigin >= MaxLength)        // out of wind 'range'
                return;

            float windFactor = 1f - (distanceFromOrigin / MaxLength);
            float distanceForce = WindForce * windFactor;
            GameEvents.OnCharacterMove?.Invoke(WindDirection, distanceForce);

            //Debug.Log($"WindTunnel: distanceFromOrigin {distanceFromOrigin} : windFactor {windFactor} : distanceForce {distanceForce}");
        }
    }

    private void OnDrawGizmos()
    {
        if (!windOn)
            return;

        Gizmos.color = Color.yellow;

        Gizmos.DrawLine(windHitPoint, transform.position);
        Gizmos.DrawWireSphere(transform.position, WindTrigger.size.x / 2f);
        Gizmos.DrawWireSphere(windHitPoint, WindTrigger.size.x / 2f);
    }
}
