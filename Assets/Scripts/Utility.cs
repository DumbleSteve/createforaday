

public static class Utility
{
    public static string FormatTime(int hundredths)
    {
        if (hundredths <= 0)
            return "--:--";

        int hunds = hundredths % 100;       // remainder hundredths
        int seconds = hundredths / 100;
        int secs = seconds % 60;            // remainder seconds
        int mins = seconds / 60;            
        return string.Format("{0:00}:{1:00}.{2:00}", mins, secs, hunds);
    }

    public static string FormatHoursMins(int hundredths)
    {
        if (hundredths <= 0)
            return "--";

        int minutes = hundredths / 100 / 60;        // discard seconds and hundredths

        int mins = minutes % 60;
        int hours = minutes / 60;

        if (hours > 0)
            return string.Format("{0}h {1}m", hours, mins);

        if (mins > 0)
            return string.Format("{0}m", mins);

        return "--";
    }

    //public static string GameStats(GameData game)
    //{
    //    return string.Format("Attempts: {0}\nCompleted: {1}\nFried: {2}", game.saveData.levelStats.PlayCount, game.saveData.levelStats.SuccessCount, game.saveData.levelStats.FailCount);
    //}

    public static string FirebaseClean(string toClean)
    {
        string clean = toClean.Replace('/', '_');
        clean = clean.Replace('.', '_');

        return clean;
    }

    public static string FullPlayerName(string player, string school, bool noSchool)
    {
        string fullName = player;

        if (!string.IsNullOrEmpty(school))
            fullName += (" @ " + school);
        else if (noSchool)
            fullName += " [ No School ]";

        return fullName;
    }
}
