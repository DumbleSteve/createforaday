﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class GameEvents
{
    // touch input

    public delegate void OnPlayerTouchDelegate(int tapCount, int touchCount);
    public static OnPlayerTouchDelegate OnPlayerTouch;

    public delegate void OnPlayerReleaseDelegate(int touchCount);
    public static OnPlayerReleaseDelegate OnPlayerRelease;

    public delegate void OnSwipeStartDelegate(Vector2 startPosition, int touchCount);
	public static OnSwipeStartDelegate OnSwipeStart;

	public delegate void OnSwipeMoveDelegate(Vector2 movePosition, Vector2 direction, float speed, float distance, int touchCount);
	public static OnSwipeMoveDelegate OnSwipeMove;

	public delegate void OnOnSwipeEndDelegate(Vector2 endPosition, Vector2 direction, float speed, float distance, int touchCount, int tapCount);
	public static OnOnSwipeEndDelegate OnSwipeEnd;

	public delegate void OnSwipeCardinalDelegate(TouchInput.SwipeCardinal cardinal, float swipeSpeed, float swipeDistance);
	public static OnSwipeCardinalDelegate OnSwipeCardinal;

    // character movement

    public delegate void OnMoveKeyPressDelegate(Vector2 direction, bool down);		// keyboard activates MoveButtonInput via event
    public static OnMoveKeyPressDelegate OnMoveKeyPress;

    public delegate void OnCharacterGroundedDelegate(CharacterController character, bool isGrounded, bool wasGrounded, Collider2D groundedOn);
    public static OnCharacterGroundedDelegate OnCharacterGrounded;

    public delegate void OnCharacterMoveDelegate(Vector2 direction, float force);
    public static OnCharacterMoveDelegate OnCharacterMove;              // while held down

	public delegate void OnCharacterWallJumpDelegate(Wall fromWall, WallJump.WallDirection fromSide);
	public static OnCharacterWallJumpDelegate OnCharacterWallJump;

	// game state

	public delegate void OnGameStartDelegate(GameData gameData, int playCount, bool promptPassword);
	public static OnGameStartDelegate OnGameStart;

	//public delegate void OnPlayerDataSetDelegate(PlayerData playerData);
	//public static OnPlayerDataSetDelegate OnPlayerDataSet;

	public delegate void OnCharacterFaintDelegate(CharacterController character);
	public static OnCharacterFaintDelegate OnCharacterFaint;            // game over - fade to black -> reload scene

	public delegate void OnReachedFinishDelegate();
	public static OnReachedFinishDelegate OnReachedFinish;           

	public delegate void OnGameWonDelegate(int gameHundredths, int bestHundredths, int bonusHundredths, string nextLevelName, GameData game);   // congrats, next level
	public static OnGameWonDelegate OnGameWon;

	public delegate void OnGameLostDelegate(int gameHundredths, GameData game);
	public static OnGameLostDelegate OnGameLost;

    public delegate void OnNewBestTimeDelegate(GameData gameData, int newBestTime, bool onWin, bool newPlayer);
    public static OnNewBestTimeDelegate OnNewBestTime;

	public delegate void OnGamePauseDelegate(bool paused);
	public static OnGamePauseDelegate OnGamePause;

	public delegate void OnResetGameDataDelegate(); // string gameName);
	public static OnResetGameDataDelegate OnResetGameData;          // reset button

	// gems

	public delegate void OnGemCollectedDelegate(float gemValue);
	public static OnGemCollectedDelegate OnGemCollected;

	public delegate void OnTotalGemValueChangedDelegate(float collectedGemValue, float totalCollectibleValue, int bonusHundredths);
	public static OnTotalGemValueChangedDelegate OnTotalGemValueChanged;

	public delegate void OnAllGemsCollectedDelegate(float collectedGemValue);
	public static OnAllGemsCollectedDelegate OnAllGemsCollected;

	// UI

	public delegate void OnGameTimerDelegate(int gameHundredths);
	public static OnGameTimerDelegate OnGameTimer;

	public delegate void OnFadeInDelegate(Action onFade);							// fade to clear
	public static OnFadeInDelegate OnFadeIn;

	public delegate void OnFadeOutDelegate(Action onFade, bool toClear);			// fade to black
	public static OnFadeOutDelegate OnFadeOut;

	public delegate void OnInitLevelsDelegate(List<GameData> games, GameData currentGame);        // populate UI
	public static OnInitLevelsDelegate OnInitLevels;

	public delegate void OnLevelSelectedDelegate(GameData gameData, int gameIndex);		// load scene
	public static OnLevelSelectedDelegate OnLevelSelected;

	public delegate void OnLoadNextLevelDelegate();						// on continue button clicked on win UI
	public static OnLoadNextLevelDelegate OnLoadNextLevel;

	public delegate void OnReloadLevelDelegate();                     // on continue button clicked on lose UI
	public static OnReloadLevelDelegate OnReloadLevel;

	public delegate void OnMenuShowingDelegate(bool showing);
	public static OnMenuShowingDelegate OnMenuShowing;

	public delegate void OnSkipPasswordDelegate();
	public static OnSkipPasswordDelegate OnSkipPassword;            // hide PasswordUI

	public delegate void OnSkipRegisterPlayerDelegate();
	public static OnSkipRegisterPlayerDelegate OnSkipRegisterPlayer;            // hide PlayerUI

	public delegate void OnDebugTextDelegate(string text);
	public static OnDebugTextDelegate OnDebugText;


	// audio

	public delegate void OnFadeAudioDelegate(string mixerParam, float toVolume, float fadeTime);
	public static OnFadeAudioDelegate OnFadeAudio;

	public delegate void OnCrossFadeAudioDelegate(string outParam, string inParam, float fadeTime);
	public static OnCrossFadeAudioDelegate OnCrossFadeAudio;


	// firestore best times

	public delegate void OnFireStoreGetBestTimesDelegate();			// for current level and player
	public static OnFireStoreGetBestTimesDelegate OnFireStoreGetBestTimes;

	public delegate void OnFireStoreBestTimesRetrievedDelegate(string bestTimes);		
	public static OnFireStoreBestTimesRetrievedDelegate OnFireStoreBestTimesRetrieved;        // callback

	public delegate void OnFireStoreBestTimesChangedDelegate(string bestTimes);
	public static OnFireStoreBestTimesChangedDelegate OnFireStoreBestTimesChanged;

	public delegate void OnFireStoreStartListenBestTimesChangedDelegate();		// for current level and player
	public static OnFireStoreStartListenBestTimesChangedDelegate OnFireStoreStartListenBestTimesChanged;

	public delegate void OnFireStoreStopListenBestTimesChangedDelegate();		// for current level and player
	public static OnFireStoreStopListenBestTimesChangedDelegate OnFireStoreStopListenBestTimesChanged;

    public delegate void OnFireStoreSetPlayerLevelBestTimeDelegate(int bestTime);          // for current level and player
    public static OnFireStoreSetPlayerLevelBestTimeDelegate OnFireStoreSetPlayerLevelBestTime;

    public delegate void OnFireStoreBestTimeSetDelegate(string bestTimeData);
	public static OnFireStoreBestTimeSetDelegate OnFireStoreBestTimeSet;        // callback

    //public delegate void OnBestTimeChangedDelegate(string bestTime);
    //public static OnBestTimeChangedDelegate OnBestTimeChanged;

    // firestore level stats

    public delegate void OnFireStoreGetLevelStatsDelegate(string levelName);
	public static OnFireStoreGetLevelStatsDelegate OnFireStoreGetLevelStats;

	public delegate void OnFireStoreGetPlayerLevelStatsDelegate();
	public static OnFireStoreGetPlayerLevelStatsDelegate OnFireStoreGetPlayerLevelStats;		// current player and level

	public delegate void OnFireStoreIncrementLevelStatsDelegate(bool play, bool success, bool fail, int gameHundredths);          // for current level
	public static OnFireStoreIncrementLevelStatsDelegate OnFireStoreIncrementLevelStats;

	public delegate void OnFireStoreIncrementPlayerLevelStatsDelegate(bool play, bool success, bool fail, int gameHundredths);          // for current level
	public static OnFireStoreIncrementPlayerLevelStatsDelegate OnFireStoreIncrementPlayerLevelStats;

	public delegate void OnFireStoreIncrementPlayerPBDelegate(int timeIncrement);          // for current level
	public static OnFireStoreIncrementPlayerPBDelegate OnFireStoreIncrementPlayerPB;

	//public delegate void OnSetLevelStatsDelegate(string levelName, string levelStatsData);	// used to set top time
	//public static OnSetLevelStatsDelegate OnSetLevelStats;

	public delegate void OnFirestoreSetPlayerLevelStatsDelegate(LevelStats newPlayerLevelStats);		// cached until player name set in firestore
    public static OnFirestoreSetPlayerLevelStatsDelegate OnFirestoreSetPlayerLevelStats;

    //public delegate void OnLevelStatsSetDelegate(string levelStatsData);
    //public static OnLevelStatsSetDelegate OnLevelStatsSet;        // callback on successful level stats update

    //public delegate void OnLevelsRetrievedDelegate(string levelsJson);          // all levels
    //public static OnLevelsRetrievedDelegate OnLevelsRetrieved;

    //public delegate void OnLevelStatsRetrievedDelegate(string levelStatsJson);
    //public static OnLevelStatsRetrievedDelegate OnLevelStatsRetrieved;

    public delegate void OnFireStorePlayerLevelStatsRetrievedDelegate(string playerLevelStatsJson);
	public static OnFireStorePlayerLevelStatsRetrievedDelegate OnFireStorePlayerLevelStatsRetrieved;      // populate gameLibrary for convenience

	//public delegate void OnPlayerLevelStatsLoadedDelegate();
	//public static OnPlayerLevelStatsLoadedDelegate OnPlayerLevelStatsLoaded;                    // start game

	public delegate void OnFireStoreNoPlayerLevelStatsDelegate();
    public static OnFireStoreNoPlayerLevelStatsDelegate OnFireStoreNoPlayerLevelStats;					// start game

    public delegate void OnFireStoreLevelStatsChangedDelegate(string levelStatsJson);			// for current level
	public static OnFireStoreLevelStatsChangedDelegate OnFireStoreLevelStatsChanged;						// fired by listener

	public delegate void OnFireStoreStartListenLevelStatsChangedDelegate(string levelName);
	public static OnFireStoreStartListenLevelStatsChangedDelegate OnFireStoreStartListenLevelStatsChanged;

	public delegate void OnFireStoreStopListenLevelStatsChangedDelegate(string levelName);
	public static OnFireStoreStopListenLevelStatsChangedDelegate OnFireStoreStopListenLevelStatsChanged;


	// firestore schools

	public delegate void OnFireStoreGetSchoolsDelegate();
	public static OnFireStoreGetSchoolsDelegate OnFireStoreGetSchools;

	public delegate void OnFireStoreStartListenSchoolsChangedDelegate();  
	public static OnFireStoreStartListenSchoolsChangedDelegate OnFireStoreStartListenSchoolsChanged;

	public delegate void OnFireStoreStopListenSchoolsChangedDelegate();  
	public static OnFireStoreStopListenSchoolsChangedDelegate OnFireStoreStopListenSchoolsChanged;

	// firestore players

	//public delegate void OnNewPlayerDelegate(int bestHundredths);					// prompt via PlayerUI
	//public static OnNewPlayerDelegate OnNewPlayer;

	public delegate void OnFireStoreSetPlayerDelegate(string playerName, string schoolName, string password, string playerData);     
	public static OnFireStoreSetPlayerDelegate OnFireStoreSetPlayer;

	public delegate void OnFireStorePlayerSetDelegate(PlayerData playerData);
	public static OnFireStorePlayerSetDelegate OnFireStorePlayerSet;                      // callback

    public delegate void OnFireStoreSchoolsRetrievedDelegate(string schools);
    public static OnFireStoreSchoolsRetrievedDelegate OnFireStoreSchoolsRetrieved;        // callback

    public delegate void OnFireStoreSchoolsChangedDelegate(string schools);
	public static OnFireStoreSchoolsChangedDelegate OnFireStoreSchoolsChanged;        // callback

	public delegate void OnFireStoreGetPlayerDelegate(string playerName);
	public static OnFireStoreGetPlayerDelegate OnFireStoreGetPlayer;

	public delegate void OnFireStorePlayerRetrievedDelegate(PlayerData playerData);
	public static OnFireStorePlayerRetrievedDelegate OnFireStorePlayerRetrieved;			// callback

	public delegate void OnFireStoreGetPlayersDelegate();
	public static OnFireStoreGetPlayersDelegate OnFireStoreGetPlayers;

	public delegate void OnFireStorePlayersChangedDelegate(string players);
	public static OnFireStorePlayersChangedDelegate OnFireStorePlayersChanged;        // callback

    public delegate void OnFireStorePlayersRetrievedDelegate(string players);
    public static OnFireStorePlayersRetrievedDelegate OnFireStorePlayersRetrieved;        // callback

    public delegate void OnFireStoreStartListenPlayersChangedDelegate();
	public static OnFireStoreStartListenPlayersChangedDelegate OnFireStoreStartListenPlayersChanged;

	public delegate void OnFireStoreStopListenPlayersChangedDelegate();
	public static OnFireStoreStopListenPlayersChangedDelegate OnFireStoreStopListenPlayersChanged;

	public delegate void OnFireStoreNoPlayerDelegate(string player);
	public static OnFireStoreNoPlayerDelegate OnFireStoreNoPlayer;        // fallback

	// passwords

	public delegate void OnFireStoreGetPasswordDelegate(string password);
	public static OnFireStoreGetPasswordDelegate OnFireStoreGetPassword;

	public delegate void OnFireStorePasswordNotFoundDelegate(); //  string password);
	public static OnFireStorePasswordNotFoundDelegate OnFireStorePasswordNotFound;

	public delegate void OnFireStoreGetPasswordsDelegate();
	public static OnFireStoreGetPasswordsDelegate OnFireStoreGetPasswords;

	public delegate void OnFireStorePasswordsChangedDelegate(string passwords);
	public static OnFireStorePasswordsChangedDelegate OnFireStorePasswordsChanged;        // callback

	public delegate void OnFireStorePasswordsRetrievedDelegate(string passwords);
	public static OnFireStorePlayersRetrievedDelegate OnFireStorePasswordsRetrieved;        // callback

	public delegate void OnFireStoreStartListenPasswordsChangedDelegate();
	public static OnFireStoreStartListenPasswordsChangedDelegate OnFireStoreStartListenPasswordsChanged;

	public delegate void OnFireStoreStopListenPasswordsChangedDelegate();
	public static OnFireStoreStopListenPasswordsChangedDelegate OnFireStoreStopListenPasswordsChanged;

	// firestore misc

	public delegate void OnFireStoreErrorDelegate(string error);
	public static OnFireStoreErrorDelegate OnFireStoreError;

	public delegate void OnFireStoreInfoDelegate(string info);
	public static OnFireStoreInfoDelegate OnFireStoreInfo;


    // json data saved to file
	// TODO: these are obsolete - can't save data from mobile browser, password used to lookup PlayerData

    public delegate void OnLoadPlayerDataFromFileDelegate(out PlayerData playerData); //, string playerName, string schoolName);
    public static OnLoadPlayerDataFromFileDelegate OnLoadPlayerDataFromFile;

	public delegate void OnPlayerDataLoadedFromFileDelegate(PlayerData playerData);
	public static OnPlayerDataLoadedFromFileDelegate OnPlayerDataLoadedFromFile;

	public delegate void OnSavePlayerDataToFileDelegate(PlayerData playerData);
    public static OnSavePlayerDataToFileDelegate OnSavePlayerDataToFile;

	public delegate void OnPlayerDataSavedToFileDelegate(PlayerData playerData);
	public static OnPlayerDataSavedToFileDelegate OnPlayerDataSavedToFile;

	//public delegate void OnLoadAllGameDataDelegate(List<GameData> games);
	//public static OnLoadAllGameDataDelegate OnLoadAllGameData;

	//public delegate void OnReadyToStartDelegate(List<GameData> games);
	//public static OnReadyToStartDelegate OnReadyToStart;

	//public delegate void OnPlayerLevelStatsLoadedDelegate();
	//public static OnPlayerLevelStatsLoadedDelegate OnPlayerLevelStatsLoaded;

	////public delegate void OnLoadGameDataDelegate(string gameName, out SaveData saveData);
	////public static OnLoadGameDataDelegate OnLoadGameData;

	//public delegate void OnSaveGameDataDelegate(SaveData saveData);
	//public static OnSaveGameDataDelegate OnSaveGameData;

	// audio

	public delegate void OnPlayAudioDelegate(AudioSource source, AudioManager.ClipTag clipName, bool randomPitch, float pitchOverride, bool checkForChange);
	public static OnPlayAudioDelegate OnPlayAudio;
}