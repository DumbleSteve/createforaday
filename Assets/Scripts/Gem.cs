using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]

public class Gem : MonoBehaviour
{
    public ParticleSystem sparkles;
    public float GemValue = 1f;         // hundredths (time bonus)

    private AudioSource gemAudio;

    private bool collected = false;


    private void Start()
    {
        gemAudio = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collected)
            return;

        if (collision.gameObject.CompareTag("Player"))
        {
            var character = collision.gameObject.GetComponent<CharacterController>();
            if (character == null || character.HasFainted || character.IsTeleporting)
                return;

            collected = true;

            GameEvents.OnPlayAudio?.Invoke(gemAudio, AudioManager.ClipTag.Collectible, false, 0f, false);
            GameEvents.OnGemCollected?.Invoke(GemValue);

            GetComponent<Collider2D>().enabled = false;
            GetComponent<SpriteRenderer>().enabled = false;
            sparkles.Stop();
        }
    }
}
