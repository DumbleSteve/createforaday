using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// script to capture horizontal or jump 'held down' keyboard inputs

public class KeyboardInput : MonoBehaviour
{
    private bool leftKeyDown = false;
    private bool rightKeyDown = false;
    private bool jumpKeyDown = false;

    private void Update()
    {
        if (Input.GetAxis("Horizontal") > 0)        // D / right arrow
        {
            if (!rightKeyDown)
            {
                rightKeyDown = true;
                GameEvents.OnMoveKeyPress?.Invoke(Vector2.right, true);
            }
            // else right key already down
        }
        else if (Input.GetAxis("Horizontal") < 0)   // A / left arrow
        {
            if (!leftKeyDown)
            {
                leftKeyDown = true;
                GameEvents.OnMoveKeyPress?.Invoke(Vector2.left, true);
            }
            // else left key already down
        }
        else        // no horizontal keys pressed
        {
            if (rightKeyDown)
            {
                rightKeyDown = false;
                GameEvents.OnMoveKeyPress?.Invoke(Vector2.right, false);
            }
            if (leftKeyDown)
            {
                leftKeyDown = false;
                GameEvents.OnMoveKeyPress?.Invoke(Vector2.left, false);
            }
        }

        if (Input.GetAxis("Jump") > 0 || Input.GetAxis("Vertical") > 0)        // space bar / W / up-arrow
        {
            if (!jumpKeyDown)
            {
                jumpKeyDown = true;
                GameEvents.OnMoveKeyPress?.Invoke(Vector2.up, true);
            }
            // else jump key already down
        }
        else
        {
            if (jumpKeyDown)
            {
                jumpKeyDown = false;
                GameEvents.OnMoveKeyPress?.Invoke(Vector2.up, false);
            }
        }
    }
}
