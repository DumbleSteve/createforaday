//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;


//// base class to capture (virtual function) horizontal or jump 'held down' inputs 
//// that charge up to a max and reset on grounding or no input

//public abstract class InputBase : MonoBehaviour
//{
//    [SerializeField] protected bool horizontal;           // else jump

//    [SerializeField] protected float forceIncrement;      // each frame
//    [SerializeField] protected float maxForce;

//    protected float moveCharge;                           // increased each frame of input, reset on grounding
//    protected bool fullyCharged => (maxForce > 0 && moveCharge >= maxForce);

//    protected bool isActivated = false;                   // key / button held down
//    protected bool isGrounded = false;

//    protected bool CanJump => !horizontal && (isGrounded || !fullyCharged);
//    protected bool CanMoveSideways => horizontal && (isGrounded || !fullyCharged);


//    private void OnEnable()
//    {
//        GameEvents.OnCharacterGrounded += OnCharacterGrounded;
//    }

//    private void OnDisable()
//    {
//        GameEvents.OnCharacterGrounded -= OnCharacterGrounded;
//    }


//    private void Start()
//    {
//        ResetCharge();
//    }

//    private void FixedUpdate()
//    {
//        GetInput();     // virtual
//    }

//    protected virtual void GetInput()
//    {
//        // overrides to get keyboard / mouse / touch / button input and fire OnCharacterMove event accordingly
//    }

//    // re-enable movement once grounded again
//    private void OnCharacterGrounded(bool isGrounded, bool wasGrounded)
//    {
//        this.isGrounded = isGrounded;

//        if (isGrounded && !wasGrounded)
//        {
//            ResetCharge();
//        }
//    }

//    private void ResetCharge()
//    {
//        moveCharge = 0;
//        //isActivated = false;
//        Debug.Log("ResetCharge");
//    }
//}
