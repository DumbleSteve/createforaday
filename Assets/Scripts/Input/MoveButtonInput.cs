using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

// script to capture left, right or jump 'held down' button
// or handle keyboard input events
// that charge up to a max and translate to force impulses
// applied to character via OnCharacterMove event

public class MoveButtonInput : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [SerializeField] private Vector2 forceDirection;    // Vector2.left, right or up
    [SerializeField] private Image buttonImage;
    [SerializeField] private Color pressedColour = Color.red;

    [SerializeField] private float forceIncrement;      // each frame, while isPressed
    [SerializeField] private float maxForce;

    private bool LeftMove => forceDirection == Vector2.left;
    private bool RightMove => forceDirection == Vector2.right;
    private bool IsHorizontal => RightMove || LeftMove;
    private bool JumpMove => forceDirection == Vector2.up;

    private bool CanJump => !IsHorizontal && (onGround || !fullyCharged);
    private bool CanMoveSideways => IsHorizontal && (onGround || !fullyCharged);

    private float moveCharge;                           // increased each frame of input, reset on grounding
    private bool fullyCharged => (maxForce > 0 && moveCharge >= maxForce);

    private bool onGround = false;

    private bool isPressed = false;                     // true while button (or move key) held down

    private bool CanBoost => (IsHorizontal && !onGround); // || (CanJump && isGrounded);

    // boost first frame of in-air horizontal movement
    [SerializeField] private bool boostFirstFrame = false;              // boost (horiz) on first frame that button was pressed
    [SerializeField] private float firstFrameBoostFactor = 3f; 

    private bool gameStarted = false;
    private bool gameOver = false;

    private Color buttonColour;         // at start (when not pressed)


    private void OnEnable()
    {
        GameEvents.OnGameStart += OnGameStart;
        GameEvents.OnMoveKeyPress += OnMoveKeyPress;
        GameEvents.OnCharacterGrounded += OnCharacterGrounded;
        GameEvents.OnCharacterWallJump += OnCharacterWallJump;
        GameEvents.OnCharacterFaint += OnCharacterFaint;
    }

    private void OnDisable()
    {
        GameEvents.OnGameStart -= OnGameStart;
        GameEvents.OnMoveKeyPress -= OnMoveKeyPress;
        GameEvents.OnCharacterGrounded -= OnCharacterGrounded;
        GameEvents.OnCharacterWallJump -= OnCharacterWallJump;
        GameEvents.OnCharacterFaint -= OnCharacterFaint;
    }


    private void Start()
    {
        gameOver = false;
        buttonColour = buttonImage.color;
        SetPressed(false);
    }

    // IPointerDownHandler implementation
    public void OnPointerDown(PointerEventData eventData)
    {
        boostFirstFrame = CanBoost;
        SetPressed(true);
    }

    // IPointerUpHandler implementation
    public void OnPointerUp(PointerEventData eventData)
    {
        SetPressed(false);
    }

    // key press down / up equivalent
    private void OnMoveKeyPress(Vector2 direction, bool down)
    {
        if (direction != forceDirection)    // only if key corresponds to this button
            return;

        if (down)
            boostFirstFrame = CanBoost;
        else if (! onGround)  // charge reset on grounded
            MaxCharge();        // set to fully charged on button up - to end jump

        SetPressed(down);
    }

    private void SetPressed(bool pressed)
    {
        if (isPressed == pressed)       // no change!
            return;

        isPressed = pressed;

        if (pressed)
            buttonImage.color = pressedColour;
        else
            buttonImage.color = buttonColour;
;    }

    // add 'charge' if button pressed and push character rigidbody
    private void FixedUpdate()
    {
        if (!gameStarted || gameOver)
            return;

        if (isPressed)
            MoveCharacter();
    }

    // increase charge and move character if on ground or not fully charged
    // called by FixedUpdate while button is pressed
    private void MoveCharacter()
    {
        if (!isPressed)
            return;

        //Debug.Log($"MoveCharacter {name}: isGrounded {isGrounded}  moveCharge: {moveCharge} / {maxForce}");
        float moveForce = forceIncrement;

        if (IsHorizontal)
        {
            //if (!isGrounded)
            //    Debug.Log($"MoveCharacter {name}: isGrounded {isGrounded} moveForce = {moveForce}  moveCharge: {moveCharge} / {maxForce} CanMoveSideways = {CanMoveSideways}");

            if (CanMoveSideways)           // must be on ground or not fully 'charged'
            {
                if (!onGround)
                {
                    if (boostFirstFrame)
                    {
                        moveForce *= firstFrameBoostFactor;
                        //Debug.Log($"MoveCharacter {name}: boostFirstFrame moveForce = {moveForce}");

                        boostFirstFrame = false;
                    }

                    moveCharge += moveForce;      // 'charge up' until maxForce reached - only if not grounded
                }

                GameEvents.OnCharacterMove?.Invoke(forceDirection, moveForce);
            }
        }
        else        // vertical (jump)
        {
            if (CanJump)                    // must be on ground or not fully 'charged'
            {
                moveCharge += moveForce;      // 'charge up' until maxForce reached
                GameEvents.OnCharacterMove?.Invoke(Vector2.up, moveForce);
            }
        }
    }

    // reset charge once grounded again
    private void OnCharacterGrounded(CharacterController character, bool isGrounded, bool wasGrounded, Collider2D groundedOn)
    {
        onGround = isGrounded;

        if (isGrounded && !wasGrounded)
            ResetCharge();      // all move buttons
    }

    // reset charge on 'into wall' button to allow follow-up wall jump in opposite direction
    // and max charge on 'from wall' button so character moves under single wall jump impulse only
    private void OnCharacterWallJump(Wall fromWall, WallJump.WallDirection fromSide)
    {
        //ResetCharge();
        //Debug.Log($"<color=cyan>OnCharacterWallJump {name}: fromWall = {fromWall.name} fromSide = {fromSide}</color>");

        switch (fromSide)
        {
            case WallJump.WallDirection.None:
                break;

            case WallJump.WallDirection.OnLeft:
            case WallJump.WallDirection.OnRight:
                if (IsHorizontal)       
                {
                    ResetCharge();
                    //Debug.Log($"<color=cyan>OnCharacterWallJump {name}: fromWall = {fromWall.name} fromSide = {fromSide}</color>");
                }
                // any remaining jump charge still available

                //else    // no more jumping - wall jump impulse only
                //{
                //    MaxCharge();
                //}
                break;
        }
    }

    private void ResetCharge()
    {
        moveCharge = 0;
        //Debug.Log($"<color=green>{name} ResetCharge</color>");
    }

    private void MaxCharge()
    {
        moveCharge = maxForce;      // set to fully charged on button up
        //Debug.Log($"<color=yellow>{name} MaxCharge</color>"); 
    }

    private void OnGameStart(GameData gameData, int playCount, bool promptPassword)
    {
        gameStarted = true;
    }

    private void OnCharacterFaint(CharacterController character)
    {
        gameOver = true;
    }
}
