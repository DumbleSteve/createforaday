﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Handle screen touches
// Mouse clicks emulate touches

public class TouchInput : MonoBehaviour
{
	public float SwipeForce;            // increase to apply to more force on swipe
	public ParticleSystem StartParticles;
	public ParticleSystem SwipeParticles;
	public ParticleSystem EndParticles;
	public bool PlayParticles = false;

	private Vector2 startPosition;      // screen
	private Vector2 lastPosition = Vector2.positiveInfinity;       // screen - last frame
	private Vector2 moveDirection;      // screen
	private Vector2 endPosition;        // screen

	private Camera mainCam;

	private long touchStartTicks;
	private long touchEndTicks;
	private float TickSpeedFactor = 100000f;        // to reduce swipe time

	private int tapCount = 0;

	public enum SwipeCardinal
	{
		None,
		Up,
		Down,
		Left,
		Right,
		Tap
	};

	private Vector2[] Directions90 = {
		Vector2.up,
		Vector2.down,
		Vector2.left,
		Vector2.right,
	};

	private float swipeThreshold = 2.5f;     // distance - anything less not considered a valid swipe (ie. a tap or accidental mini-swipe)

	//private GameManager.GameState currentState;
	private bool gameOver; // => currentState == GameManager.GameState.GameOver;

	private bool gamePaused;

    private void Start()
    {
		mainCam = Camera.main;
	}

    private void Update()
	{
		if (gamePaused)
		{
			return;
		}

		// track a single touch
		if (Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);

			// handle finger movements based on TouchPhase
			switch (touch.phase)
			{
				case TouchPhase.Began:
					StartTouch(touch.position, Input.touchCount);
					break;

				case TouchPhase.Moved:
					MoveTouch(touch.position, Input.touchCount);
					break;

				case TouchPhase.Ended:
					EndTouch(touch.position, Input.touchCount);
					break;

				case TouchPhase.Stationary:         // touching but hasn't moved
					break;

				case TouchPhase.Canceled:           // system cancelled touch
					break;
			}
		}

		// emulate touch with left mouse
		else if (Input.GetMouseButtonDown(0))
		{
			StartTouch(Input.mousePosition, 1);
		}
		else if (Input.GetMouseButton(0))
		{
			MoveTouch(Input.mousePosition, 1);
		}
		else if (Input.GetMouseButtonUp(0))
		{
			EndTouch(Input.mousePosition, 1);
		}

		// right mouse == 2 finger swipe
		else if (Input.GetMouseButtonDown(1))
		{
			StartTouch(Input.mousePosition, 2);
		}
		else if (Input.GetMouseButton(1))
		{
			MoveTouch(Input.mousePosition, 2);
		}
		else if (Input.GetMouseButtonUp(1))
		{
			EndTouch(Input.mousePosition, 2);
		}
	}

	private void StartTouch(Vector2 screenPosition, int touchCount)
	{
		if (touchCount == 1)            // one finger
		{
			tapCount++;
		}

		GameEvents.OnPlayerTouch?.Invoke(tapCount, touchCount);        // eg. tap to play again...

		if (gameOver)
			return;

		startPosition = screenPosition;
		touchStartTicks = DateTime.Now.Ticks;

		lastPosition = startPosition;

		GameEvents.OnSwipeStart?.Invoke(startPosition, touchCount);

		// sparkle
		if (PlayParticles)
		{
			var worldStart = mainCam.ScreenToWorldPoint(startPosition);

			StartParticles.transform.position = worldStart;
			StartParticles.Play();


			SwipeParticles.transform.position = worldStart;
			SwipeParticles.Play();      // looping
		}
	}

	private void MoveTouch(Vector2 screenPosition, int touchCount)
	{
		if (gameOver)
			return;

		Vector2 movePosition = screenPosition;

		if (lastPosition != Vector2.positiveInfinity && movePosition != lastPosition)
		{
			moveDirection = (movePosition - lastPosition).normalized;

			float moveDistance = Mathf.Abs(Vector2.Distance(lastPosition, movePosition));
			float moveSpeed = moveDistance / Time.deltaTime;     // speed = distance / time

			GameEvents.OnSwipeMove?.Invoke(movePosition, moveDirection, moveSpeed * SwipeForce, moveDistance, touchCount);

			lastPosition = movePosition;
		}
		else
			GameEvents.OnSwipeMove?.Invoke(movePosition, Vector2.zero, 0f, 0f, touchCount);

		// sparkle
		if (PlayParticles)
			SwipeParticles.transform.position = mainCam.ScreenToWorldPoint(movePosition);
	}

	private void EndTouch(Vector2 screenPosition, int touchCount)
	{
		if (gameOver)
			return;

		endPosition = screenPosition;
		touchEndTicks = DateTime.Now.Ticks;
		moveDirection = (endPosition - startPosition).normalized;

		float moveDistance = Mathf.Abs(Vector2.Distance(startPosition, endPosition));
		float moveSpeed = 0;     // distance / time
		float touchTicks = touchEndTicks - touchStartTicks;

		if (touchTicks > 0)            // should be!!
		{
			float swipeTime = touchTicks / TickSpeedFactor;
			moveSpeed = moveDistance / swipeTime;     // speed = distance / time
			//Debug.Log($"EndTouch direction = {moveDirection} distance = {moveDistance} time = {swipeTime} speed = {moveSpeed} tapCount = {tapCount}");
		}

		if (moveSpeed > 0)
		{
			GameEvents.OnSwipeEnd?.Invoke(endPosition, moveDirection, moveSpeed * SwipeForce, moveDistance, touchCount, tapCount);
		}

		GetSwipeCardinal(moveSpeed * SwipeForce);     // if a 'valid' swipe

		GameEvents.OnPlayerRelease?.Invoke(touchCount);

		// sparkle
		if (PlayParticles)
		{
			var worldEnd = mainCam.ScreenToWorldPoint(endPosition);

			EndParticles.transform.position = worldEnd;
			EndParticles.Play();

			SwipeParticles.transform.position = worldEnd;
			SwipeParticles.Stop();
		}
	}


	private void GetSwipeCardinal(float swipeSpeed)
	{
		var swipeDistance = (endPosition - startPosition).magnitude;

		Vector2 cardinalDirection = GetCardinal(moveDirection);        // normalised
		SwipeCardinal swipeCardinal = SwipeCardinal.None;

		if (swipeDistance < swipeThreshold)     // ie. a tap or accidental swipe
		{
			swipeCardinal = SwipeCardinal.Tap;
		}
		else        // valid swipe
		{
			if (cardinalDirection == Vector2.up)
				swipeCardinal = SwipeCardinal.Up;
			else if (cardinalDirection == Vector2.down)
				swipeCardinal = SwipeCardinal.Down;
			else if (cardinalDirection == Vector2.left)
				swipeCardinal = SwipeCardinal.Left;
			else if (cardinalDirection == Vector2.right)
				swipeCardinal = SwipeCardinal.Right;
		}

		//Debug.Log($"GetSwipeCardinal {swipeCardinal} moveDirection {moveDirection}");
        if (swipeCardinal != SwipeCardinal.None)
			GameEvents.OnSwipeCardinal?.Invoke(swipeCardinal, swipeSpeed, swipeDistance);
	}

	// find nearest cardinal direction (of 4) to the swipe direction
	private Vector2 GetCardinal(Vector2 direction)
	{
		float nearest = -Mathf.Infinity;
		Vector2 result = Vector2.zero;

		foreach (var direction90 in Directions90)
		{
			var dotToCompare = Vector2.Dot(direction, direction90); // .Dot returns 1 to -1 to indicate closeness to vertical (0 is horizontal)

			if (dotToCompare > nearest)
			{
				nearest = dotToCompare;
				result = direction90;
			}
		}

		//Debug.Log($"GetCardinal {result} swipeDirection = {direction}");
		return result;
	}

	//// find nearest cardinal direction (of 4) to the swipe direction
	//private Vector2 ClampTo90(Vector2 direction)
	//{
	//	Vector2 result = Directions90[0];
	//	float nearest = Vector2.Dot(direction, Directions90[0]);      // .Dot returns 1 to -1 to indicate closeness to vertical (0 is horizontal)

	//	for (int i = 1; i < Directions90.Length; i++)
	//	{
	//		var dotToCompare = Vector2.Dot(direction, Directions90[i]);

	//		if (dotToCompare > nearest)
	//		{
	//			nearest = dotToCompare;
	//			result = Directions90[i];
	//		}
	//	}

	//	return result;
	//}
}
